<?php

namespace App;
use App\FbCampaignAccount;
use Illuminate\Database\Eloquent\Model;

class FbCampaignApp extends Model
{
    protected $table = 'facebook_apps';
    protected $fillable = [
        'name', 'app_id', 'app_secret', 'access_token'
    ];

//    public function fbCampaignAccount() {
//        return $this->hasMany(FbCampaignAccount::class, 'fb_app_id', 'id');
//    }
}
