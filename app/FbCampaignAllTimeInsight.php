<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class FbCampaignAllTimeInsight extends Model
{
    protected $table = 'fb_campaign_alltime_insights';
    protected $fillable = [
        'fb_campaign_id', 'campaign_id', 'name', 'results', 'spend', 'impressions', 'clicks', 'unique_clicks',
        'unique_link_clicks_ctr', 'cost_per_unique_click', 'cost_per_unique_add_to_cart',
        'unique_adds_to_cart', 'cpm', 'purchase', 'time_range'
    ];
}
