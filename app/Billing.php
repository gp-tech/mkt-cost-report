<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    //
    protected $fillable = [
        'site',
        'card',
        'verify',
        'expire_month',
        'expire_year',
        'email',
        'password',
        'username',
        'is_registered',
        'branch',
        'alt_address',
        'uid',
    ];

    public function address()
    {
        return $this->hasOne(Address::class, 'billing_id', 'id');
    }

    public static function getNext($site)
    {
        $maxRoller = static::where('site', '=', $site)->max('roller');
        
        $nextItem = static::where('site', '=', $site)->where('roller', '<', $maxRoller)->first();
        if (empty($nextItem)) {
            $nextItem = static::where('site', '=', $site)->where('roller', '=', $maxRoller)->first();
            $nextItem->roller = $maxRoller + 1;
        } else {
            $nextItem->roller = $maxRoller;
        }
        
        $nextItem->save();
        
        return $nextItem;
    }
}
