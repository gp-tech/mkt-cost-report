<?php

namespace App;

use App\Traits\GetInsights;
use Illuminate\Database\Eloquent\Model;


class FbCampaign extends Model
{
    use GetInsights;

    protected $fillable = [
        'campaign_id', 'name', 'status', 'account_id', 'fb_app_id'
    ];

    public function campaignInsight() {
        return $this->hasMany(FbCampaignInsight::class);
    }

}
