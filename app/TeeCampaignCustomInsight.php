<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TeeCampaignCustomInsight extends Model
{
    protected $table = 'tee_campaign_custom_insights';
    protected $fillable = [
        'campaign_id', 'url', 'view', 'name',
        'orders', 'conversion', 'unit_sales',
        'total_profit', 'pixel', 'status', 'time_range', 'source'
    ];

    public function teeCampaign() {
        return $this->belongsTo(Campaign::class);
    }
}
