<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $fillable = [
        'type',
        'billing_id',
        'order_id',
        'first_name',
        'last_name',
        'address1',
        'address2',
        'telephone',
        'city',
        'company',
        'state',
        'state_code',
        'zip',
        'country',
        'country_code',
    ];

    public static function getNext($billingId)
    {
        $maxRoller = static::where('billing_id', '=', $billingId)->where('type', '=', 1)->max('roller');
        
        $nextItem = static::where('billing_id', '=', $billingId)->where('type', '=', 1)
            ->where('roller', '<', $maxRoller)
            ->first();
        if (empty($nextItem)) {
            $nextItem = static::where('billing_id', '=', $billingId)->where('type', '=', 1)
                ->where('roller', '=', $maxRoller)
                ->first();
            $nextItem->roller = $maxRoller + 1;
        } else {
            $nextItem->roller = $maxRoller;
        }
        
        $nextItem->save();
        
        return $nextItem;
    }
}
