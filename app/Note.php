<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * 
 * @method \Illuminate\Database\Eloquent\Builder visible() scope for visible users
 * @method \Illuminate\Database\Eloquent\Builder deletable() scope for deletable users
 *
 */
class Note extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'order_id',
        'comment'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
