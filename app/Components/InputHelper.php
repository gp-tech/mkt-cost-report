<?php
namespace App\Components;

class InputHelper
{

    /**
     * Get trimmed inputs
     *
     * @param array $keys
     *
     * @return array
     */
    public static function trim($keys)
    {
        $inputs = request()->only($keys);
        
        return static::recursive_trimming($inputs);
    }

    protected static function recursive_trimming($inputs)
    {
        $results = [];
        
        foreach ($inputs as $key => $value) {
            if (is_array($value)) {
                $results[$key] = static::recursive_trimming($value);
            } else {
                $results[$key] = trim($value);
            }
        }
        
        return $results;
    }
}