<?php

namespace App\Components\Shopify;

class ShopifyWrapper
{
    protected $api;

    public function __construct($domain, $apiKey, $apiSecret, $apiPassword)
    {
        $this->api = new ShopifyAPI();
        $this->api->setApiKey($apiKey);
        $this->api->setApiSecret($apiSecret);
        $this->api->setShop($domain);
        $this->api->setAccessToken($apiPassword);
    }

    public function getOrders($data = [])
    {

        // set default values
        try {
            if (\File::exists($path = storage_path('orders.json'))) {
                if (!empty($data['page']) && $data['page'] == 1) {
                    return json_decode(\File::get($path));
                } else {
                    return null;
                }
            }
            return $this->api->request('GET', '/admin/orders.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getOrder($orderId, $data = [])
    {
        try {
            return $this->api->request('GET', '/admin/orders/' . $orderId . '.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getTransactions($orderId, $data = [])
    {
        try {
            return $this->api->request('GET', '/admin/orders/' . $orderId . '/transactions.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function createTransaction($orderId, $data = [])
    {
        try {
            return $this->api->request('POST', '/admin/orders/' . $orderId . '/transactions.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function doCancel($orderId, $data = [])
    {
        try {
            return $this->api->request('POST', '/admin/orders/' . $orderId . '/cancel.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function calculateRefund($orderId, $data = [])
    {
        try {
            return $this->api->request('POST', '/admin/orders/' . $orderId . '/refunds/calculate.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function doRefund($orderId, $data = [])
    {
        try {
            return $this->api->request('POST', '/admin/orders/' . $orderId . '/refunds.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function doFulfill($orderId, $data = [])
    {
        try {
            return $this->api->request('POST', '/admin/orders/' . $orderId . '/fulfillments.json', $data);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getBarcode($variantId)
    {
        try {
            $result = $this->api->request('GET', '/admin/variants/' . $variantId . '.json', [
                'fields' => 'barcode',
            ]);

            if (empty($result->variant->barcode)) {
                return null;
            }
            return $result->variant->barcode;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getImages($productId)
    {
        try {
            return $this->api->request('GET', '/admin/products/' . $productId . '/images.json');
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getProduct($productId) {
        try {
            return $this->api->request('GET', '/admin/products/' . $productId . '.json');
        } catch (\Exception $e) {
            return null;
        }
    }
}
