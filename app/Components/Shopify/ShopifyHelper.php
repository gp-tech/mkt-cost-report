<?php
namespace App\Components\Shopify;

use Carbon\Carbon;

class ShopifyHelper
{

    protected static $dateTimeFields = [
        'closed_at',
        'processed_at',
        'cancelled_at',
        'created_at',
        'updated_at'
    ];

    public static function parseDateTimeFields($data, $fields = null)
    {
        if (empty($fields)) {
            $fields = static::$dateTimeFields;
        }
        
        foreach ($fields as $field) {
            if (! empty($data[$field])) {
                $data[$field] = Carbon::parse($data[$field]);
            }
        }
        
        return $data;
    }
}