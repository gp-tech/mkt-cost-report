<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Components\Shopify\ShopifyWrapper;

class Shop extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'domain',
        'api_key',
        'api_secret',
        'api_password',
        'status',
    ];
    
    protected $date = ['deleted_at'];

    /**
     * 
     * @return \App\Components\Shopify\ShopifyWrapper
     */
    public function getShopifyWrapper() {
        return new ShopifyWrapper($this->domain, $this->api_key, $this->api_secret, $this->api_password);
    }
    
    public function scopeActive($builder)
    {
        return $builder->where('status', 1);
    }
}
