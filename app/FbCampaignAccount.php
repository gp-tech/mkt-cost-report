<?php

namespace App;

use App\FbCampaignApp;
use Illuminate\Database\Eloquent\Model;


class FbCampaignAccount extends Model
{
    protected $table = 'facebook_accounts';
    protected $fillable = [
        'fb_app_id', 'name', 'account_id'
    ];

    public function CampaignApp() {
        return $this->belongsTo(FbCampaignApp::class, 'fb_app_id');
    }
}
