<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'campaign_id', 'name', 'url', 'view', 'orders', 'conversion', 'unit_sales', 'total_profit', 'pixel', 'status', 'source', 'time_range'
    ];

    public function campaignInsight() {
        return $this->hasMany(TeeCampaignInsight::class);
    }
}
