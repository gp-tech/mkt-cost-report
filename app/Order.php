<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;

class Order extends Model
{

    protected $fillable = [
        'shop_id',
        'pending_user_id',
        'uid',
        'name',
        'billing_address_id',
        'ship_address_id',
        'order_number',
        'cancel_reason',
        'cancelled_at',
        'processed_at',
        'closed_at',
        'currency',
        'email',
        'financial_status',
        'fulfillment_status',
        'total_price',
        'is_processed',
        'created_at',
        'updated_at',
    ];

    protected $date = [
        'deleted_at',
        'processed_at',
        'closed_at',
        'cancelled_at'
    ];
    
    public function shop()
    {
        return $this->belongsTo('App\Shop', 'shop_id', 'id');
    }
    
    public function billing_address()
    {
        return $this->hasOne('App\Address')->where('type', 0);
    }
    
    public function shipping_address()
    {
        return $this->hasOne('App\Address')->where('type', 1);
    }
    
    public function note()
    {
        return $this->hasMany('App\Note');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function orderProduct()
    {
        return $this->hasMany('App\OrderProduct');
    }
    
    public function products()
    {
        return $this->belongsToMany('App\Product', 'order_products')->withPivot('quantity', 'price')->with('links');
    }
    
    public function pending_user()
    {
        return $this->belongsTo('App\User', 'pending_user_id', 'id');
    }
    
    public function scopeScopeUser(Builder $query)
    {
        if (Auth::user()->role_id == 2) {
            $query->where('user_id', '=', Auth::user()->id);
        }
        return $query;
    }
    
    public function scopeScopeCancelled(Builder $query)
    {
        $query->whereNotNull('cancelled_at');
        return $query;
    }
    
    public function scopeScopeNotCancelled(Builder $query)
    {
        $query->whereNull('cancelled_at');
        return $query;
    }
    
    public function scopeScopeProcessed(Builder $query, $value)
    {
        $query->where('is_processed', '=', $value);
        return $query;
    }
}
