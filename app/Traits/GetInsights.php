<?php

namespace App\Traits;

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\Campaign;

// Use for App\Account
trait GetInsights
{
    public function getAdsInsights($time, $env)
    {
        $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
        $api->setDefaultGraphVersion('11.0');
        $api->setLogger(new CurlLogger());

        $fields = array(
            'spend', 'unique_link_clicks_ctr', 'clicks', 'impressions', 'unique_clicks', 'reach', 'unique_actions',
            'cost_per_unique_click', 'cost_per_unique_action_type', 'actions', 'cpm', 'purchase_roas',
            'website_purchase_roas', 'account_currency', 'action_values'
        );

        if ($time == 'all') {
            if ($this->start_time < date('Y-m-d', strtotime("2021-07-01"))) {
                $this->start_time = '2021-07-01';
            }

            $start_time = $this->start_time;
            $begin = new \DateTime($start_time);
            $end = new \DateTime(date('Y-m-d'));
            $end = $end->modify('+1 day');

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval, $end);

            foreach ($daterange as $date) {
                $since = $date->format("Y-m-d");
                $params = array(
                    'time_increment' => 1,
                    'time_range'     => json_encode([
                        'since' => date('Y-m-d', strtotime($since)),
                        'until' => date('Y-m-d', strtotime($since))
                    ])
                );
                $id = $this->campaign_id;
                echo 'Updating campaign ' . $id . ' -- ' . $since . PHP_EOL;
                try {
                    $response = (new Campaign($id))->getInsights(
                        $fields,
                        $params
                    )->getResponse()->getContent();

                    if (!isset($response['data'])) return;
                    if (count($response['data']) < 1) return;

                    $this->updateOrCreateData($response, $since, $this->id, $this->campaign_id);
                } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookClientException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookServerException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                    return $e->getMessage();
                } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                    return $e->getMessage();
                } catch(\Exception $e) {
                    echo 'GET ERROR =' . $this->id . PHP_EOL;
                    //\App\FbCampaign::where('id', $this->id)->update(['status' => 'ERROR']);
                    return;
                }
            }
        } else {
            if ($time == 'today') {
                $time = date('Y-m-d');
            } else {
                $time = date('Y-m-d', strtotime("-1 days"));
            }

            $params = array(
                'time_increment' => 1,
                'time_range'     => json_encode([
                    'since' => date('Y-m-d', strtotime($time)),
                    'until' => date('Y-m-d', strtotime($time))
                ])
            );

            $id = $this->campaign_id;
            try {
                $response = (new Campaign($id))->getInsights(
                    $fields,
                    $params
                )->getResponse()->getContent();

                if (!isset($response['data'])) return;
                if (count($response['data']) < 1) return;

                $this->updateOrCreateData($response, $time, $this->id, $this->campaign_id);
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookClientException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookServerException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                return $e->getMessage();
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                return $e->getMessage();
            } catch(\Exception $e) {
                echo 'GET ERROR =' . $this->id . PHP_EOL;
                //\App\FbCampaign::where('id', $this->id)->update(['status' => 'ERROR']);
                return;
            }
        }

    }

    public function updateOrCreateData($response, $time, $id, $campaign_id) {
        echo 'Updating campaign ' . $campaign_id . PHP_EOL;
        $insert = [];
        $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
        $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
        $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
        $insert['purchase_conversion_value'] = $insert['purchase_roas'] = $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
        $insert['time_range'] = date('Y-m-d', strtotime($time));
        $insert['fb_campaign_id'] = $id;
        $insert['campaign_id'] = $campaign_id;

        if (count($response['data']) > 0) {
            $response = $response['data'][0];

            if ($response['impressions'] < 1) return;

            if (isset($response['actions'])) {
                foreach ($response['actions'] as $item) {

                    if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                        $insert['unique_adds_to_cart'] = $item['value'];
                    }

                    if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                        $insert['purchase'] = $insert['results'] = $item['value'];
                    }
                }
            }
            if (isset($response['action_values'])) {
                foreach ($response['action_values'] as $item) {
                    if (isset($item['action_type']) && $item['action_type'] == 'omni_purchase') {
                        $insert['purchase_conversion_value'] = $item['value'];
                    }
                }
            }
            if (isset($response['purchase_roas'])) {
                if (isset($response['purchase_roas'][0])) {
                    $insert['purchase_roas'] = $response['purchase_roas'][0]['value'];
                }
            }
            if (isset($response['unique_actions'])) {
                foreach ($response['unique_actions'] as $item) {
                    if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                        $insert['unique_clicks'] = isset($item['value']) ? $item['value'] : 0;
                    }
                }
            }

            $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
            $insert['spend'] = $response['spend'];

            if ($response['account_currency'] == 'VND') {
                $insert['spend'] = round($response['spend'] / 22600, 2);
            }

            if ($response['account_currency'] == 'EUR') {
                $insert['spend'] = round($response['spend'] / 0.82, 2);
            }
            $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
            $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
            $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;

            $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;

            if (\App\FbCampaignInsight::where('campaign_id', $campaign_id)->where('time_range', $insert['time_range'])->count()) {
                \App\FbCampaignInsight::where('campaign_id', $campaign_id)->where('time_range', $insert['time_range'])->update($insert);
            } else {
                \App\FbCampaignInsight::forceCreate(
                    $insert
                );
            }
        }

    }
}
