<?php

namespace App;

use App\Traits\GetInsights;
use Illuminate\Database\Eloquent\Model;


class FbCampaignInsight extends Model
{
    protected $fillable = [
        'fb_campaign_id', 'campaign_id', 'results', 'spend', 'impressions', 'clicks', 'unique_clicks',
        'unique_link_clicks_ctr', 'cost_per_unique_click', 'cost_per_unique_add_to_cart',
        'unique_adds_to_cart', 'cpm', 'purchase', 'time_range', 'purchase_roas', 'purchase_conversion_value'
    ];

    public function fbCampaign() {
        return $this->belongsTo(FbCampaign::class);
    }
}
