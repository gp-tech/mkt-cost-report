<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{

    protected $fillable = [
        'order_id',
        'product_id',
        'quantity',
        'price',
        'total_discount',
    ];
    
    public function product() {
        return $this->belongsTo('App\Product');
    }
}
