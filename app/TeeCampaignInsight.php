<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TeeCampaignInsight extends Model
{
    protected $table = 'tee_campaign_insights';
    protected $fillable = [
        'campaign_id', 'url', 'view', 'name',
        'orders', 'conversion', 'unit_sales',
        'total_profit', 'pixel', 'status', 'time_range'
    ];

    public function teeCampaign() {
        return $this->belongsTo(Campaign::class);
    }
}
