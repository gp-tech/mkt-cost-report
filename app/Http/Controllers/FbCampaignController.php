<?php

namespace App\Http\Controllers;

use App\Job;
use FacebookAds\Api;
use FacebookAds\Object\AbstractCrudObject;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Yajra\DataTables\DataTables;
use App\FbCampaignInsight;
use App\FbCampaign;
use App\FbCampaignApp;
use App\FbCampaignAccount;
use FacebookAds\Object\User;
use App\FbCampaignAllTimeInsight;
use function GuzzleHttp\Promise\all;

class FbCampaignController extends Controller
{
    public function __construct()
    {

    }

    public function index(Request $request){

        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-15 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime('-1 day'));
        $act = !empty($request->input('act')) ? $request->input('act') : '';
        $keyword = !empty($request->input('keyword')) ? $request->input('keyword') : '';
        $keyword1 = !empty($request->input('keyword1')) ? $request->input('keyword1') : '';
        $status = !empty($request->input('status')) ? $request->input('status') : '';
        $FbCampaignAccounts = FbCampaignAccount::where('status', 1)->get();

        $campaigns = New FbCampaign();
        $fb_cids = FbCampaignInsight::whereBetween('time_range', [$start, $end])->get(['fb_campaign_id']);

        $list_ids = [];
        if (count($fb_cids)) {
            foreach ($fb_cids as $cid) {
                $list_ids[] = $cid->fb_campaign_id;
            }
            $campaigns = $campaigns->whereIn('id', $list_ids);
        } else {
            $campaigns = [];
            return view("fb-campaigns.index", compact(
                'FbCampaignAccounts', 'campaigns','keyword', 'keyword1', 'start', 'end', 'act'));
        }

        if (!empty($keyword)) {
            $campaigns = $campaigns->where('name', 'LIKE', "%{$keyword}%");
        }
        if (!empty($keyword1)) {
            $campaigns = $campaigns->where('name', 'NOT LIKE', "%{$keyword1}%");
        }
        if ($status) {
            $campaigns = $campaigns->where('status', '=', "$status");
        }

        if ($act) {
            $campaigns = $campaigns->with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }])->where('account_id', $act);
        } else {


            $campaigns = $campaigns->with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }]);
        }
        $campaigns = $campaigns->get();
        $total = [];
        $total['results'] = $total['spend'] = $total['unique_link_clicks_ctr'] = $total['unique_adds_to_cart'] = $total['reach'] = 0;
        $total['purchase_conversion_value'] = $total['purchase_roas'] = $total['cpm'] = $total['purchase'] = $total['clicks'] = $total['impressions'] = $total['unique_clicks'] = 0;

        foreach ($campaigns as $campaign) {
            $total['results'] += $campaign->campaignInsight->sum('results');
            $total['reach'] += $campaign->campaignInsight->sum('reach');
            $total['spend'] += $campaign->campaignInsight->sum('spend');
            $total['unique_link_clicks_ctr'] += $campaign->campaignInsight->sum('unique_link_clicks_ctr');
            $total['unique_adds_to_cart'] += $campaign->campaignInsight->sum('unique_adds_to_cart');
            $total['cpm'] += $campaign->campaignInsight->sum('cpm');
            $total['purchase'] += $campaign->campaignInsight->sum('purchase');
            //$total['reach'] += $campaign->campaignInsight->sum('reach');
            $total['clicks'] += $campaign->campaignInsight->sum('clicks');
            $total['impressions'] += $campaign->campaignInsight->sum('impressions');
            $total['unique_clicks'] += $campaign->campaignInsight->sum('unique_clicks');
            if (!empty($campaign->campaignInsight->sum('purchase_roas')))
            {
                $total['purchase_roas'] += (float)$campaign->campaignInsight->sum('purchase_roas');
            }
            if (!empty($campaign->campaignInsight->sum('purchase_conversion_value')))
            {
                $total['purchase_conversion_value'] += (float)$campaign->campaignInsight->sum('purchase_conversion_value');
            }

        }
        return view("fb-campaigns.index", compact(
            'FbCampaignAccounts','campaigns', 'total', 'keyword', 'keyword1', 'start', 'end', 'act'));
    }

    public function anyData(Request $request){

        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-14 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime('-7 day'));
        $act = !empty($request->input('act')) ? $request->input('act') : '';

        if ($act) {
            $campaigns = FbCampaign::with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }])->where('account_id', $act)->get();
        } else {
            $campaigns = FbCampaign::with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }])->get();
        }
        $campaigns_data = Datatables::of($campaigns)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<a href="'. route('fb-campaigns.detail', $row->id) .'" class="btn btn-xs btn-warning">Detail</a>';
            })
            ->addColumn('results', function($row){
                return $row->campaignInsight->sum('results');
            })
            ->addColumn('spend', function($row){
                return round($row->campaignInsight->sum('spend'), 2);
            })
            ->addColumn('unique_link_clicks_ctr', function($row){
                return round($row->campaignInsight->sum('unique_link_clicks_ctr'), 2);
            })
            ->addColumn('cost_per_unique_click', function($row){
                return round($row->campaignInsight->sum('cost_per_unique_click'), 2);
            })
            ->addColumn('cost_per_unique_add_to_cart', function($row){
                return round($row->campaignInsight->sum('cost_per_unique_add_to_cart'), 2);
            })
            ->addColumn('unique_adds_to_cart', function($row){
                return round($row->campaignInsight->sum('unique_adds_to_cart'), 2);
            })
            ->addColumn('cpm', function($row){
                return round($row->campaignInsight->sum('cpm'), 2);
            })
            ->addColumn('purchase', function($row){
                return $row->campaignInsight->sum('purchase');
            })
            ->addColumn('clicks', function($row){
                return $row->campaignInsight->sum('clicks');
            })
            ->addColumn('impressions', function($row){
                return $row->campaignInsight->sum('impressions');
            })
            ->addColumn('unique_clicks', function($row){
                return $row->campaignInsight->sum('unique_clicks');
            })
            ->addColumn('created_at', function($row){
                return date('Y-m-d', strtotime($row->created_at));
            })
            ->rawColumns(['action'])
            ->make(true);
        return $campaigns_data;
    }

    public function detail($id) {
        //echo date('Y-m-d', strtotime('-4 day'));die;
        $result = FbCampaignInsight::where('fb_campaign_id', $id)->where('time_range', '>', date('Y-m-d', strtotime('-5 day')))
            ->orderBy('time_range', 'desc')->get();
        $last_3day = FbCampaignInsight::where('fb_campaign_id', $id)->where('time_range', '>', date('Y-m-d', strtotime('-4 day')))
            ->selectRaw("SUM(results) as results")
            ->selectRaw("SUM(spend) as spend")
            ->selectRaw("SUM(unique_link_clicks_ctr) as unique_link_clicks_ctr")
            ->selectRaw("SUM(cost_per_unique_click) as cost_per_unique_click")
            ->selectRaw("SUM(cost_per_unique_add_to_cart) as cost_per_unique_add_to_cart")
            ->selectRaw("SUM(unique_adds_to_cart) as unique_adds_to_cart")
            ->selectRaw("SUM(cpm) as cpm")
            ->selectRaw("SUM(purchase) as purchase")
            ->groupBy('fb_campaign_id')
            ->first();
        $last_7day = FbCampaignInsight::where('fb_campaign_id', $id)->where('time_range', '>', date('Y-m-d', strtotime('-8 day')))
            ->selectRaw("SUM(results) as results")
            ->selectRaw("SUM(spend) as spend")
            ->selectRaw("SUM(unique_link_clicks_ctr) as unique_link_clicks_ctr")
            ->selectRaw("SUM(cost_per_unique_click) as cost_per_unique_click")
            ->selectRaw("SUM(cost_per_unique_add_to_cart) as cost_per_unique_add_to_cart")
            ->selectRaw("SUM(unique_adds_to_cart) as unique_adds_to_cart")
            ->selectRaw("SUM(cpm) as cpm")
            ->selectRaw("SUM(purchase) as purchase")
            ->groupBy('fb_campaign_id')
            ->first();

        $campaign_total = FbCampaignInsight::where('fb_campaign_id', $id)
            ->selectRaw("SUM(results) as results")
            ->selectRaw("SUM(spend) as spend")
            ->selectRaw("SUM(unique_link_clicks_ctr) as unique_link_clicks_ctr")
            ->selectRaw("SUM(cost_per_unique_click) as cost_per_unique_click")
            ->selectRaw("SUM(cost_per_unique_add_to_cart) as cost_per_unique_add_to_cart")
            ->selectRaw("SUM(unique_adds_to_cart) as unique_adds_to_cart")
            ->selectRaw("SUM(cpm) as cpm")
            ->selectRaw("SUM(purchase) as purchase")
            ->groupBy('fb_campaign_id')
            ->first();
        return view("fb-campaigns.detail", compact('last_7day', 'result', 'last_3day', 'campaign_total'));

    }

    public function appRefresh($id) {
        $fbApp = FbCampaignApp::find($id);
        $FbCampaignApps = FbCampaignApp::all();
        if (!$fbApp) {
            return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('error', 'Your app has not been refresh successfully!');
        } else {
            \Artisan::call('insight:get-campaigns-insight-app-id '.$id);
            return redirect('/fb-campaigns/apps')->with('success', 'Your app has been refresh successfully!');
        }

        return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('success', 'Your app has been refresh successfully!');
    }

    public function appDelete($id) {
        //echo date('Y-m-d', strtotime('-4 day'));die;
        $fbApp = FbCampaignApp::find($id);
        $FbCampaignApps = FbCampaignApp::all();
        if (!$fbApp) {
            return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('error', 'Your app has not been delete successfully!');
        } else {
            FbCampaign::where('fb_app_id', $fbApp->id)->update(['status' => 'DELETE']);
            FbCampaignApp::where('id', $id)->update(['status' => 2]);
            FbCampaignAccount::where('fb_app_id', $id)->update(['status' => 2]);
            return redirect('fb-campaigns/apps')->with('success', 'Your app has been delete successfully!');
        }

        return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('success', 'Your app has been delete successfully!');
    }

    public function appEdit($id) {
        //echo date('Y-m-d', strtotime('-4 day'));die;
        $fbApp = FbCampaignApp::find($id);
        $FbCampaignApps = FbCampaignApp::all();

        if(request()->isMethod('post')){

            if(request()->validate([
                'name' => ['required', 'string'],
                'app_id' => ['required', 'string'],
                'app_secret' => ['required', 'string'],
            ])) {
                $fbApp->name = request()->name;
                $fbApp->app_id = request()->app_id;
                $fbApp->user_id = request()->user_id;
                $fbApp->message = '';
                $fbApp->app_secret = request()->app_secret;
                $fbApp->access_token = request()->access_token;

                $curl = 'https://graph.facebook.com/v11.0/oauth/access_token?grant_type=fb_exchange_token&client_id='.request()->app_id.'&client_secret='.request()->app_secret.'&fb_exchange_token=' . request()->access_token;
                $response = $this->get_web_page($curl);
                if ($response) {
                    $resArr = json_decode($response);
                    if (!empty($resArr->access_token)) {
                        $fbApp->access_token = $resArr->access_token;
                    }
                }

                if (request()->access_token && $fbApp->access_token != request()->access_token) {
                    $fbApp->status = 1;
                }

                $res = $fbApp->update();
                if ($res) {
                    \Artisan::call('app:refresh ' .$fbApp->id);
                }
                return redirect('fb-campaigns/app-edit/'. $fbApp->id)->with('success', 'Your app has been edit successfully!');

            }
        }
        if (!$fbApp) {
            return redirect('fb-campaigns/apps')->with('error', 'Error');
        } else {
            return view("fb-campaigns.app-edit", compact('fbApp'));
        }
    }

    public function apps(Request $request){

        $FbCampaignApps = FbCampaignApp::all();
        return view("fb-campaigns.apps", compact('FbCampaignApps'));
    }

    public function accounts(Request $request){
        $app_id = $request->get('app_id');
        $status = $request->get('status');

        if (empty($status)) $status = 0;

        $fb_apps = FbCampaignApp::where('status', 1)->get();
        if ($app_id) {
            $FbCampaignAccounts = FbCampaignAccount::with(['CampaignApp'])->where('fb_app_id', $app_id)->where('status', $status)->get();
        } else {
            $FbCampaignAccounts = FbCampaignAccount::with(['CampaignApp'])->where('status', $status)->get();
        }
        return view("fb-campaigns.accounts", compact('FbCampaignAccounts', 'app_id', 'fb_apps', 'status'));
    }

    public function getAccounts($app_id)
    {
        if (empty($app_id)) return;

        \Artisan::call('app:refresh '.$app_id);

        return redirect('/fb-campaigns/apps')->with('success', 'All account has been updated!');
    }

    public function jobs(Request $request){
        $jobs = Job::get();
        $job_data = [];
        foreach ($jobs as $job) {
            $payload = json_decode($job->payload);
            $_job['uuid'] = $payload->uuid;
            $_job['displayName'] = $payload->displayName;
            $_job['attempts'] = $job->attempts;
            $_job['created_at'] = date('Y-m-d h:i:s', strtotime($job->created_at));
            $job_data[] = $_job;
        }
        return view("fb-campaigns.jobs", compact('job_data'));
    }

    public function clearJob(Request $request){
        DB::table('jobs')->delete();
        return redirect('/fb-campaigns/jobs')->with('success', 'All jobs has been cleared!');
    }

    public function addApp(Request $request)
    {

        if($request->isMethod('post')){
            if($request->validate([
                'name' => ['required', 'string'],
                'app_id' => ['required', 'string'],
                'app_secret' => ['required', 'string'],
            ])){
                if (FbCampaignApp::where('app_secret', $request->app_secret)->count()) {
                    return redirect('/fb-campaigns/apps')->with('error', 'The  app exist !');
                }
                $fbApp = new FbCampaignApp;
                $fbApp->name = $request->name;
                $fbApp->app_id = $request->app_id;
                $fbApp->user_id = $request->user_id;
                $fbApp->message = '';
                $fbApp->app_secret = $request->app_secret;
                $fbApp->access_token = $request->access_token;

                $curl = 'https://graph.facebook.com/v11.0/oauth/access_token?grant_type=fb_exchange_token&client_id='.$request->app_id.'&client_secret='.$request->app_secret.'&fb_exchange_token=' . $request->access_token;
                $response = $this->get_web_page($curl);
                if ($response) {
                    $resArr = json_decode($response);
                    if (!empty($resArr->access_token)) {
                        $fbApp->access_token = $resArr->access_token;
                    }
                }
                $res = $fbApp->save();
                if ($res && !empty($request->user_id)) {
                    \Artisan::call('app:refresh ' .$fbApp->id);
                    \Artisan::call('insight:get-campaigns-insight-app-id '. $fbApp->id);
                }
                return redirect('/fb-campaigns/apps')->with('success', 'The  app ' . $fbApp->name . ' has been added!');
            }
        }
        return view("fb-campaigns.app-add");
    }

    public function get_web_page($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return $content;
    }


    public function addAccount(Request $request)
    {
        if($request->isMethod('post')){
            if($request->validate([

                'fb_app_id' => ['required', 'string'],
                'name' => ['required', 'string'],
                'account_id' => ['required', 'string']
            ])){
                $fbApp = new FbCampaignAccount;
                $fbApp->name = $request->name;
                $fbApp->fb_app_id = $request->fb_app_id;
                $fbApp->account_id = $request->account_id;

                $save = $fbApp->save();
                if ($save)
                    \Artisan::call('insight:get-campaigns '.$fbApp->account_id);
                return redirect('fb-campaigns/add-account') ->withSuccess(['The caption has been added!']);
            }
        }
        $fb_apps = FbCampaignApp::all();
        return view("fb-campaigns.acc-add", compact('fb_apps'));
    }
}
