<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\User;

class UserController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function add(Request $request)
    {
        if(Auth::getUser()->group != 1){
            return abort(404);
        }

        $groups = [
            [ 'id' => 1, 'name' => 'Administrator' ],
            [ 'id' => 2, 'name' => 'Content' ],
            [ 'id' => 3, 'name' => 'Sale' ]
        ];

        if($request->isMethod('post')){
            if($request->validate([
                'name' => ['required', 'string'],
                'email' => ['required', 'string'],
                'password' => ['required', 'string'],
                'group' => ['required'],
            ])){
                $user = User::where('email', '=', $request->input('email'))->first();
                if ($user === null) {
                    $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->group = $request->group;
                    $user->password = Hash::make($request->password);
                    $user->save();

                    return redirect('users/add') ->withSuccess(['The user has been added!']);
                }
                else{
                    return redirect('users/add') ->withErrors(['The email is already exist, please try another email!'])->withInput();
                }
            }
        }

        return view("users.add", compact('groups'));
    }

    public function list(){
        $limit = 20;
        $users = DB::table('users')->paginate(15);

        return view("users.list", compact('users'));
    }
}