<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(Request $request){
        return view('login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if(!$request->validate([
            'email' => ['required', 'string'],
            'password' => ['required', 'string'],
        ])){
            redirect()->back();
        }
        
        if (Auth::attempt($credentials)) {
            return redirect()->intended('dashboard');
        }
        else{
            return redirect()->back()->withErrors(['The email or password is invalid!']);
        }
    }
}