<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Yajra\DataTables\DataTables;
use App\Order;
use App\FbCampaignAllTimeInsight;

class ShopifyController extends Controller
{
    public function __construct()
    {

    }

    public function orders(Request $request){

        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-15 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime('-1 day'));
        $act = !empty($request->input('act')) ? $request->input('act') : '';
        $keyword = !empty($request->input('keyword')) ? $request->input('keyword') : '';

        $orders = DB::table('orders')->select('*');

        if ($keyword) {
            $orders->where('name', 'LIKE', "%{$keyword}%");
        }
        if (!empty($start) && !empty($end)) {
            $orders->whereBetween('created_at', [$start, $end]);
        }

        $orders = $orders->get();
        return view("shopify.orders", compact(
            'orders', 'keyword', 'start', 'end', 'act'));
    }

    public function anyData(Request $request){

        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-14 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime('-7 day'));
        $act = !empty($request->input('act')) ? $request->input('act') : '';

        if ($act) {
            $campaigns = FbCampaign::with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }])->where('account_id', $act)->get();
        } else {
            $campaigns = FbCampaign::with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }])->get();
        }
        $campaigns_data = Datatables::of($campaigns)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                return '<a href="'. route('fb-campaigns.detail', $row->id) .'" class="btn btn-xs btn-warning">Detail</a>';
            })
            ->addColumn('results', function($row){
                return $row->campaignInsight->sum('results');
            })
            ->addColumn('spend', function($row){
                return round($row->campaignInsight->sum('spend'), 2);
            })
            ->addColumn('unique_link_clicks_ctr', function($row){
                return round($row->campaignInsight->sum('unique_link_clicks_ctr'), 2);
            })
            ->addColumn('cost_per_unique_click', function($row){
                return round($row->campaignInsight->sum('cost_per_unique_click'), 2);
            })
            ->addColumn('cost_per_unique_add_to_cart', function($row){
                return round($row->campaignInsight->sum('cost_per_unique_add_to_cart'), 2);
            })
            ->addColumn('unique_adds_to_cart', function($row){
                return round($row->campaignInsight->sum('unique_adds_to_cart'), 2);
            })
            ->addColumn('cpm', function($row){
                return round($row->campaignInsight->sum('cpm'), 2);
            })
            ->addColumn('purchase', function($row){
                return $row->campaignInsight->sum('purchase');
            })
            ->addColumn('clicks', function($row){
                return $row->campaignInsight->sum('clicks');
            })
            ->addColumn('impressions', function($row){
                return $row->campaignInsight->sum('impressions');
            })
            ->addColumn('unique_clicks', function($row){
                return $row->campaignInsight->sum('unique_clicks');
            })
            ->addColumn('created_at', function($row){
                return date('Y-m-d', strtotime($row->created_at));
            })
            ->rawColumns(['action'])
            ->make(true);
        return $campaigns_data;
    }

    public function detail($id) {
        //echo date('Y-m-d', strtotime('-4 day'));die;
        $result = FbCampaignInsight::where('fb_campaign_id', $id)->where('time_range', '>', date('Y-m-d', strtotime('-5 day')))
            ->orderBy('time_range', 'desc')->get();
        $last_3day = FbCampaignInsight::where('fb_campaign_id', $id)->where('time_range', '>', date('Y-m-d', strtotime('-4 day')))
            ->selectRaw("SUM(results) as results")
            ->selectRaw("SUM(spend) as spend")
            ->selectRaw("SUM(unique_link_clicks_ctr) as unique_link_clicks_ctr")
            ->selectRaw("SUM(cost_per_unique_click) as cost_per_unique_click")
            ->selectRaw("SUM(cost_per_unique_add_to_cart) as cost_per_unique_add_to_cart")
            ->selectRaw("SUM(unique_adds_to_cart) as unique_adds_to_cart")
            ->selectRaw("SUM(cpm) as cpm")
            ->selectRaw("SUM(purchase) as purchase")
            ->groupBy('fb_campaign_id')
            ->first();
        $last_7day = FbCampaignInsight::where('fb_campaign_id', $id)->where('time_range', '>', date('Y-m-d', strtotime('-8 day')))
            ->selectRaw("SUM(results) as results")
            ->selectRaw("SUM(spend) as spend")
            ->selectRaw("SUM(unique_link_clicks_ctr) as unique_link_clicks_ctr")
            ->selectRaw("SUM(cost_per_unique_click) as cost_per_unique_click")
            ->selectRaw("SUM(cost_per_unique_add_to_cart) as cost_per_unique_add_to_cart")
            ->selectRaw("SUM(unique_adds_to_cart) as unique_adds_to_cart")
            ->selectRaw("SUM(cpm) as cpm")
            ->selectRaw("SUM(purchase) as purchase")
            ->groupBy('fb_campaign_id')
            ->first();

        $campaign_total = FbCampaignInsight::where('fb_campaign_id', $id)
            ->selectRaw("SUM(results) as results")
            ->selectRaw("SUM(spend) as spend")
            ->selectRaw("SUM(unique_link_clicks_ctr) as unique_link_clicks_ctr")
            ->selectRaw("SUM(cost_per_unique_click) as cost_per_unique_click")
            ->selectRaw("SUM(cost_per_unique_add_to_cart) as cost_per_unique_add_to_cart")
            ->selectRaw("SUM(unique_adds_to_cart) as unique_adds_to_cart")
            ->selectRaw("SUM(cpm) as cpm")
            ->selectRaw("SUM(purchase) as purchase")
            ->groupBy('fb_campaign_id')
            ->first();
        return view("fb-campaigns.detail", compact('last_7day', 'result', 'last_3day', 'campaign_total'));

    }

    public function appRefresh($id) {
        //echo date('Y-m-d', strtotime('-4 day'));die;
        $fbApp = FbCampaignApp::find($id);
        $FbCampaignApps = FbCampaignApp::all();
        if (!$fbApp) {
            return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('error', 'Your app has not been refresh successfully!');
        } else {
            $curl = 'https://graph.facebook.com/v10.0/'.$fbApp->user_id.'/adaccounts?limit=1000&access_token=' . $fbApp->access_token;
            $response = $this->get_web_page($curl);
            if ($response) {
                $resArr = json_decode($response);
                if (!empty($resArr->error->message)) {
                    return redirect('/fb-campaigns/apps')->with('error', $resArr->error->message);
                }
                if (count($resArr->data)) {

                    foreach ($resArr->data as $act) {
                        if (FbCampaignAccount::where('account_id', $act->account_id)->where('fb_app_id', $fbApp->id)->count())
                        {
                            continue;
                        }
                        $fbAcc = new FbCampaignAccount;
                        $fbAcc->name = $act->account_id;
                        $fbAcc->fb_app_id = $fbApp->id;
                        $fbAcc->account_id = $act->account_id;

                        $save = $fbAcc->save();

                        if ($save)
                        {
                            \Artisan::call('insight:get-campaigns '.$act->account_id.'_'.$fbApp->id);
                        }
                    }
                }
                return redirect('/fb-campaigns/apps')->with('success', 'Your app has been refresh successfully!');
            }
        }

        return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('success', 'Your app has been refresh successfully!');
    }

    public function appDelete($id) {
        //echo date('Y-m-d', strtotime('-4 day'));die;
        $fbApp = FbCampaignApp::find($id);
        $FbCampaignApps = FbCampaignApp::all();
        if (!$fbApp) {
            return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('error', 'Your app has not been delete successfully!');
        } else {
            FbCampaignAccount::where('fb_app_id', $fbApp->id)->delete();
            FbCampaignApp::where('id', $id)->delete();
            return redirect('fb-campaigns/apps')->with('success', 'Your app has been delete successfully!');
        }

        return view("fb-campaigns.apps", compact('FbCampaignApps'))->with('success', 'Your app has been delete successfully!');
    }

    public function apps(Request $request){
        $FbCampaignApps = FbCampaignApp::all();
        return view("fb-campaigns.apps", compact('FbCampaignApps'));
    }

    public function accounts(Request $request){
        $FbCampaignAccounts = FbCampaignAccount::with(['CampaignApp'])->get();
        return view("fb-campaigns.accounts", compact('FbCampaignAccounts'));
    }

    public function addApp(Request $request)
    {
        if($request->isMethod('post')){
            if($request->validate([
                'name' => ['required', 'string'],
                'app_id' => ['required', 'string'],
                'app_secret' => ['required', 'string'],
            ])){
                $fbApp = new FbCampaignApp;
                $fbApp->name = $request->name;
                $fbApp->app_id = $request->app_id;
                $fbApp->user_id = $request->user_id;
                $fbApp->app_secret = $request->app_secret;
                $fbApp->access_token = $request->access_token;

                $curl = 'https://graph.facebook.com/v10.0/oauth/access_token?grant_type=fb_exchange_token&client_id='.$request->app_id.'&client_secret='.$request->app_secret.'&fb_exchange_token=' . $request->access_token;
                $response = $this->get_web_page($curl);
                if ($response) {
                    $resArr = json_decode($response);
                    if (!empty($resArr->access_token)) {
                        $fbApp->access_token = $resArr->access_token;
                    }
                }
                $res = $fbApp->save();
                if ($res && !empty($request->user_id)) {
                    $curl = 'https://graph.facebook.com/v10.0/'.$request->user_id.'/adaccounts?access_token=' . $fbApp->access_token;
                    $response = $this->get_web_page($curl);
                    if ($response) {
                        $resArr = json_decode($response);
                        if (count($resArr->data)) {
                            foreach ($resArr->data as $act) {
                                $fbAcc = new FbCampaignAccount;
                                $fbAcc->name = $act->account_id;
                                $fbAcc->fb_app_id = $fbApp->id;
                                $fbAcc->account_id = $act->account_id;

                                $save = $fbAcc->save();
                                if ($save)
                                {
                                    \Artisan::call('insight:get-campaigns '.$act->account_id);
                                }
                            }
                        }
                    }
                }
                return redirect('/fb-campaigns/apps')->with('success', 'The  app ' . $fbApp->name . ' has been added!');
            }
        }
        return view("fb-campaigns.app-add");
    }

    public function get_web_page($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return $content;
    }


    public function addAccount(Request $request)
    {
        if($request->isMethod('post')){
            if($request->validate([

                'fb_app_id' => ['required', 'string'],
                'name' => ['required', 'string'],
                'account_id' => ['required', 'string']
            ])){
                $fbApp = new FbCampaignAccount;
                $fbApp->name = $request->name;
                $fbApp->fb_app_id = $request->fb_app_id;
                $fbApp->account_id = $request->account_id;

                $save = $fbApp->save();
                if ($save)
                    \Artisan::call('insight:get-campaigns '.$fbApp->account_id);
                return redirect('fb-campaigns/add-account') ->withSuccess(['The caption has been added!']);
            }
        }
        $fb_apps = FbCampaignApp::all();
        return view("fb-campaigns.acc-add", compact('fb_apps'));
    }
}
