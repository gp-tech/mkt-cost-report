<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\FbCampaign;
use App\FbCampaignAllTimeInsight;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CampaignController extends Controller
{
    protected $checkURL = 'https://teechip.com/manager/v2/?page=1';
    protected $settings = [];
    protected $campaignGroups = [];
    protected $sources = [
        'teechip' => 1
    ];
    //protected $payableId = '58bbc989174ee97e866899fc';
    protected $payableId = '58cbf716cb31924787fd717e';
    protected $userId = '6020c90f795b6265fa1ca099';

    public function __construct()
    {
        $this->settings = json_decode(file_get_contents('../storage/setting.json'), true);
        $this->settings['filters'] = $this->settings['filters'];
    }


    public function report(Request $request)
    {
        date_default_timezone_set('America/Phoenix');
        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-3 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime(now()));
        //$end = date('Y-m-d', strtotime($end . '+1 days'));
        if ($start == '2017-01-01') {
            $table = 'campaigns';
        } elseif ($start < '2021-03-21') {
            $table = 'tee_campaign_custom_insights';
        } else {
            $table = 'tee_campaign_insights';
        }

        $tee_tottal_result = DB::table($table)
            ->select(
                DB::raw("SUM(view) as total_view"),
                DB::raw("SUM(orders) as total_orders"),
                DB::raw("SUM(conversion) as total_conversion"),
                DB::raw("SUM(unit_sales) as total_unit_sales"),
                DB::raw("SUM(total_profit) as total_profits")
            );
        $tee_result = DB::table($table)->select('*');

        $keyword1 = $request->input('keyword1');
        $keyword2 = $request->input('keyword2');
        $keywords = explode(' ', $keyword1);
        $keywords2 = explode(' ', $keyword2);
        $all_time = false;
        $end_ = '';
        if ($start == '2017-01-01') {
            $all_time = true;
            $endTime = date('Y-m-d', strtotime(now() . '+1 days'));
            $param = '{"operationName":"overviewStats","variables":{"payableId":"58cbf716cb31924787fd717e","dates":{"fieldName":"createdAt","start":"2017-01-01T08:00:00.000Z","end":"'.$endTime.'T06:59:59.999Z","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":1,"search":"'.$keyword1.'"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
            $tee_data = $this->scrapeCustomAll($start, $end, $param, $keyword2);
            $campaigns_total = new \stdClass;
            $campaigns_total->total_view = 0;
            $campaigns_total->total_orders = 0;
            $campaigns_total->total_conversion = 0;
            $campaigns_total->total_unit_sales = 0;
            $campaigns_total->total_profits = 0;

            foreach ($tee_data as $tee_datum) {
                $campaigns_total->total_view += $tee_datum['view'];
                $campaigns_total->total_orders += $tee_datum['orders'];
                $campaigns_total->total_conversion += $tee_datum['conversion'];
                $campaigns_total->total_unit_sales += $tee_datum['unit_sales'];
                $campaigns_total->total_profits += $tee_datum['total_profit'];
            }
            $tee_campaigns = '';//$tee_result->get();
        } elseif ($start < '2021-03-21') {
            $tee_data = $this->scrapeCustom($start, $end, $keyword1, $keyword2);
            $campaigns_total = new \stdClass;
            $campaigns_total->total_view = 0;
            $campaigns_total->total_orders = 0;
            $campaigns_total->total_conversion = 0;
            $campaigns_total->total_unit_sales = 0;
            $campaigns_total->total_profits = 0;

            foreach ($tee_data as $tee_datum) {
                $campaigns_total->total_view += $tee_datum['view'];
                $campaigns_total->total_orders += $tee_datum['orders'];
                $campaigns_total->total_conversion += $tee_datum['conversion'];
                $campaigns_total->total_unit_sales += $tee_datum['unit_sales'];
                $campaigns_total->total_profits += $tee_datum['total_profit'];
            }
            $tee_campaigns = '';//$tee_result->get();
        }
        else {
            //not in time database
            if (!empty($start) && !empty($end)) {
                $tee_result->whereBetween('time_range', [$start, $end]);
                $tee_tottal_result->whereBetween('time_range', [$start, $end]);
            }

            if ($keyword2) {
                foreach ($keywords2 as $keyword_2) {
                    $tee_result->where('url', 'NOT LIKE', "%{$keyword_2}%");
                    $tee_tottal_result->where('url', 'NOT LIKE', "%{$keyword_2}%");
                }

            }
            if ($keyword1) {
                foreach ($keywords as $keyword) {
                    $tee_result->where('url', 'LIKE', "%{$keyword}%");
                    $tee_tottal_result->where('url', 'LIKE', "%{$keyword}%");
                }
            }

            $campaigns_total = $tee_tottal_result->first();
            $tee_campaigns = $tee_result->get();
            $tee_data = [];
            foreach ($tee_campaigns as $tee_campaign) {
                if (isset($tee_data[$tee_campaign->campaign_id])) {
                    $tee_data[$tee_campaign->campaign_id]['view'] += $tee_campaign->view;
                    $tee_data[$tee_campaign->campaign_id]['orders'] += $tee_campaign->orders;
                    $tee_data[$tee_campaign->campaign_id]['conversion'] += $tee_campaign->conversion;
                    $tee_data[$tee_campaign->campaign_id]['unit_sales'] += $tee_campaign->unit_sales;
                    $tee_data[$tee_campaign->campaign_id]['total_profit'] += $tee_campaign->total_profit;
                } else {
                    $tee_data[$tee_campaign->campaign_id]['url'] = $tee_campaign->url;
                    $tee_data[$tee_campaign->campaign_id]['name'] = $tee_campaign->name;
                    $tee_data[$tee_campaign->campaign_id]['view'] = $tee_campaign->view;
                    $tee_data[$tee_campaign->campaign_id]['orders'] = $tee_campaign->orders;
                    $tee_data[$tee_campaign->campaign_id]['conversion'] = $tee_campaign->conversion;
                    $tee_data[$tee_campaign->campaign_id]['unit_sales'] = $tee_campaign->unit_sales;
                    $tee_data[$tee_campaign->campaign_id]['total_profit'] = $tee_campaign->total_profit;
                }
            }
        }




        //fb
        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-3 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime('-1 day'));
        $act = !empty($request->input('act')) ? $request->input('act') : '';
        $total = [];
        $total['results'] = $total['spend'] = $total['unique_link_clicks_ctr'] = $total['unique_adds_to_cart'] = 0;
        $total['cpm'] = $total['purchase'] = $total['clicks'] = $total['impressions'] = $total['unique_clicks'] = 0;

        $campaigns = New FbCampaign();

        if ($keyword1) {

            foreach ($keywords as $keyword) {
                $campaigns = $campaigns->where('name', 'LIKE', "%{$keyword}%");
            }
        }

        if ($act) {
            $campaigns = $campaigns->with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }])->where('account_id', $act);
        } else {
            $campaigns = $campaigns->with(['campaignInsight' => function ($query ) use($start, $end){
                $query->whereBetween('fb_campaign_insights.time_range', [$start, $end]);
            }]);
        }
        $fb_campaigns = $campaigns->get();
        foreach ($fb_campaigns as $campaign) {
            $total['results'] += $campaign->campaignInsight->sum('results');
            $total['spend'] += $campaign->campaignInsight->sum('spend');
            $total['unique_link_clicks_ctr'] += $campaign->campaignInsight->sum('unique_link_clicks_ctr');
            $total['unique_adds_to_cart'] += $campaign->campaignInsight->sum('unique_adds_to_cart');
            $total['cpm'] += $campaign->campaignInsight->sum('cpm');
            $total['purchase'] += $campaign->campaignInsight->sum('purchase');
            $total['clicks'] += $campaign->campaignInsight->sum('clicks');
            $total['impressions'] += $campaign->campaignInsight->sum('impressions');
            $total['unique_clicks'] += $campaign->campaignInsight->sum('unique_clicks');
        }

        return view("campaigns.report", compact('start', 'end', 'keyword1', 'keyword2',
            'campaigns_total', 'tee_campaigns', 'fb_campaigns', 'total', 'tee_data'
        ));
    }

    public function index(Request $request)
    {
        date_default_timezone_set('America/Los_Angeles');
        $start = !empty($request->input('start_date')) ? $request->input('start_date') : date('Y-m-d', strtotime('-3 day'));
        $end = !empty($request->input('end_date')) ? $request->input('end_date') : date('Y-m-d', strtotime(now()));
        //$end = date('Y-m-d', strtotime($end . '+1 days'));

        if ($start == '2017-01-01') {
            $table = 'campaigns';
        } elseif ($start < '2021-03-21') {
            $table = 'tee_campaign_custom_insights';
        } else {
            $table = 'tee_campaign_insights';
        }

        $tee_tottal_result = DB::table($table)
            ->select(
                DB::raw("SUM(view) as total_view"),
                DB::raw("SUM(orders) as total_orders"),
                DB::raw("SUM(conversion) as total_conversion"),
                DB::raw("SUM(unit_sales) as total_unit_sales"),
                DB::raw("SUM(total_profit) as total_profits")
            );


        $tee_result = DB::table($table)->select('*');

        $keyword1 = $request->input('keyword1');
        $keyword2 = $request->input('keyword2');
        $keywords = explode(' ', $keyword1);
        $keywords2 = explode(' ', $keyword2);
        $all_time = false;
        $end_ = '';
        if ($start == '2017-01-01') {
            $all_time = true;
            $endTime = date('Y-m-d', strtotime(now() . '+1 days'));
            $param = '{"operationName":"overviewStats","variables":{"payableId":"58cbf716cb31924787fd717e","dates":{"fieldName":"createdAt","start":"2017-01-01T08:00:00.000Z","end":"'.$endTime.'T06:59:59.999Z","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":1,"search":"'.$keyword1.'"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
            $campaigns = $this->scrapeCustomAll($start, $end, $param, $keyword2);
            return view("campaigns.search", compact('start', 'end', 'campaigns', 'keyword1', 'keyword2'));
        } elseif ($start < '2021-03-21') {
            $campaigns = $this->scrapeCustom($start, $end, $keyword1, $keyword2);
            return view("campaigns.search", compact('start', 'end', 'campaigns', 'keyword1', 'keyword2'));
        }
        else {
            //not in time database
            $end_ = date('Y-m-d', strtotime($end . '+1 days'));
            $param = '{"operationName":"overviewStats","variables":{"payableId":"58cbf716cb31924787fd717e","dates":{"fieldName":"createdAt","start":"'.$start.'T07:00:00.000Z","end":"'.$end_.'T06:59:59.999Z","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":1,"search":"'.$keywords[0].'"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';

            if (!empty($start) && !empty($end)) {
                $tee_result->whereBetween('time_range', [$start, $end]);
                $tee_tottal_result->whereBetween('time_range', [$start, $end]);
            }
        }

        if ($keyword2) {
            foreach ($keywords2 as $keyword_2) {
                $tee_result->where('url', 'NOT LIKE', "%{$keyword_2}%");
                $tee_tottal_result->where('url', 'NOT LIKE', "%{$keyword_2}%");
            }

        }
        if ($keyword1) {
//            $check = DB::table('campaigns')->where('url', 'LIKE', "%{$keywords[0]}%")->first();
//            if (!$check) {
//                $this->scrapeCustom($param, 50, $all_time, $start, $end_);
//            }

            foreach ($keywords as $keyword) {
                $tee_result->where('url', 'LIKE', "%{$keyword}%");
                $tee_tottal_result->where('url', 'LIKE', "%{$keyword}%");
            }
        }

        $campaigns_total = $tee_tottal_result->first();
        $campaigns_data = $tee_result->get();
        $campaigns = [];
        foreach ($campaigns_data as $campaign)
        {
            if (isset($campaigns[$campaign->campaign_id]['url'])) {
                $campaigns[$campaign->campaign_id]['view'] += $campaign->view;
                $campaigns[$campaign->campaign_id]['orders'] += $campaign->orders;
                $campaigns[$campaign->campaign_id]['conversion'] += $campaign->conversion;
                $campaigns[$campaign->campaign_id]['unit_sales'] += $campaign->unit_sales;
                $campaigns[$campaign->campaign_id]['total_profit'] += $campaign->total_profit;
            } else {
                $campaigns[$campaign->campaign_id]['view'] = $campaign->view;
                $campaigns[$campaign->campaign_id]['orders'] = $campaign->orders;
                $campaigns[$campaign->campaign_id]['conversion'] = $campaign->conversion;
                $campaigns[$campaign->campaign_id]['unit_sales'] = $campaign->unit_sales;
                $campaigns[$campaign->campaign_id]['total_profit'] = $campaign->total_profit;

                $campaigns[$campaign->campaign_id]['url'] = $campaign->url;
                $campaigns[$campaign->campaign_id]['pixel'] = $campaign->pixel;
                $campaigns[$campaign->campaign_id]['status'] = $campaign->status;
                $campaigns[$campaign->campaign_id]['pixel'] = $campaign->pixel;
                $campaigns[$campaign->campaign_id]['time_range'] = $campaign->time_range;
            }

        }
        return view("campaigns.index", compact('start', 'end', 'campaigns_total', 'campaigns', 'keyword1', 'keyword2'));
    }

    public function getAll(Request $request, $maxPage = 0)
    {
        $this->scrapeAll($request, 50);

        $response = [
            'true'
        ];

        return response()->json($response);
    }

    public function cron(Request $request)
    {
        $this->scrape($request, 5);

        $response = [
            'true'
        ];

        return response()->json($response);
    }

    private function postJson($url, $params)
    {
        $ch = curl_init();

        $headers = [
            'Accept: */*',
            //'Accept-Encoding: gzip, deflate, br',
            'Accept-Language: en-US,en;q=0.5',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            //'Content-Length: 3220',
            'content-type: application/json',
            'Cookie: ' . $this->settings['cookie'],
            'DNT: 1',
            'Host: teechip.com',
            'Origin: https://teechip.com',
            'Pragma: no-cache',
            //'Referer: https://teechip.com/manager/v2/?page=1&timePeriod=today&startDate=1600153200000&endDate=1600239599999',
            'sl-auth-token: USECOOKIE',
            'X-XSRF-TOKEN: ' . $this->settings['token'],
        ];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($this->startsWith($url, 'https://')) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_CAINFO, '../storage/cacert.pem');
        }

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    private function startsWith($str, $needle)
    {
        $length = strlen($needle);
        return (substr($str, 0, $length) === $needle);
    }

    private function scrape(Request $request, $maxPage = 0)
    {
        set_time_limit(0);
        date_default_timezone_set('America/Los_Angeles');

        $range_time = date('Y-m-d', strtotime(now()));
        //get yesterday data
        $yesterday_campaign = \App\TeeCampaignInsight::where('time_range', $range_time)->first();
        if (empty($yesterday_campaign->id)) {
            $check = $this->scrapeYesterday($request, 5);
            if ($check) {
                $endTime = date('Y-m-d', strtotime($range_time . '+1 days'));
                $params = '{"operationName":"getUserGroups","variables":{"userId":"' . $this->userId . '"},"query":"query getUserGroups($userId: String!) {\n  groupsByUserId(userId: $userId) {\n    domain\n    id\n    __typename\n  }\n}\n"}';

                $url = 'https://teechip.com/gateway/gql';
                $data = json_decode($this->postJson($url, $params), true);
                $groups = [];
                $page = 1;
                if (!empty($data['data']['groupsByUserId'])) {
                    foreach ($data['data']['groupsByUserId'] as $d) {
                        $groups[$d['id']] = 'https://' . $d['domain'] . '/';
                    }
                }

                $url_data = 'https://teechip.com/gateway/gql';
                $startTime = "{$range_time}T07:00:00.000Z";
                $endTime = "{$endTime}T06:59:59.999Z";
                foreach ($groups as $d => $name) {
                    $params = '{"operationName":"overviewStats","variables":{"payableId":"' . $this->payableId . '","dates":{"fieldName":"createdAt","start":"' . $startTime . '","end":"' . $endTime . '","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"'.$d.'","search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                    $data = json_decode($this->postJson($url_data, $params), true);
                    if (empty($data['data']['overviewStats']['results']['campaignData'])) {
                        continue;
                    } else {
                        $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                        $campaignDetails = $data['data']['overviewStats']['results']['campaignDetails'];
                        $totalCampaign = count($campaigns);

                        for ($i = 0; $i < $totalCampaign; $i++) {
                            $c = $campaigns[$i];

                            if ($c['views'] > 0) {
                                $pixel = !empty($campaignDetails[$i]['social']['trackingTags']['facebookPixelId']) ? $campaignDetails[$i]['social']['trackingTags']['facebookPixelId'] : '';
                                $url = !empty($groups[$c['groupId']]) ? $groups[$c['groupId']] : '';
                                if (!empty($campaignDetails[$i]['url'])) {
                                    $url .= $campaignDetails[$i]['url'];
                                }

                                $campaign = Campaign::where('campaign_id', '=', $c['campaignId'])->first();
                                $check = false;
                                if ($campaign == null) {
                                    $c_params = '{"operationName":"overviewStats","variables":{"payableId":"' . $this->payableId . '","dates":{"fieldName":"createdAt","start":"2017-01-01T08:00:00.000Z","end":"' . $endTime . '","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"' . $d . '","search":"' . $campaignDetails[$i]['url'] . '"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                                    $c_url = 'https://teechip.com/gateway/gql';
                                    $campaign_data = json_decode($this->postJson($c_url, $c_params), true);
                                    $d_campaigns = $campaign_data['data']['overviewStats']['results']['campaignData'];
                                    $c_campaignDetails = $campaign_data['data']['overviewStats']['results']['campaignDetails'];

                                    $cc = $d_campaigns[0];
                                    $pixel = !empty($c_campaignDetails[0]['social']['trackingTags']['facebookPixelId']) ? $c_campaignDetails[0]['social']['trackingTags']['facebookPixelId'] : '';
                                    $url = !empty($groups[$c['groupId']]) ? $groups[$cc['groupId']] : '';
                                    if (!empty($c_campaignDetails[0]['url'])) {
                                        $url .= $c_campaignDetails[0]['url'];
                                    }
                                    $cinsert = [];
                                    $cinsert['campaign_id'] = $cc['campaignId'];
                                    $cinsert['name'] = $c_campaignDetails[0]['title'];
                                    $cinsert['url'] = $url;
                                    $cinsert['view'] = $cc['views'];
                                    $cinsert['orders'] = $cc['orderCount'];
                                    $cinsert['conversion'] = $cc['conversionRate'];
                                    $cinsert['unit_sales'] = $cc['itemCount'];
                                    $cinsert['total_profit'] = floatval($cc['profit']) * 0.01;
                                    $cinsert['pixel'] = $pixel;
                                    $cinsert['status'] = !empty($c_campaignDetails[0]['status']) && $c_campaignDetails[0]['status'] == 'active' ? 1 : 0;
                                    $cinsert['time_range'] = date('Y-m-d');
                                    $check = \App\Campaign::forceCreate(
                                        $cinsert
                                    );
                                } else {
                                    $check = true;
                                    $tee_campaign = \App\TeeCampaignInsight::where('campaign_id', $c['campaignId'])
                                        ->where('time_range', $range_time)->first();
                                    if (!empty($tee_campaign->id)) {
                                        $campaign->view += isset($tee_campaign->view) ? $c['views'] - $tee_campaign->view  : $c['views'];
                                        $campaign->orders += isset($tee_campaign->orders) ? $c['orderCount'] - $tee_campaign->orders : $c['orderCount'];
                                        $campaign->conversion += isset($tee_campaign->conversion) ? $c['conversionRate'] - $tee_campaign->conversion : $c['conversionRate'];
                                        $campaign->unit_sales += isset($tee_campaign->unit_sales) ? $c['itemCount'] - $tee_campaign->unit_sales : $c['itemCount'];
                                        $campaign->total_profit += isset($tee_campaign->total_profit) ? (floatval($c['profit']) * 0.01) - $tee_campaign->total_profit : floatval($c['profit']) * 0.01;
                                        $check = $campaign->save();
                                    }
                                }

                                if ($check) {
                                    $insert = [];
                                    $insert['campaign_id'] = $c['campaignId'];;//$campaign->id;
                                    $insert['url'] = $url;
                                    $insert['view'] = $c['views'];
                                    $insert['name'] = $campaignDetails[$i]['title'];
                                    $insert['pixel'] = $pixel;
                                    $insert['orders'] = $c['orderCount'];
                                    $insert['conversion'] = $c['conversionRate'];
                                    $insert['unit_sales'] = $c['itemCount'];
                                    $insert['total_profit'] = floatval($c['profit']) * 0.01;
                                    $insert['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                                    $insert['time_range'] = $range_time;
                                    \App\TeeCampaignInsight::updateOrCreate(
                                        ['time_range' => $range_time, 'campaign_id' => $c['campaignId']],
                                        $insert
                                    );
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $endTime = date('Y-m-d', strtotime($range_time . '+1 days'));
            $params = '{"operationName":"getUserGroups","variables":{"userId":"' . $this->userId . '"},"query":"query getUserGroups($userId: String!) {\n  groupsByUserId(userId: $userId) {\n    domain\n    id\n    __typename\n  }\n}\n"}';

            $url = 'https://teechip.com/gateway/gql';
            $data = json_decode($this->postJson($url, $params), true);
            $groups = [];
            $page = 1;
            if (!empty($data['data']['groupsByUserId'])) {
                foreach ($data['data']['groupsByUserId'] as $d) {
                    $groups[$d['id']] = 'https://' . $d['domain'] . '/';
                }
            }

            $url_data = 'https://teechip.com/gateway/gql';
            $startTime = "{$range_time}T07:00:00.000Z";
            $endTime = "{$endTime}T06:59:59.999Z";
            foreach ($groups as $d => $name) {
                $params = '{"operationName":"overviewStats","variables":{"payableId":"' . $this->payableId . '","dates":{"fieldName":"createdAt","start":"' . $startTime . '","end":"' . $endTime . '","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"'.$d.'","search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                $data = json_decode($this->postJson($url_data, $params), true);
                if (empty($data['data']['overviewStats']['results']['campaignData'])) {
                    continue;
                } else {
                    $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                    $campaignDetails = $data['data']['overviewStats']['results']['campaignDetails'];
                    $totalCampaign = count($campaigns);

                    for ($i = 0; $i < $totalCampaign; $i++) {
                        $c = $campaigns[$i];

                        if ($c['views'] > 0) {
                            $pixel = !empty($campaignDetails[$i]['social']['trackingTags']['facebookPixelId']) ? $campaignDetails[$i]['social']['trackingTags']['facebookPixelId'] : '';
                            $url = !empty($groups[$c['groupId']]) ? $groups[$c['groupId']] : '';
                            if (!empty($campaignDetails[$i]['url'])) {
                                $url .= $campaignDetails[$i]['url'];
                            }

                            $campaign = Campaign::where('campaign_id', '=', $c['campaignId'])->first();
                            $check = false;
                            if ($campaign == null) {
                                $c_params = '{"operationName":"overviewStats","variables":{"payableId":"' . $this->payableId . '","dates":{"fieldName":"createdAt","start":"2017-01-01T08:00:00.000Z","end":"' . $endTime . '","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"' . $d . '","search":"' . $campaignDetails[$i]['url'] . '"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                                $c_url = 'https://teechip.com/gateway/gql';
                                $campaign_data = json_decode($this->postJson($c_url, $c_params), true);
                                $d_campaigns = $campaign_data['data']['overviewStats']['results']['campaignData'];
                                $c_campaignDetails = $campaign_data['data']['overviewStats']['results']['campaignDetails'];

                                $cc = $d_campaigns[0];
                                $pixel = !empty($c_campaignDetails[0]['social']['trackingTags']['facebookPixelId']) ? $c_campaignDetails[0]['social']['trackingTags']['facebookPixelId'] : '';
                                $url = !empty($groups[$c['groupId']]) ? $groups[$cc['groupId']] : '';
                                if (!empty($c_campaignDetails[0]['url'])) {
                                    $url .= $c_campaignDetails[0]['url'];
                                }
                                $cinsert = [];
                                $cinsert['campaign_id'] = $cc['campaignId'];
                                $cinsert['name'] = $c_campaignDetails[0]['title'];
                                $cinsert['url'] = $url;
                                $cinsert['view'] = $cc['views'];
                                $cinsert['orders'] = $cc['orderCount'];
                                $cinsert['conversion'] = $cc['conversionRate'];
                                $cinsert['unit_sales'] = $cc['itemCount'];
                                $cinsert['total_profit'] = floatval($cc['profit']) * 0.01;
                                $cinsert['pixel'] = $pixel;
                                $cinsert['status'] = !empty($c_campaignDetails[0]['status']) && $c_campaignDetails[0]['status'] == 'active' ? 1 : 0;
                                $cinsert['time_range'] = date('Y-m-d');
                                $check = \App\Campaign::forceCreate(
                                    $cinsert
                                );
                            } else {
                                $check = true;
                                $tee_campaign = \App\TeeCampaignInsight::where('campaign_id', $c['campaignId'])
                                    ->where('time_range', $range_time)->first();
                                if (!empty($tee_campaign->id)) {
                                    $campaign->view += isset($tee_campaign->view) ? $c['views'] - $tee_campaign->view  : $c['views'];
                                    $campaign->orders += isset($tee_campaign->orders) ? $c['orderCount'] - $tee_campaign->orders : $c['orderCount'];
                                    $campaign->conversion += isset($tee_campaign->conversion) ? $c['conversionRate'] - $tee_campaign->conversion : $c['conversionRate'];
                                    $campaign->unit_sales += isset($tee_campaign->unit_sales) ? $c['itemCount'] - $tee_campaign->unit_sales : $c['itemCount'];
                                    $campaign->total_profit += isset($tee_campaign->total_profit) ? (floatval($c['profit']) * 0.01) - $tee_campaign->total_profit : floatval($c['profit']) * 0.01;
                                    $check = $campaign->save();
                                }
                            }

                            if ($check) {
                                $insert = [];
                                $insert['campaign_id'] = $c['campaignId'];;//$campaign->id;
                                $insert['url'] = $url;
                                $insert['view'] = $c['views'];
                                $insert['name'] = $campaignDetails[$i]['title'];
                                $insert['pixel'] = $pixel;
                                $insert['orders'] = $c['orderCount'];
                                $insert['conversion'] = $c['conversionRate'];
                                $insert['unit_sales'] = $c['itemCount'];
                                $insert['total_profit'] = floatval($c['profit']) * 0.01;
                                $insert['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                                $insert['time_range'] = $range_time;
                                \App\TeeCampaignInsight::updateOrCreate(
                                    ['time_range' => $range_time, 'campaign_id' => $c['campaignId']],
                                    $insert
                                );
                            }
                        }
                    }
                }
            }
        }
        /*for ($k = 2; $k < 4; $k++) {
               $j = $k + 1;
               $startTime = "2021-04-{$k}T07:00:00.000Z";
               $endTime = "2021-04-{$j}T06:59:59.999Z";
               $range_time = "2021-04-{$k}";
       }*/
    }

    private function scrapeYesterday(Request $request, $maxPage = 0)
    {
        set_time_limit(0);
        date_default_timezone_set('America/Los_Angeles');
        //$range_time = '2021-04-02';
        //$endTime = '2021-04-03';
        $range_time = date('Y-m-d', strtotime(now() . '-1 days'));
        $endTime = date('Y-m-d', strtotime(now()));
        $params = '{"operationName":"getUserGroups","variables":{"userId":"' . $this->userId . '"},"query":"query getUserGroups($userId: String!) {\n  groupsByUserId(userId: $userId) {\n    domain\n    id\n    __typename\n  }\n}\n"}';

        $url = 'https://teechip.com/gateway/gql';
        $data = json_decode($this->postJson($url, $params), true);
        $groups = [];
        $page = 1;
        if (!empty($data['data']['groupsByUserId'])) {
            foreach ($data['data']['groupsByUserId'] as $d) {
                $groups[$d['id']] = 'https://' . $d['domain'] . '/';
            }
        }

        $url_data = 'https://teechip.com/gateway/gql';
        $startTime = "{$range_time}T07:00:00.000Z";
        $endTime = "{$endTime}T06:59:59.999Z";
        foreach ($groups as $d => $name) {
            $params = '{"operationName":"overviewStats","variables":{"payableId":"' . $this->payableId . '","dates":{"fieldName":"createdAt","start":"' . $startTime . '","end":"' . $endTime . '","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"'.$d.'","search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
            $data = json_decode($this->postJson($url_data, $params), true);

            if (empty($data['data']['overviewStats']['results']['campaignData'])) {
                continue;
            } else {
                $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                $campaignDetails = $data['data']['overviewStats']['results']['campaignDetails'];
                $totalCampaign = count($campaigns);
                $listCampaign = [];

                for ($i = 0; $i < $totalCampaign; $i++) {
                    $c = $campaigns[$i];

                    if ($c['views'] > 0) {
                        $pixel = !empty($campaignDetails[$i]['social']['trackingTags']['facebookPixelId']) ? $campaignDetails[$i]['social']['trackingTags']['facebookPixelId'] : '';
                        $url = !empty($groups[$c['groupId']]) ? $groups[$c['groupId']] : '';
                        if (!empty($campaignDetails[$i]['url'])) {
                            $url .= $campaignDetails[$i]['url'];
                        }

                        $campaign = Campaign::where('campaign_id', '=', $c['campaignId'])->first();

                        $check = false;
                        if ($campaign == null) {
                            $c_params = '{"operationName":"overviewStats","variables":{"payableId":"' . $this->payableId . '","dates":{"fieldName":"createdAt","start":"2017-01-01T08:00:00.000Z","end":"' . $endTime . '","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"'.$d.'","search":"' . $campaignDetails[$i]['url'] . '"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                            $c_url = 'https://teechip.com/gateway/gql';
                            $campaign_data = json_decode($this->postJson($c_url, $c_params), true);
                            $d_campaigns = $campaign_data['data']['overviewStats']['results']['campaignData'];
                            $c_campaignDetails = $campaign_data['data']['overviewStats']['results']['campaignDetails'];

                            $cc = $d_campaigns[0];
                            $pixel = !empty($c_campaignDetails[0]['social']['trackingTags']['facebookPixelId']) ? $c_campaignDetails[0]['social']['trackingTags']['facebookPixelId'] : '';
                            $url = !empty($groups[$c['groupId']]) ? $groups[$cc['groupId']] : '';
                            if (!empty($c_campaignDetails[0]['url'])) {
                                $url .= $c_campaignDetails[0]['url'];
                            }
                            $cinsert = [];
                            $cinsert['campaign_id'] = $cc['campaignId'];
                            $cinsert['name'] = $c_campaignDetails[0]['title'];
                            $cinsert['url'] = $url;
                            $cinsert['view'] = $cc['views'];
                            $cinsert['orders'] = $cc['orderCount'];
                            $cinsert['conversion'] = $cc['conversionRate'];
                            $cinsert['unit_sales'] = $cc['itemCount'];
                            $cinsert['total_profit'] = floatval($cc['profit']) * 0.01;
                            $cinsert['pixel'] = $pixel;
                            $cinsert['status'] = !empty($c_campaignDetails[0]['status']) && $c_campaignDetails[0]['status'] == 'active' ? 1 : 0;
                            $cinsert['time_range'] = date('Y-m-d');
                            $check = \App\Campaign::forceCreate(
                                $cinsert
                            );
                        } else {
                            $tee_campaign = \App\TeeCampaignInsight::where('campaign_id', $c['campaignId'])
                                ->where('time_range', $range_time)->first();

                            if (!empty($tee_campaign->id)) {
                                $campaign->view += isset($tee_campaign->view) ? $c['views'] - $tee_campaign->view  : $c['views'];
                                $campaign->orders += isset($tee_campaign->orders) ? $c['orderCount'] - $tee_campaign->orders : $c['orderCount'];
                                $campaign->conversion += isset($tee_campaign->conversion) ? $c['conversionRate'] - $tee_campaign->conversion : $c['conversionRate'];
                                $campaign->unit_sales += isset($tee_campaign->unit_sales) ? $c['itemCount'] - $tee_campaign->unit_sales : $c['itemCount'];
                                $campaign->total_profit += isset($tee_campaign->total_profit) ? (floatval($c['profit']) * 0.01) - $tee_campaign->total_profit : floatval($c['profit']) * 0.01;
                                $check = $campaign->save();
                            }
                        }

                        if ($check) {
                            $insert = [];
                            $insert['campaign_id'] = $c['campaignId'];;//$campaign->id;
                            $insert['url'] = $url;
                            $insert['view'] = $c['views'];
                            $insert['name'] = $campaignDetails[$i]['title'];
                            $insert['pixel'] = $pixel;
                            $insert['orders'] = $c['orderCount'];
                            $insert['conversion'] = $c['conversionRate'];
                            $insert['unit_sales'] = $c['itemCount'];
                            $insert['total_profit'] = floatval($c['profit']) * 0.01;
                            $insert['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                            $insert['time_range'] = $range_time;
                            \App\TeeCampaignInsight::updateOrCreate(
                                ['time_range' => $range_time, 'campaign_id' => $c['campaignId']],
                                $insert
                            );
                        }
                    }
                }
            }
        }

        return true;
    }

    private function scrapeAll(Request $request, $maxPage = 0)
    {
        set_time_limit(0);
        date_default_timezone_set('America/Phoenix');
        $params = '{"operationName":"getUserGroups","variables":{"userId":"' . $this->userId . '"},"query":"query getUserGroups($userId: String!) {\n  groupsByUserId(userId: $userId) {\n    domain\n    id\n    __typename\n  }\n}\n"}';
        $url = 'https://teechip.com/gateway/gql';
        $data = json_decode($this->postJson($url, $params), true);
        $groups = [];

        if (!empty($data['data']['groupsByUserId'])) {
            foreach ($data['data']['groupsByUserId'] as $d) {
                $groups[$d['id']] = 'https://' . $d['domain'] . '/';
            }
        }

        foreach ($groups as $d => $name) {
            $hasNext = true;
            $page = 1;
            while ($hasNext) {
                $params = '{"operationName":"overviewStats","variables":{"payableId":"58cbf716cb31924787fd717e","dates":{"fieldName":"createdAt","start":"2017-01-01T08:00:00.000Z","end":"2021-04-09T06:59:59.999Z","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":' . $page . ',"groupId":"' . $d . '","search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                $url = 'https://teechip.com/gateway/gql';
                $data = json_decode($this->postJson($url, $params), true);

                if (empty($data['data']['overviewStats']['results']['campaignData'])) {
                    $hasNext = false;
                    break;
                } else {
                    $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                    $campaignDetails = $data['data']['overviewStats']['results']['campaignDetails'];
                    $totalCampaign = count($campaigns);

                    for ($i = 0; $i < $totalCampaign; $i++) {
                        $c = $campaigns[$i];
                        $pixel = !empty($campaignDetails[$i]['social']['trackingTags']['facebookPixelId']) ? $campaignDetails[$i]['social']['trackingTags']['facebookPixelId'] : '';
                        $url = !empty($groups[$c['groupId']]) ? $groups[$c['groupId']] : '';

                        if (!empty($campaignDetails[$i]['url'])) {
                            $url .= $campaignDetails[$i]['url'];
                        }

                        if ($c['views'] < 1) {
                            $hasNext = false;
                            break;
                        }

                        $insert = [];
                        $insert['campaign_id'] = $c['campaignId'];
                        $insert['name'] = $campaignDetails[$i]['title'];
                        $insert['url'] = $url;
                        $insert['view'] = $c['views'];
                        $insert['orders'] = $c['orderCount'];
                        $insert['conversion'] = $c['conversionRate'];
                        $insert['unit_sales'] = $c['itemCount'];
                        $insert['total_profit'] = floatval($c['profit']) * 0.01;
                        $insert['pixel'] = $pixel;
                        $insert['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                        $insert['time_range'] = date('Y-m-d');
//                        \App\Campaign::updateOrCreate(
//                            ['campaign_id' => $c['campaignId']],
//                            $insert
//                        );
                        \App\Campaign::forceCreate(
                            $insert
                        );
                    }
                    $page++;
                    if ($page > $maxPage) {
                        $hasNext = false;
                        break;
                    }
                }
            }
        }
    }

    private function scrapeCustom($start, $end, $keyword, $keyword2)
    {
        set_time_limit(0);
        $params = '{"operationName":"getUserGroups","variables":{"userId":"' . $this->userId . '"},"query":"query getUserGroups($userId: String!) {\n  groupsByUserId(userId: $userId) {\n    domain\n    id\n    __typename\n  }\n}\n"}';
        $url = 'https://teechip.com/gateway/gql';
        $data = json_decode($this->postJson($url, $params), true);
        $groups = [];
        if (!empty($data['data']['groupsByUserId'])) {
            foreach ($data['data']['groupsByUserId'] as $d) {
                $groups[$d['id']] = 'https://' . $d['domain'] . '/';
            }
        }

        //foreach ($period as $key => $value) {
            //$startTime = $value->format('Y-m-d') ;
            $endTime = date('Y-m-d', strtotime($end . '+1 days'));
            $param = '{"operationName":"overviewStats","variables":{"payableId":"58cbf716cb31924787fd717e","dates":{"fieldName":"createdAt","start":"'.$start.'T08:00:00.000Z","end":"'.$endTime.'T06:59:59.999Z","timePeriod":"custom","timeZone":"America/Los_Angeles"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"views","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":1,"search":"'.$keyword.'"},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        customizations {\n          id\n          __typename\n        }\n        retailProducts {\n          id\n          code\n          designLineId {\n            inkpDesignId\n            __typename\n          }\n          __typename\n        }\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';

            $url = 'https://teechip.com/gateway/gql';
            $data = json_decode($this->postJson($url, $param), true);

            if (empty($data['data']['overviewStats']['results']['campaignData'])) {
                return [];
            }

            $campaigns = $data['data']['overviewStats']['results']['campaignData'];
            $campaignDetails = $data['data']['overviewStats']['results']['campaignDetails'];
            $totalCampaign = count($campaigns);
            $insert = [];

            for ($i = 0; $i < $totalCampaign; $i++) {
                $c = $campaigns[$i];
                $pixel = !empty($campaignDetails[$i]['social']['trackingTags']['facebookPixelId']) ? $campaignDetails[$i]['social']['trackingTags']['facebookPixelId'] : '';
                $url = !empty($groups[$c['groupId']]) ? $groups[$c['groupId']] : '';
                if (!empty($campaignDetails[$i]['url'])) {
                    $url .= $campaignDetails[$i]['url'];
                }

                if ($campaignDetails[$i]['url'] && !empty($keyword2)) {
                    $keywords2 = explode(' ', $keyword2);
                    foreach ($keywords2 as $keyword_) {
                        if(!str_contains($campaignDetails[$i]['url'], $keyword_)) {

                            $insert[$i]['campaign_id'] = $c['campaignId'];//$campaign->id;
                            $insert[$i]['url'] = $url;
                            $insert[$i]['view'] = $c['views'];
                            $insert[$i]['name'] = $campaignDetails[$i]['title'];
                            $insert[$i]['pixel'] = $pixel;
                            $insert[$i]['orders'] = $c['orderCount'];
                            $insert[$i]['conversion'] = $c['conversionRate'];
                            $insert[$i]['unit_sales'] = $c['itemCount'];
                            $insert[$i]['total_profit'] = floatval($c['profit']) * 0.01;
                            $insert[$i]['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                        }

                    }
                } else {

                    $insert[$i]['campaign_id'] = $c['campaignId'];//$campaign->id;
                    $insert[$i]['url'] = $url;
                    $insert[$i]['view'] = $c['views'];
                    $insert[$i]['name'] = $campaignDetails[$i]['title'];
                    $insert[$i]['pixel'] = $pixel;
                    $insert[$i]['orders'] = $c['orderCount'];
                    $insert[$i]['conversion'] = $c['conversionRate'];
                    $insert[$i]['unit_sales'] = $c['itemCount'];
                    $insert[$i]['total_profit'] = floatval($c['profit']) * 0.01;
                    $insert[$i]['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                }
            }

            return $insert;
        //}
    }
    private function scrapeCustomAll($start, $end, $param, $keyword2)
    {
        set_time_limit(0);
        $params = '{"operationName":"getUserGroups","variables":{"userId":"' . $this->userId . '"},"query":"query getUserGroups($userId: String!) {\n  groupsByUserId(userId: $userId) {\n    domain\n    id\n    __typename\n  }\n}\n"}';
        $url = 'https://teechip.com/gateway/gql';
        $data = json_decode($this->postJson($url, $params), true);
        $groups = [];
        if (!empty($data['data']['groupsByUserId'])) {
            foreach ($data['data']['groupsByUserId'] as $d) {
                $groups[$d['id']] = 'https://' . $d['domain'] . '/';
            }
        }

        $url = 'https://teechip.com/gateway/gql';
        $data = json_decode($this->postJson($url, $param), true);

        if (empty($data['data']['overviewStats']['results']['campaignData'])) {
            return [];
        }

        $campaigns = $data['data']['overviewStats']['results']['campaignData'];
        $campaignDetails = $data['data']['overviewStats']['results']['campaignDetails'];
        $totalCampaign = count($campaigns);
        $insert = [];
        for ($i = 0; $i < $totalCampaign; $i++) {
            $c = $campaigns[$i];
            $pixel = !empty($campaignDetails[$i]['social']['trackingTags']['facebookPixelId']) ? $campaignDetails[$i]['social']['trackingTags']['facebookPixelId'] : '';
            $url = !empty($groups[$c['groupId']]) ? $groups[$c['groupId']] : '';
            if (!empty($campaignDetails[$i]['url'])) {
                $url .= $campaignDetails[$i]['url'];
            }

            if ($campaignDetails[$i]['url'] && !empty($keyword2)) {
                $keywords2 = explode(' ', $keyword2);
                foreach ($keywords2 as $keyword_) {
                    if(!str_contains($campaignDetails[$i]['url'], $keyword_)) {

                        $insert[$i]['campaign_id'] = $c['campaignId'];//$campaign->id;
                        $insert[$i]['url'] = $url;
                        $insert[$i]['view'] = $c['views'];
                        $insert[$i]['name'] = $campaignDetails[$i]['title'];
                        $insert[$i]['pixel'] = $pixel;
                        $insert[$i]['orders'] = $c['orderCount'];
                        $insert[$i]['conversion'] = $c['conversionRate'];
                        $insert[$i]['unit_sales'] = $c['itemCount'];
                        $insert[$i]['total_profit'] = floatval($c['profit']) * 0.01;
                        $insert[$i]['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
                    }

                }
            } else {

                $insert[$i]['campaign_id'] = $c['campaignId'];//$campaign->id;
                $insert[$i]['url'] = $url;
                $insert[$i]['view'] = $c['views'];
                $insert[$i]['name'] = $campaignDetails[$i]['title'];
                $insert[$i]['pixel'] = $pixel;
                $insert[$i]['orders'] = $c['orderCount'];
                $insert[$i]['conversion'] = $c['conversionRate'];
                $insert[$i]['unit_sales'] = $c['itemCount'];
                $insert[$i]['total_profit'] = floatval($c['profit']) * 0.01;
                $insert[$i]['status'] = !empty($campaignDetails[$i]['status']) && $campaignDetails[$i]['status'] == 'active' ? 1 : 0;
            }
        }

        return $insert;
        //}
    }
}
