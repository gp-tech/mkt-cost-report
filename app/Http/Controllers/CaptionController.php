<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Facades\DB;

use App\Caption;

class CaptionController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function add(Request $request)
    {
        if($request->isMethod('post')){
            if($request->validate([
                'name' => ['required', 'string'],
                'content' => ['required', 'string'],
            ])){
                $caption = new Caption;
                $caption->name = $request->name;
                $caption->content = $request->content;
                $caption->default = $request->default;

                if(!empty($caption->default)){
                    DB::update('update captions set `default` = 0');
                }

                $caption->save();

                return redirect('captions/add') ->withSuccess(['The caption has been added!']);
            }
        }

        return view("captions.add");
    }

    public function edit(Request $request, $id)
    {
        $caption = Caption::where('id', '=', $id)->first();

        if($request->isMethod('post')){
            if($request->validate([
                'name' => ['required', 'string'],
                'content' => ['required', 'string'],
            ])){
                if ($caption != null) {
                    $caption->name = $request->name;
                    $caption->content = $request->content;
                    $caption->default = $request->default;

                    if(!empty($caption->default)){
                        DB::update('update captions set `default` = 0');
                    }

                    $caption->save();

                    return redirect('captions/edit/'. $caption->id) ->withSuccess(['The caption has been edited!']);
                }
                else{
                    return redirect('captions/edit/'. $id) ->withErrors(['Can\'t find the caption!'])->withInput();
                }
            }
        }

        return view("captions.edit", compact('caption'));
    }

    public function list(Request $request){
        $limit = 20;

        $page = 1;

        if(!empty($request->input('page'))){
            $page = $request->input('page');
        }

        $no = ($page - 1) * $limit + 1;

        $captions = Caption::paginate($limit);

        return view("captions.list", compact('captions', 'no'));
    }
}