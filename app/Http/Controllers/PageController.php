<?php

namespace App\Http\Controllers;

use App\Caption;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PageController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function add(Request $request)
    {
        $rules = [
            'name' => ['required', 'string'],
            'fb_id' => ['required', 'string'],
        ];

        if($request->isMethod('post')){
            if($request->validate($rules)){
                $page = Page::where('fb_id', '=', $request->input('fb_id'))->first();
                if ($page === null) {
                    $page = new Page;
                    $page->name = $request->name;
                    $page->fb_id = $request->fb_id;
                    $page->token = '';
                    $page->save();

                    return redirect('pages/list') ->withSuccess(['The '.$request->name.' page added!']);
                }
                else{
                    return redirect('pages/add') ->withErrors(['The FB ID is already exist!'])->withInput();
                }
            }
        }

        return view("pages.add");
    }

    public function list(Request $request){
        $limit = 20;
        $domain = env('MAIN_DOMAIN', false);
        $no     = 0;
        $pages = DB::table('pages')
            ->select('*')
            ->orderBy('id', 'desc')
            ->get();

        return view("pages.list", compact('pages', 'no'));
    }

}
