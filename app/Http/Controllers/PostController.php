<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use App\Post;
use App\Caption;
use App\Page;

class PostController extends Controller
{
    public function edit(Request $request, $id)
    {
        $post = Post::where('id', '=', $id)->first();
        $pages = Page::all();
        $captions = Caption::all();

        if($request->isMethod('post')){
            if($request->validate([
                'niche' => ['required', 'string'],
                'caption' => ['required', 'string'],
                'page' => ['required', 'string'],
            ])){
                if ($post != null) {
                    $post->niche = $request->input('niche');
                    $post->caption_id = $request->input('caption');
                    $post->page_id = $request->input('page');
                    $post->save();

                    return redirect('posts/edit/'. $post->id) ->withSuccess(['The post has been updated!']);
                }
            }
        }

        return view("posts.edit", compact('post', 'pages', 'captions'));
    }

    public function list(Request $request){
        $limit = 20;
        $domain = env('MAIN_DOMAIN', false);

        $createdDates = DB::table('posts')
        ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as value'))
        ->groupBy('value')
        ->orderBy('value', 'desc')
        ->get();

        $dates = [];
        foreach($createdDates as $date){
            $dates[] = [
                'value' => $date->value,
                'label' => $date->value
            ];
        }

        $page = 1;
        $currentDate = date_create('now')->format('Y-m-d');

        if(!empty($request->input('page'))){
            $page = $request->input('page');
        }

        if(empty($request->input('date')) && count($dates) > 0){
            $currentDate = $dates[0]['value'];
        }
        else if(!empty($request->input('date'))){
            $currentDate = $request->input('date');
        }

        $no = ($page - 1) * $limit + 1;

        $posts = Post::whereDate('created_at', '=', $currentDate)->paginate($limit);
        $captionIDs = [];

        foreach($posts as $post){
            $captionIDs[] = $post->caption_id;
        }

        $captionIDs = array_unique($captionIDs);

        $captions = Caption::whereIn('id', $captionIDs)->get();

        $captionTemplates = [];

        foreach($captions as $caption){
            $captionTemplates[$caption->id] = $caption->content;
        }

        return view("posts.list", compact('posts', 'dates', 'domain', 'no', 'captionTemplates', 'currentDate'));
    }

    public function token(Request $request){
        $limit = 20;
        $domain = env('MAIN_DOMAIN', false);

        $createdDates = DB::table('posts')
            ->select(DB::raw('DATE_FORMAT(created_at, "%Y-%m-%d") as value'))
            ->groupBy('value')
            ->orderBy('value', 'desc')
            ->get();

        $dates = [];
        foreach($createdDates as $date){
            $dates[] = [
                'value' => $date->value,
                'label' => $date->value
            ];
        }

        $page = 1;
        $currentDate = date_create('now')->format('Y-m-d');

        if(!empty($request->input('page'))){
            $page = $request->input('page');
        }

        if(empty($request->input('date')) && count($dates) > 0){
            $currentDate = $dates[0]['value'];
        }
        else if(!empty($request->input('date'))){
            $currentDate = $request->input('date');
        }

        $no = ($page - 1) * $limit + 1;

        $posts = Post::whereDate('created_at', '=', $currentDate)->paginate($limit);
        $captionIDs = [];

        foreach($posts as $post){
            $captionIDs[] = $post->caption_id;
        }

        $captionIDs = array_unique($captionIDs);

        $captions = Caption::whereIn('id', $captionIDs)->get();

        $captionTemplates = [];

        foreach($captions as $caption){
            $captionTemplates[$caption->id] = $caption->content;
        }

        return view("posts.token", compact('posts', 'dates', 'domain', 'no', 'captionTemplates', 'currentDate'));
    }

    public function uploadForm(Request $request){

        return view("posts.upload");
    }

    public function upload(Request $request){

        $input = $request->all();
        $images=array();
        $caption = Caption::where('default', '=', 1)->first();

        if($files = $request->file('images')){
            foreach($files as $file){
                $name= $file->getClientOriginalName();
                $file->move('images', $name);
                $images[] = $name;
            }

            foreach($images as $image){
                $post = new Post;

                $params = explode('-', $image);
                if(!empty($params[0])){
                    $post->niche = $params[0];

                    $page = Page::where('name', 'like', '%'. $post->niche .'%')->first();

                    if($page != null){
                        $post->page_id = $page->fb_id;
                    }
                }
                if(count($params) > 1){
                    array_pop($params);
                    $post->campaign_code = implode('-', $params);
                }

                $post->image = $image;

                if($caption != null){
                    $post->caption_id = $caption->id;
                }

                $post->save();
            }

            echo 'Done';
        }
        else{
            echo 'Please select image(s) and try again.';
        }
    }

    public function getFBPostID(Request $request){
        $response = [
            'status' => 'error',
            'message' => ''
        ];

        $id = $request->input('id');

        $post = Post::where('id', '=', $id)->first();

        if($post != null){
            $domain = env('MAIN_DOMAIN', false);

            $caption = Caption::where('id', '=', $post->caption_id)->first();

	        $link = $domain . $post->campaign_code;

            $content = '';
            if($caption != null){
                $content = str_replace('<link>', $link, $caption->content);
            }

            $token = '';
            $page = Page::where('fb_id', '=', $post->page_id)->first();
            if($page != null){
                $token = $page->token;
            }

            $data = [
                'page_id' => $post->page_id,
                'content' => $content,
                'image' => $post->image,
                'token' => $token
            ];

            $data = $this->submitFB($data);

            if(!empty($data['error'])){
                $response['message'] = $data['error']['message'];
            }
            else if(!empty($data['id'])){
                $post->post_id = $data['id'];
                $post->pushed_date = date('Y-m-d');
                $post->save();

                $response['status'] = 'success';
                $response['post_id'] = $data['id'];
            }
        }

        return response()->json($response);
    }

    public function getAllIDs(Request $request){
        $date = $request->input('date');
        $ids = DB::table('posts')->select('id')->whereDate('created_at', '=', $date)->whereNull('post_id')->get();

        $response['ids'] = $ids;

        return response()->json($response);
    }

    private function submitFB($post){
        // set post fields
        $params = [
            //'published' => 'false',
            'caption' => $post['content'],
            'url' => 'http://'. $_SERVER['HTTP_HOST'] . '/images/' . $post['image'],
            'access_token'   => $post['token']
        ];

        $ch = curl_init('https://graph.facebook.com/'. $post['page_id'] .'/photos');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        // execute!
        $response = curl_exec($ch);

        curl_close($ch);

        return json_decode($response, true);
    }

    public function export(Request $request){
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $date = $request->input('date');
        $domain = env('MAIN_DOMAIN', false);
        $posts = Post::whereDate('created_at', '=', $date)->get();

        $columns = array('Date', 'Mock-up image name', 'Niche', 'Page ID', 'URL Campaign', 'Campaign code', 'Post ID');

        $callback = function() use ($posts, $domain, $columns)
        {
            $file = fopen('php://output', 'w');

            fputcsv($file, $columns);

            foreach($posts as $post) {
                fputcsv($file, [
                    $post->pushed_date,
                    $post->image,
                    $post->niche,
                    $post->page_id,
                    $domain . $post->campaign_code,
                    $post->campaign_code,
                    $post->post_id
                ]);
            }
            fclose($file);
        };

        //return Response::stream($callback, 200, $headers);

       // return response()->streamDownload($callback, 'output.csv');

       return (new StreamedResponse($callback, 200, $headers))->sendContent();
    }

    public function saveToken(Request $request){
        //set_time_limit(0);
        $response = [
            'status' => 'error',
            'message' => ''
        ];
        $appID = env('FB_APP_ID', false);
        $appSecret = env('FB_APP_SECRET', false);
        $token = $request->input('access-token');

        $url = 'https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=' .$appID. '&client_secret=' .$appSecret. '&fb_exchange_token='. $token;
        $data = $this->requestToken($url);

        if(!empty($data['access_token'])){
            $token = $data['access_token'];
            $userID = env('FB_USER_ID', false);

            $pages = Page::all();

            foreach($pages as $page){
                $url = 'https://graph.facebook.com/'. $page->fb_id .'?fields=access_token&access_token='. $token;
                $data = $this->requestToken($url);

                if(!empty($data['access_token'])){
                    $page->token = $data['access_token'];
                    $page->save();
                }
                else{
                    if(!empty($data['error']['message'])){
                        $response['message'] = $data['error']['message'];
                        break;
                    }
                }
            }

            $response['status'] = 'success';
        }
        else{
            if(!empty($data['error']['message'])){
                $response['message'] = $data['error']['message'];
            }
        }

        return response()->json($response);
    }

    private function requestToken($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        if($response === false){
            echo curl_error($ch);
        }


        curl_close($ch);

        return json_decode($response, true);
    }
}
