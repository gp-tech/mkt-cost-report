<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\StreamedResponse;

class SaleController extends Controller
{
    protected $checkURL = 'https://teechip.com/manager/v2/?page=1';
    protected $settings = [];
    protected $campaignGroups = [];
    protected $columns = [
        'DATE',
        'CAMPAIGNS',
        'URL',
        'Views',
        'Orders',
        'Unite Sales',
        'Profit'
    ];
    protected $trackingAllowed = [
        'source',
        'utm_source'
    ];

    public function __construct()
    {
        $this->settings = json_decode(file_get_contents('../storage/setting.json'), true);
        $this->settings['filters'] = $this->settings['filters'];

        for ($i = 1; $i <= 20; $i++)
        {
            $this->columns[] = 'Source_'. sprintf("%02d", $i);
            $this->columns[] = 'Views_'. sprintf("%02d", $i);
            $this->columns[] = 'Orders_'. sprintf("%02d", $i);
            $this->columns[] = 'Unit'. sprintf("%02d", $i);
            $this->columns[] = 'Sales_'. sprintf("%02d", $i);
            $this->columns[] = 'Conversion Rate_'. sprintf("%02d", $i);
            $this->columns[] = 'Total Profit_'. sprintf("%02d", $i);
        }
    }

    public function index(){
        $settings = $this->settings;

        return view("sales.index", compact('settings'));
    }

    public function save(Request $request){
        $response = [
            'status' => 'success',
            'message' => ''
        ];

        try{
            $this->settings['cookie'] = $request->input('cookie', '');
            $this->settings['auth'] = $request->input('auth', '');
            $this->settings['token'] = $request->input('token', '');
            $this->settings['notifyEmail'] = $request->input('notifyEmail', '');
            $this->settings['reportEmail'] = $request->input('reportEmail', '');

            file_put_contents('../storage/setting.json', json_encode($this->settings));
        }
        catch(\Exception $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);
    }

    public function saveSendy(Request $request){
        $response = [
            'status' => 'success',
            'message' => ''
        ];

        try{
            $filters = $request->input('filters', []);

            foreach($filters as &$filter){
                if(empty($filter['check'])){
                    $filter['check'] = 0;
                }
            }

            $this->settings['filters'] = $filters;

            file_put_contents('../storage/setting.json', json_encode($this->settings));
        }
        catch(\Exception $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }

        return response()->json($response);
    }

    public function getAll(Request $request){
        $file = '../storage/files/alltime.txt';
        $page = $request->input('page', '1');
        $type = $request->input('type', 'read');
        $hasNext = true;
        $startTime = '2017-01-01T08:00:00.000Z';
        $endTime = $this->getEndAllTime();
        $subscribed = 0;
        $totalEmail = 0;
        $total = 0;
        $message = '';

        if($page == '1' && $type == 'read'){
            if(file_exists($file )){
                unlink($file);
            }
        }

        switch($type){
            case 'read':
                while ($hasNext)
                {
                    $params = '{"operationName":"overviewStats","variables":{"payableId":"58bbc989174ee97e866899fc","dates":{"fieldName":"createdAt","start":"'. $startTime .'","end":"'. $endTime .'","timePeriod":"custom","timeZone":"America/Dawson"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"profit","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":'. $page .',"search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                    $url = 'https://teechip.com/gateway/gql';
                    $data = json_decode($this->postJson($url, $params), true);

                    if (empty($data['data']['overviewStats']['results']['campaignData']))
                    {
                        $hasNext = false;
                        break;
                    }
                    else
                    {
                        $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                        foreach ($campaigns as $c)
                        {
                            file_put_contents($file, $c['campaignId'] . PHP_EOL, FILE_APPEND);
                        }

                        $page++;

                        break;
                    }
                }
            break;
            case 'download':
                $campaigns = explode(PHP_EOL, file_get_contents($file));
                $id = '';
                if(count($campaigns) >= $page){
                    $id = $campaigns[$page - 1];
                    if(empty($id)){
                        $hasNext = false;
                    }
                    else{
                        if($this->download('https://teechip.com/manager/campaigns/' . $id . '/manage/customers.csv', '../storage/files/campaign/' . $id .'.csv')){
                            $page++;
                        }
                        else{
                            $message = 'Please refresh Cookie, Authenzation, XSRF-TOKEN.';
                            $hasNext = false;
                        }
                    }
                }
                else{
                    $hasNext = false;
                }
            break;
            case 'push':
                $files = explode(PHP_EOL, file_get_contents($file));
                $total = count($files);
                $hasNext = true;

                if($total >= $page){
                    if($files[$page - 1] != ''){
                        $file = '../storage/files/campaign/' . $files[$page - 1] . '.csv';
                        $return = $this->readPush($file, false, true);
                        $subscribed += $return['totalSubcribe'];
                        $totalEmail += $return['totalEmail'];
                        $page++;
                    }
                    else{
                        $hasNext = false;
                    }
                }
                else{
                    $hasNext = false;
                }
            break;
        }

        $response = [
            'page' => $page,
            'total' => $total,
            'subscribed' => $subscribed,
            'hasNext' => $hasNext,
            'message' => $message
        ];

        return response()->json($response);
    }

    public function getToday(Request $request){
        $file = '../storage/files/today.txt';
        $local = $request->input('local', false);
        $page = $request->input('page', '1');
        $type = $request->input('type', 'read');
        $hasNext = true;
        $limit = 50;
        $subscribed = 0;
        $totalEmail = 0;
        $total = 0;
        $message = '';

        if($page == '1' && $type == 'read'){
            if(file_exists($file )){
                unlink($file);
            }
        }

        switch($type){
            case 'read':
                $times = $this->getTodayTimes();
                while ($hasNext)
                {
                    $params = '{"operationName":"overviewStats","variables":{"payableId":"58bbc989174ee97e866899fc","dates":{"fieldName":"createdAt","start":"'. $times['start'] .'","end":"'. $times['end'] .'","timePeriod":"custom","timeZone":"America/Dawson"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"profit","sortOrder":"desc"},"campaignStatus":"active","limit":'. $limit .',"page":'. $page .',"search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
                    $url = 'https://teechip.com/gateway/gql';
                    $data = json_decode($this->postJson($url, $params), true);

                    if(!empty($data['errors'])){
                        $hasNext = false;
                        $message = 'Please refresh Cookie, Authenzation, XSRF-TOKEN.';
                        $this->notifyExpire();
                    }
                    else if (empty($data['data']['overviewStats']['results']['campaignData']))
                    {
                        $hasNext = false;
                        if(!file_exists($file)){
                            file_put_contents($file, '', FILE_APPEND);
                        }
                        break;
                    }
                    else
                    {
                        $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                        foreach ($campaigns as $c)
                        {
                            if($c['orderCount'] == 0)
                            {
                                break;
                            }

                            file_put_contents($file, $c['campaignId'] . PHP_EOL, FILE_APPEND);
                        }

                        $page++;

                        break;
                    }
                }
            break;
            case 'download':
                if(file_exists($file )){
                    $campaigns = explode(PHP_EOL, file_get_contents($file));
                    $id = '';
                    if(count($campaigns) >= $page){
                        $id = $campaigns[$page - 1];
                        if(empty($id)){
                            $hasNext = false;
                        }
                        else{
                            if($this->download('https://teechip.com/manager/campaigns/' . $id . '/manage/customers.csv', '../storage/files/campaign/' . $id .'.csv')){
                                $page++;
                            }
                            else{
                                $message = 'Please refresh Cookie, Authenzation, XSRF-TOKEN.';
                                $hasNext = false;
                            }
                        }
                    }
                    else{
                        $hasNext = false;
                    }
                }
                else{
                    $hasNext = false;
                }
            break;
            case 'push':
                $files = explode(PHP_EOL, file_get_contents($file));
                $total = count($files);
                $hasNext = true;

                if($total >= $page){
                    if($files[$page - 1] != ''){
                        $file = '../storage/files/campaign/' . $files[$page - 1] . '.csv';
                        $return = $this->readPush($file, false, true);
                        $subscribed += $return['totalSubcribe'];
                        $totalEmail += $return['totalEmail'];
                        $page++;
                    }
                    else{
                        $hasNext = false;
                    }
                }
                else{
                    $hasNext = false;
                }
            break;
        }

        $response = [
            'page' => $page,
            'total' => $total,
            'subscribed' => $subscribed,
            'totalEmail' => $totalEmail,
            'message' => '',
            'hasNext' => $hasNext
        ];

        if(!$local)
            return response()->json($response);
        else{
            $response;
        }
    }

    public function getRange(Request $request){
        set_time_limit(0);

        $response = [
            'status' => 'success',
            'message' => ''
        ];

        $from = $request->input('from', '');
        $to = $request->input('to', '');

        try{
            $startDate = (new \DateTime($from));
            $endDate = (new \DateTime($to));
            $count = $endDate->diff($startDate)->format("%a");

            if($count >= 0){
                for($i = 0; $i <= $count; $i++){
                    $this->campaignByDate($startDate);

                    $startDate->modify('+1 day');
                }
            }
            else{
                $response['status'] = 'error';
                $response['message'] = 'Please insert valid dates!';
            }
        }
        catch(\Exception $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }
       
        return response()->json($response);
    }

    public function getYesterday(){
        set_time_limit(0);
        
        $response = [
            'status' => 'success',
            'message' => ''
        ];

        try{
            $date = (new \DateTime())->modify('-1 day');
            $this->campaignByDate($date);
        }
        catch(\Exception $e){
            $response['status'] = 'error';
            $response['message'] = $e->getMessage();
        }
       
        return response()->json($response);
    }

    private function campaignByDate($date){
        $strDate = $date->format("d/m/Y");
        $filePath = '../storage/files/csv/day/' . $date->format('Y.m.d') . '.csv';
        $hasNext = true;
        $page = 1;
        $listCampaign = [];

        $start = new \DateTime($date->format('Y-m-d ') . '07:00:00');
        $end = (new \DateTime($date->format('Y-m-d ') . '06:59:59'))->modify('+1 day');

        $startDate =  str_replace('+00:00', '.000Z', $start->format(\DateTime::ATOM));
        $startDate2 = (new \DateTime($date->format('Y-m-d ') . '17:00:00'))->modify('-1 day')->getTimestamp() . '000';
        
        $endDate = str_replace('+00:00', '.999Z', $end->format(\DateTime::ATOM));
        $endDate2 = (new \DateTime($date->format('Y-m-d ') . '16:59:59'))->getTimestamp() . '999';

        while ($hasNext)
        {
            $params = '{"operationName":"overviewStats","variables":{"payableId":"58bbc989174ee97e866899fc","dates":{"fieldName":"createdAt","start":"'. $startDate .'","end":"'. $endDate .'","timePeriod":"custom","timeZone":"America/Dawson"},"ase":false,"fetchCampaignDetails":true,"sortBy":{"sortKey":"profit","sortOrder":"desc"},"campaignStatus":"active","limit":50,"page":'. $page .',"search":""},"query":"query overviewStats($payableId: String!, $dates: DateRange, $groupId: String, $page: Int, $limit: Int, $campaignStatus: String, $sortBy: Sort, $ase: Boolean, $fetchCampaignDetails: Boolean!, $search: String) {\n  overviewStats(OverviewStatsInput: {payableId: $payableId, dateRange: $dates, groupId: $groupId, ase: $ase, page: $page, limit: $limit, campaignStatus: $campaignStatus, sortBy: $sortBy, fetchCampaignDetails: $fetchCampaignDetails, search: $search}) {\n    results {\n      campaignData {\n        id\n        campaignId\n        groupId\n        payableId\n        views\n        orderCount\n        ASEorderCount\n        itemCount\n        cancelledOrderCount\n        ASEcancelledOrderCount\n        cancelledItemCount\n        profit\n        removedProfit\n        ASEprofit\n        referralProfit\n        rewardsProfit\n        conversionRate\n        UTCHour\n        __typename\n      }\n      campaignIds\n      groupByCampaign\n      uniqueCampaignCount\n      campaignDetails {\n        _id\n        status\n        groupId\n        entityId\n        url\n        title\n        description\n        tags {\n          style\n          storefront\n          category\n          marketing_tags\n          __typename\n        }\n        createdAt\n        social {\n          countdown\n          sold\n          defaultSide\n          trackingTags {\n            facebookPixelId\n            twitterPixelId\n            pinterestPixelId\n            __typename\n          }\n          __typename\n        }\n        image {\n          prefix\n          __typename\n        }\n        isEditing\n        isSelected\n        disabledProductIds\n        hasDesignLineErrors\n        takedown {\n          queueJobId\n          createdAt\n          runAt\n          reason\n          note\n          source\n          reviewerId\n          __typename\n        }\n        __typename\n      }\n      totalsByCampaign {\n        views {\n          value\n          __typename\n        }\n        removedProfit {\n          value\n          __typename\n        }\n        profit {\n          value\n          __typename\n        }\n        ASEcancelledOrderCount {\n          value\n          __typename\n        }\n        ASEorderCount {\n          value\n          __typename\n        }\n        ASEprofit {\n          value\n          __typename\n        }\n        cancelledItemCount {\n          value\n          __typename\n        }\n        cancelledOrderCount {\n          value\n          __typename\n        }\n        itemCount {\n          value\n          __typename\n        }\n        orderCount {\n          value\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    error {\n      code\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"}';
            $url = 'https://teechip.com/gateway/gql';
            $data = json_decode($this->postJson($url, $params), true);

            if(!empty($data['errors'])){
                $hasNext = false;
                $message = 'Please refresh Cookie, Authenzation, XSRF-TOKEN.';
                $this->notifyExpire();
            }
            else if (empty($data['data']['overviewStats']['results']['campaignData']))
            {
                $hasNext = false;
                break;
            }
            else
            {
                $campaigns = $data['data']['overviewStats']['results']['campaignData'];
                foreach ($campaigns as $c)
                {
                    if($c['orderCount'] == 0)
                    {
                        break;
                    }

                    $listCampaign[$c['campaignId']] = 0;
                }

                $page++;
            }
        }

        $listCampaign = array_keys($listCampaign);

        $file = fopen($filePath, 'w');

        fputcsv($file, $this->columns);

        foreach($listCampaign as $c){
            $this->campaignDetail($c, $file, $strDate, $startDate2, $endDate2);
        }

        fclose($file);
    }

    public function campaignDetail($campaignId, $file, $date, $startDate, $endDate){
        $url = 'https://api.scalablelicensing.com/rest/campaigns/auth/detail/'. $campaignId;
        $campaign = json_decode($this->get($url), true);

        if(empty($this->campaignGroups[$campaign['groupId']])){
            $url = 'https://api.scalablelicensing.com/rest/group/'. $campaign['groupId'];
            $group = json_decode($this->get($url), true);

            $this->campaignGroups[$campaign['groupId']] = "https://" . $group['domain'] . "/";
        }
        $campaign['url'] = $this->campaignGroups[$campaign['groupId']] . $campaign['url'];

        // Total
        $url = 'https://api.scalablelicensing.com/rest/campaigns/stats-overview-total?start='. $startDate .'&end='. $endDate .'&campaignId='. $campaignId .'&ase=false';
        $total = json_decode($this->get($url), true);

        // Breakdown
        $url = 'https://api.scalablelicensing.com/rest/campaigns/'. $campaignId .'/stats-breakdown?start='. $startDate .'&end='. $endDate .'&ase=false';
        $breakdown = json_decode($this->get($url), true);

        $data = [
            $date,
            $campaign['title'],
            $campaign['url'],
            $total['views'],
            $total['orders'],
            $total['sold'],
        ];

        if ($total['profit'] > 0)
        {
            $data[] = '$' . ($total['profit'] * 0.01);
        }
        else
        {
            $data[] = '$0';
        }

        foreach($breakdown['tracking'] as $tracking){
            if(in_array($tracking['param'], $this->trackingAllowed)){
                $data[] = $tracking['value'];
                $data[] = $tracking['views'];
                $data[] = $tracking['orders'];
                $data[] = $tracking['sold'];

                if ($tracking['orders'] == 0 || $tracking['views'] == 0)
                {
                    $data[] = '0%';
                }
                else
                {
                    $data[] = round(floatval($tracking['orders']) / floatval($tracking['views']) * 100, 2) . '%';
                }

                if ($tracking['profit'] > 0)
                {
                    $data[] = '$' . ($tracking['profit'] * 0.01);
                }
                else
                {
                    $data[] = '$0';
                }
            }
        }

        fputcsv($file, $data);
    }

    public function exportMonth(Request $request){
        $month = $request->input('page', date('Y.m'));
        $title = 'export_csv_'.$month . '.csv';
        $folder = '../storage/files/csv/day/';
        $file = '../storage/files/csv/month/' . $month . '.csv';

        if (file_exists($file))
        {
            unlink($file);
        }

        $files = glob('../storage/files/csv/day/'. $month .'*.csv');
        $total = count($files);
        $columns = $this->columns;

        $file = fopen('php://output', 'w');

        $callback = function() use ($columns, $files, $file)
        {
            $file = fopen('php://output', 'w');

            fputcsv($file, $columns);

            foreach($files as $f){
                $lines = array_map('str_getcsv', file($f)); 
                $lineCount = count($lines);

                if($lineCount > 2){
                    for ($i = 1; $i < $lineCount; $i++)
                    {
                        fputcsv($file, $lines[$i]);
                    }
                }
            }

            fclose($file);
        };

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=". $title,
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        return response()->streamDownload($callback, $title, $headers);
    }

    public function sendy(Request $request){
        $page = $request->input('page', '1');
        $subscribed = 0;
        $totalEmail = 0;
        $files = glob('../storage/files/campaign/*.csv');
        $total = count($files);
        $hasNext = true;

        if($total >= $page){
            $file = $files[$page - 1];
            $return = $this->readPush($file, false, true);
            $subscribed += $return['totalSubcribe'];
            $totalEmail += $return['totalEmail'];
            $page++;
        }
        else{
            $hasNext = false;
        }

        $response = [
            'page' => $page,
            'total' => $total,
            'subscribed' => $subscribed,
            'hasNext' => $hasNext
        ];

        return response()->json($response);
    }

    private function readPush($file, $isToday = false, $isPushOnly = false){
        set_time_limit(0);
        $keys = explode(PHP_EOL, file_get_contents('../storage/keys.json'));
        $data = array_map('str_getcsv', file($file));
        $total  = count($data);
        $totalSubcribe = 0;
        $totalEmail = 0;

        if($total  > 1){
            for($i = 1; $i < $total; $i++){
                $fields = $data[$i];
                $params = [];

                foreach($keys as $index => $key){
                    $params[$key] = $fields[$index];
                }

                $params["list"] = $this->settings['list'];
                $params["api_key"] = $this->settings['sendyAPIKey'];
                $params["boolean"] = "true";

                if ($isPushOnly)
                {
                    foreach($this->settings['filters'] as $filter){
                        if(empty($filter['listID'])) continue;

                        if($filter['check'] && $this->startsWith($params["CampaignURL"], $filter['filter'])){
                            $params["list"] = $filter['listID'];
                            $response = $this->post($this->settings['url'], $params);

                            if($response == "1")
                            {
                                $totalSubcribe++;
                            }
                        }
                    }
                }
                else{
                    // Push to all
                    $response = $this->post($this->settings['url'], $params);

                    if($response == "1")
                    {
                        $totalSubcribe++;
                    }

                    if($this->startsWith($params["CampaignURL"], $filter['filter'])){
                        $params["list"] = $filter['listID'];
                        $response = $this->post($this->settings['url'], $params);

                        if($response == "1")
                        {
                            $totalSubcribe++;
                        }
                    }
                }
                
                $totalEmail++;
            }
        }

        return [
            'totalSubcribe' => $totalSubcribe,
            'totalEmail' => $totalEmail
        ];
    }

    public function cron(Request $request){
        set_time_limit(0);

        $now = new \Datetime();

        if($now->format('i') == 59){
            $changeDate = $now->format('H') == 23 ? true : false;
            $request->request->add(['local' => true]);
            $request->request->add(['type' => 'read']);
            
            $type = 'read';
            $totalEmail = 0;
            $hasNext = true;
            $page = 1;

            while($hasNext){
                switch($type){
                    case 'read':
                        $request->request->add(['page' => $page]);
                        $response = $this->getToday($request);
                        $page++;

                        if(!$response['hasNext']){
                            $type = 'download';
                            $request->request->add(['type' => $type]);
                            $page = 1;
                        }
                    break;
                    case 'download':
                        $request->request->add(['page' => $page]);
                        $response = $this->getToday($request);
                        $page++;

                        if(!$response['hasNext']){
                            $type = 'push';
                            $request->request->add(['type' => $type]);
                            $page = 1;
                        }
                    break;
                    case 'push':
                        $request->request->add(['page' => $page]);
                        $response = $this->getToday($request);
                        $totalEmail += $response['totalEmail'];
                        $page++;

                        if(!$response['hasNext']){
                            $hasNext = false;
                            if($changeDate){
                                $this->settings['yesterdayCount'] = $this->settings['todayCount'];
                                $this->settings['todayCount'] = $totalEmail;
                                file_put_contents('../storage/setting.json', json_encode($this->settings));

                                $this->sendReport();
                            }
                            
                            echo 'Done!';
                        }
                    break;
                }
            }
        }
    }

    public function check(){
        //$this->sendEmail('toanmav@gmail.com', 'test', 'This is test email');
        $headers = [
            'Cookie: ' . $this->settings['cookie'],
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->checkURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec ($ch);

        curl_close ($ch);

        echo $this->contains($response, 'ROOT_QUERY":{"id"');
    }

    private function download($url, $file){
        $headers = [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            //'Accept-Encoding: gzip, deflate, br',
            'Accept-Language: en-US,en;q=0.9,fr-FR;q=0.8,fr;q=0.7',
            'Connection: keep-alive',
            'Cookie: '. $this->settings['cookie'],
            'DNT: 1',
            'Host: teechip.com',
            //'Referer: https://teechip.com/manager/campaigns/manage/customer?campaignId=58cac6f7567226333eee766b',
            'Sec-Fetch-Dest: document',
            'Sec-Fetch-Mode: navigate',
            'Sec-Fetch-Site: same-origin',
            'Sec-Fetch-User: ?1',
            'Upgrade-Insecure-Requests: 1',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36');
        
        if($this->startsWith($url, 'https://')){
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CAINFO, '../storage/cacert.pem');
		}

        $response = curl_exec ($ch);
        curl_close ($ch);

        if($response == '{"error":"You are not allowed to view this page"}'){
            return false;
        }
        else{
            file_put_contents($file, $response);
            return true;
        }
    }

    private function postJson($url, $params){
        $ch = curl_init();

        $headers = [
            'Accept: */*',
            //'Accept-Encoding: gzip, deflate, br',
            'Accept-Language: en-US,en;q=0.5',
            'Cache-Control: no-cache',
            'Connection: keep-alive',
            //'Content-Length: 3220',
            'content-type: application/json',
            'Cookie: '. $this->settings['cookie'],
            'DNT: 1',
            'Host: teechip.com',
            'Origin: https://teechip.com',
            'Pragma: no-cache',
            //'Referer: https://teechip.com/manager/v2/?page=1&timePeriod=today&startDate=1600153200000&endDate=1600239599999',
            'sl-auth-token: USECOOKIE',
            'X-XSRF-TOKEN: '. $this->settings['token'],
        ];

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if($this->startsWith($url, 'https://')){
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_CAINFO, '../storage/cacert.pem');
		}

        $response = curl_exec($ch);

        curl_close ($ch);

        return $response;
    }

    private function post($url, $params){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        curl_close ($ch);

        return $response;
    }

    private function get($url)
    {
        $headers = [
            'Authorization: '. $this->settings['auth'],
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec ($ch);

        curl_close ($ch);

        return $response;
    }

    private function getEndAllTime(){
        $now = (new \DateTime("now", new \DateTimeZone("UTC")))->modify('-8 hours');
        $end = (new \DateTime($now->format('Y-m-d ') . '06:59:59'))->modify('+1 day');

        return str_replace('+00:00', '.999Z', $end->format(\DateTime::ATOM));
    }

    private function getTodayTimes()
    {
        $now = (new \DateTime("now", new \DateTimeZone("UTC")))->modify('-8 hours');
        $start = new \DateTime($now->format('Y-m-d ') . '07:00:00');
        $end = (new \DateTime($now->format('Y-m-d ') . '06:59:59'))->modify('+1 day');
        $firstDate = new \DateTime('1970-1-1');

        $data = [
            'start' => str_replace('+00:00', '.000Z', $start->format(\DateTime::ATOM)),
            'end' => str_replace('+00:00', '.999Z', $start->format(\DateTime::ATOM)),
        ];

        return $data;
    }

    private function contains($str, $needed){
        if (str_contains($str, $needed)) { 
            return true;
        }
    
        return false;
    }

    private function startsWith($str, $needle)
	{
		 $length = strlen($needle);
		 return (substr($str, 0, $length) === $needle);
    }

    private function notifyExpire(){
        $body = 'Hi, <br><br>The cookie, authorization, token are expired. Please update the app. <br><br>Regards,';
        $this->sendEmail($this->settings['notifyEmail'], 'TeeChip App - Expired', $body);
    }

    private function sendReport(){
        $body = 'Hi, <br><br>Here is the daily report: <br> - Yesterday: ' . $this->settings['yesterdayCount'] . '<br> - Today: '. $this->settings['todayCount'] .' <br><br>Regards,';
        $this->sendEmail($this->settings['reportEmail'], 'TeeChip App - Daily Report', $body);
    }
    
    private function sendEmail($email, $subject, $body){
        Mail::send([], [], function ($message) use($email, $subject, $body) {
            $message->to($email)
              ->subject($subject)
              ->setBody($body, 'text/html');
          });
    }
}