<?php

namespace App\Console\Commands;

use App\Campaigns;
use App\FbCampaignAccount;
use App\FbCampaignApp;
use App\Jobs\GetFbCampaignInsights;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\User;
use Illuminate\Console\Command;

class PushGetCampaignInsigntAppId extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insight:get-campaigns-insight-app-id {app_id}'; // time = today || range_23846473529650584

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push get campaign insights.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    //cung 1 camp nam trong 2 app khac nhau thi du lieu co khac nhau ko.
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $app_id = $this->argument('app_id');
        $app = FbCampaignApp::where('id', $app_id)->first();
        $api = Api::init($app->app_id, $app->app_secret, $app->access_token);
        $api->setDefaultGraphVersion('11.0');
        //get ads accounts
        $fields = array(
            'id',
            'name'
        );
        $params = array(
            'limit' => 1000
        );
        try {

            $adaccounts = (new User($app->user_id))->getAdAccounts(
                $fields,
                $params
            );
            foreach ($adaccounts as $account) {
                $data = $account->getData();
                $data['id'] = str_replace('act_', '', $data['id']);
                if (count($data)) {

                    if (FbCampaignAccount::where('account_id', $data['id'])->where('fb_app_id', $app->id)->count())
                    {
                        echo $data['name'] . PHP_EOL;
                        \Artisan::call('insight:get-campaigns '.$data['id']);
                        FbCampaignAccount::where('account_id', $data['id'])->where('fb_app_id', $app->id)->update(['status' => 1, 'message' => '', 'name' =>  $data['name']]);
                        continue;
                    }
                    $fbAcc = new FbCampaignAccount;
                    $fbAcc->name = $data['name'];
                    $fbAcc->fb_app_id = $app->id;
                    $fbAcc->account_id = $data['id'];
                    $fbAcc->message = '';
                    $fbAcc->status = 1;
                    $fbAcc->save();
                    \Artisan::call('insight:get-campaigns '.$data['id']);
                }
            }
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            return $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
            return $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
            return $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookClientException $e) {
            return $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookOtherException $e) {
            return $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookServerException $e) {
            return $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
            return $e->getMessage();
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            return $e->getMessage();
        } catch(\Exception $e) {
            echo 'ERROR ' . $e->getMessage() . PHP_EOL;
            FbCampaignApp::where('id', $app->id)->update(['status' => 0, 'message' => $e->getMessage()]);
            \App\FbCampaign::where('fb_app_id', $app->id)->update(['status' => 'ERROR']);
        }
        //end get ads accounts

        $fb_campaigns = \App\FbCampaign::where('fb_app_id', $app_id)->get();
        $time = 'all';
        $i = 0;
        foreach ($fb_campaigns as $fb_campaign) {

            if (!empty($fb_campaign->fb_app_id)) {
                $env = FbCampaignApp::where('id', $fb_campaign->fb_app_id)->first();

                if (empty($env)) {
                    continue;
                }

                if ($env->status != 1) continue;
                if ($fb_campaign->start_time < date('Y-m-d', strtotime('-3 months'))) {
                    $fb_campaign->start_time = date('Y-m-d', strtotime('-1 months'));
                }
                if ($i > 0 && $i % 1000 == 0) {
                    echo 'Sleeping ' . $i . PHP_EOL;
                    sleep(5);
                }
                GetFbCampaignInsights::dispatch($fb_campaign, $time, $env);
            }
            $i++;
        }
    }
}
