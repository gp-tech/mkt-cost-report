<?php

namespace App\Console\Commands;

use App\Campaigns;
use App\FbCampaign;
use App\FbCampaignAccount;
use App\FbCampaignApp;
use Facebook\FacebookApp;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\AdAccount;
use Illuminate\Console\Command;
use FacebookAds\Object\User;
use Illuminate\Database\Eloquent\Model;

//use App\Jobs\GetAdsInsights;

class AppRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:refresh {app_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'app refresh account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function get_web_page($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return $content;
    }

    public function handle()
    {
        $app_id = $this->argument('app_id');
        if ($app_id != 'all') {
            $apps = FbCampaignApp::where('id', $app_id)->get();
        } else {
            $apps = FbCampaignApp::where('status', 1)->get();
        }

        foreach ($apps as $app) {
            $api = Api::init($app->app_id, $app->app_secret, $app->access_token);
            $api->setDefaultGraphVersion('11.0');
            $fields = array(
                'id',
                'name'
            );
            $params = array(
                'limit' => 1000
            );
            try {

                $adaccounts = (new User($app->user_id))->getAdAccounts(
                    $fields,
                    $params
                );
                foreach ($adaccounts as $account) {
                    $data = $account->getData();
                    $data['id'] = str_replace('act_', '', $data['id']);
                    if (count($data)) {
                        if (FbCampaignAccount::where('account_id', $data['id'])->where('fb_app_id', $app->id)->count())
                        {
                            echo $data['name'] . PHP_EOL;
                            FbCampaignAccount::where('account_id', $data['id'])->where('fb_app_id', $app->id)->update(['status' => 1, 'message' => '', 'name' =>  $data['name']]);
                            continue;
                        }
                        $fbAcc = new FbCampaignAccount;
                        $fbAcc->name = $data['name'];
                        $fbAcc->fb_app_id = $app->id;
                        $fbAcc->account_id = $data['id'];
                        $fbAcc->message = '';
                        $fbAcc->status = 1;
                        $save = $fbAcc->save();

                        if ($save) {
                            \Artisan::call('insight:get-campaigns '.$data['id'].'_'.$app->id);
                        }
                    }
                }
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookClientException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookServerException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                return $e->getMessage();
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                return $e->getMessage();
            } catch(\Exception $e) {
                echo 'ERROR ' . $e->getMessage() . PHP_EOL;
                FbCampaignApp::where('id', $app->id)->update(['status' => 0, 'message' => $e->getMessage()]);
                \App\FbCampaign::where('fb_app_id', $app->id)->update(['status' => 'ERROR']);
                continue;
            }
        }
    }
}
