<?php

namespace App\Console\Commands;

use App\Campaigns;
use App\FbCampaignAccount;
use App\FbCampaignApp;
use App\Jobs\GetFbCampaignInsights;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\Campaign;
use Illuminate\Console\Command;
use FacebookAds\Object\AdAccount;

class PushGetCampaignInsigntByTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insight:get-campaigns-insight-time {time}'; // time = 2021-06-14

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push get campaign insights.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //date_default_timezone_set('America/Phoenix');
        //$this->getAdsInsights();
        $time = $this->argument('time');

        $fb_campaigns = \App\FbCampaign::where('status', 'ACTIVE')->where('campaign_id', 23848085193450050)->get();
        //$fb_campaigns = \App\FbCampaign::where('fb_app_id', 49)->get();
        //$fb_campaigns = \App\FbCampaign::get();
        //dd($fb_campaigns);
        //$accounts = \App\FbCampaign::where('account_id', 425669212043069)->get();
        foreach ($fb_campaigns as $fb_campaign) {
            //if ($fb_campaign->status != 'ACTIVE') continue;
            //if ($fb_campaign->fb_app_id == 21) {
                //$env = FbCampaignApp::where('id', $fb_campaign->fb_app_id)->first();
                //$this->getAdsInsights1($fb_campaign, $time, $env, true);
            //};
            //if ($fb_campaign->campaign_id == '23847496049250076') continue;
            //if ($fb_campaign->start_time < date('Y-m-d', strtotime("-2 month"))) continue;
            //dd($fb_campaign);
            //\Artisan::call('insight:get-campaigns '.$account->account_id);

            if (!empty($fb_campaign->fb_app_id)) {
                $env = FbCampaignApp::where('id', $fb_campaign->fb_app_id)->first();
                $this->getAdsInsights1($fb_campaign, $time, $env, true);
                //die;
                GetFbCampaignInsights::dispatch($fb_campaign, $time, $env, true);
            }
        }
    }

    public function getAdsInsights1($fb_campaign, $time, $env, $by_time = false)
    {
        //$env = FbCampaignApp::where('id', 49)->first();
        $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
        $api->setDefaultGraphVersion('11.0');
        $api->setLogger(new CurlLogger());

        $fields = array(
            'spend', 'unique_link_clicks_ctr', 'clicks', 'impressions', 'unique_clicks', 'reach', 'unique_actions',
            'cost_per_unique_click', 'cost_per_unique_action_type', 'actions', 'cpm', 'purchase_roas',
            'website_purchase_roas', 'account_currency'
        );
        echo $fb_campaign->fb_app_id . PHP_EOL;
        $id = $fb_campaign->campaign_id;

        $params = array(
            'time_increment' => 1,
            'date_preset'    => 'today'
        );


        $response = (new Campaign($id))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent();

        dd($response);
        if ($by_time == true) {
            $start_time = $fb_campaign->start_time;
            if ($start_time < date('Y-m-d', strtotime("-3 month"))) return;
            $params = array(
                'time_increment' => 1,
                'time_range'     => json_encode([
                    'since' => date('Y-m-d', strtotime($time)),
                    'until' => date('Y-m-d', strtotime($time))
                ])
            );

            echo $fb_campaign->fb_app_id . PHP_EOL;
            $id = $fb_campaign->campaign_id;


            try {
                $response = (new Campaign($id))->getInsights(
                    $fields,
                    $params
                )->getResponse()->getContent();

                $insert = [];
                $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
                $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
                $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
                $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
                $insert['fb_campaign_id'] = $fb_campaign->id;
                $insert['campaign_id'] = $fb_campaign->campaign_id;
                if (count($response['data']) < 1) return;
                if (count($response['data']) > 0) {
                    $response = $response['data'][0];
                    if ($response['impressions'] < 1) return;

                    if (isset($response['actions'])) {
                        foreach ($response['actions'] as $item) {

                            if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                                $insert['unique_adds_to_cart'] = $item['value'];
                            }

                            if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                                $insert['purchase'] = $insert['results'] = $item['value'];
                            }
                        }
                    }

                    if (isset($response['unique_actions'])) {
                        foreach ($response['unique_actions'] as $item) {
                            if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                                $insert['unique_clicks'] = isset($item['value']) ? $item['value'] : 0;
                            }
                        }
                    }

                    $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
                    $insert['spend'] = $response['spend'];

                    if ($response['account_currency'] == 'VND') {
                        $insert['spend'] = round( $response['spend'] / 22600, 2);
                    }

                    if ($response['account_currency'] == 'EUR') {
                        $insert['spend'] = round($response['spend'] / 0.82, 2);
                    }


                    $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
                    $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
                    $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;
                    $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;

                    $insert['time_range'] = date('Y-m-d', strtotime($time));
                    if (\App\FbCampaignInsight::where('campaign_id', $fb_campaign->campaign_id)->where('fb_campaign_id', $fb_campaign->id)->where('time_range', $insert['time_range'])->count()) {
                        \App\FbCampaignInsight::where('campaign_id', $fb_campaign->campaign_id)->where('fb_campaign_id', $fb_campaign->id)->where('time_range', $insert['time_range'])->update($insert);
                    } else {
                        \App\FbCampaignInsight::forceCreate(
                            $insert
                        );
                    }
                }
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookClientException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookServerException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                $response = $e->getMessage();
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                $response = $e->getMessage();
            } catch(\Exception $e) {
                \App\FbCampaignInsight::where('campaign_id', $fb_campaign->campaign_id)->where('fb_campaign_id', $fb_campaign->id)->where('time_range', $insert['time_range'])->update($insert);
            }

            if ($response == "error")
            {
                echo 123;die;
                return;
            }
            //}


        } else {
            if ($time == 'today' || $time == 'yesterday') {
                $params = array(
                    'time_increment' => 1,
                    'date_preset'    => $time
                );
                try {
                    $id = $this->campaign_id;
                    $response = (new Campaign($id))->getInsights(
                        $fields,
                        $params
                    )->getResponse()->getContent();

                    $insert = [];
                    $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
                    $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
                    $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
                    $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
                    $insert['time_range'] = date('Y-m-d', strtotime($time));
                    $insert['fb_campaign_id'] = $this->id;
                    $insert['campaign_id'] = $this->campaign_id;
                    if (count($response['data']) > 0) {
                        $response = $response['data'][0];

                        if ($response['impressions'] < 1) return;

                        if (isset($response['actions'])) {
                            foreach ($response['actions'] as $item) {

                                if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                                    $insert['unique_adds_to_cart'] = $item['value'];
                                }

                                if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                                    $insert['purchase'] = $insert['results'] = $item['value'];
                                }
                            }
                        }

                        if (isset($response['unique_actions'])) {
                            foreach ($response['unique_actions'] as $item) {
                                if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                                    $insert['unique_clicks'] = isset($response['link_click']) ? $response['link_click'] : 0;
                                }
                            }
                        }

                        $insert['cost_per_unique_click'] = $response['cost_per_unique_click'];
                        $insert['spend'] = $response['spend'];

                        if ($response['account_currency'] == 'VND') {
                            $insert['spend'] = round($response['spend'] / 22600, 2);
                        }

                        if ($response['account_currency'] == 'EUR') {
                            $insert['spend'] = round($response['spend'] / 0.82, 2);
                        }
                        $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
                        $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
                        $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;

                        $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;
                    }
                    if (\App\FbCampaignInsight::where('campaign_id', $this->campaign_id)->where('fb_campaign_id', $this->id)->where('time_range', $insert['time_range'])->count()) {
                        \App\FbCampaignInsight::where('campaign_id', $this->campaign_id)->where('fb_campaign_id', $this->id)->where('time_range', $insert['time_range'])->update($insert);
                    } else {
                        \App\FbCampaignInsight::forceCreate(
                            $insert
                        );
                    }
                } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookClientException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookServerException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                    return $e->getMessage();
                } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                    return $e->getMessage();
                } catch(\Exception $e) {
                    return 'ERROR';
                }

            } else {
                $start_time = $this->start_time;
                if ($start_time < date('Y-m-d', strtotime("-3 month"))) return;
                $begin = new \DateTime($start_time);
                $end = new \DateTime(date('Y-m-d'));
                $end = $end->modify('+1 day');

                $interval = new \DateInterval('P1D');
                $daterange = new \DatePeriod($begin, $interval, $end);
                foreach ($daterange as $date) {
                    $since = $date->format("Y-m-d");
                    $params = array(
                        'time_increment' => 1,
                        'time_range'     => json_encode([
                            'since' => date('Y-m-d', strtotime($since)),
                            'until' => date('Y-m-d', strtotime($since))
                        ])
                    );

//                if (\App\FbCampaignInsight::where('campaign_id', $this->campaign_id)->where('fb_campaign_id', $this->id)->where('time_range', $since)->count()) continue;

                    $id = $this->campaign_id;
                    try {
                        $response = (new Campaign($id))->getInsights(
                            $fields,
                            $params
                        )->getResponse()->getContent();
                        $insert = [];
                        $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
                        $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
                        $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
                        $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
                        $insert['fb_campaign_id'] = $this->id;
                        $insert['campaign_id'] = $this->campaign_id;
                        if (count($response['data']) < 1) continue;
                        if (count($response['data']) > 0) {
                            $response = $response['data'][0];
                            if ($response['impressions'] < 1) continue;

                            if (isset($response['actions'])) {
                                foreach ($response['actions'] as $item) {

                                    if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                                        $insert['unique_adds_to_cart'] = $item['value'];
                                    }

                                    if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                                        $insert['purchase'] = $insert['results'] = $item['value'];
                                    }
                                }
                            }

                            if (isset($response['unique_actions'])) {
                                foreach ($response['unique_actions'] as $item) {
                                    if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                                        $insert['unique_clicks'] = isset($item['value']) ? $item['value'] : 0;
                                    }
                                }
                            }

                            $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
                            $insert['spend'] = $response['spend'];

                            if ($response['account_currency'] == 'VND') {
                                $insert['spend'] = round( $response['spend'] / 22600, 2);
                            }

                            if ($response['account_currency'] == 'EUR') {
                                $insert['spend'] = round($response['spend'] / 0.82, 2);
                            }


                            $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
                            $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
                            $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;
                            $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;

                            $insert['time_range'] = $since;
                            if (\App\FbCampaignInsight::where('campaign_id', $this->campaign_id)->where('fb_campaign_id', $this->id)->where('time_range', $since)->count()) {
                                \App\FbCampaignInsight::where('campaign_id', $this->campaign_id)->where('fb_campaign_id', $this->id)->where('time_range', $since)->update($insert);
                            } else {
                                \App\FbCampaignInsight::forceCreate(
                                    $insert
                                );
                            }
                        }
                    } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                        $response = $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                        $response = $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                        $response = $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookClientException $e) {
                        $response = $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                        $response = $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookServerException $e) {
                        $response = $e->getMessage();
                    } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                        $response = $e->getMessage();
                    } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                        $response = $e->getMessage();
                    } catch(\Exception $e) {
                        continue;
                    }

                    if ($response == "error")
                    {
                        continue;
                    }
                }
            }
        }

    }

    public function getAdsInsights()
    {
        $env = FbCampaignApp::where('id', 49)->first();

        date_default_timezone_set('America/Phoenix');
        $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
        $api->setDefaultGraphVersion('11.0');
        $api->setLogger(new CurlLogger());

        //echo date('Y-m-d h:i:s');die;
        //if ('2015-02-24' < date('Y-m-d', strtotime("-1 month"))) return;
        $fields = array(
            'spend', 'unique_link_clicks_ctr', 'clicks', 'impressions', 'unique_clicks', 'reach', 'unique_actions',
            'cost_per_unique_click', 'cost_per_unique_action_type', 'actions', 'cpm', 'purchase_roas',
            'website_purchase_roas', 'account_currency'
        );

        $camp = \App\FbCampaign::where('campaign_id', 23847645197660772)->first();


        $start_time = $camp->start_time;
        if ($start_time < date('Y-m-d', strtotime("-3 month"))) return;
        $begin = new \DateTime($start_time);
        $end = new \DateTime(date('Y-m-d'));
        $end = $end->modify('+1 day');

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval, $end);


        $params = array(
            'time_increment' => 1,
            'date_preset'    => 'yesterday'
        );
        $response = (new Campaign(23847645197660772))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent();

        dd($response);

        foreach ($daterange as $date) {
            $since = $date->format("Y-m-d");
            $params = array(
                'time_increment' => 1,
                'time_range'     => json_encode([
                    'since' => date('Y-m-d', strtotime($since)),
                    'until' => date('Y-m-d', strtotime($since))
                ])
            );



            $id = $camp->campaign_id;

            $response = (new Campaign($id))->getInsights(
                $fields,
                $params
            )->getResponse()->getContent();

            $insert = [];
            $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
            $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
            $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
            $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
            $insert['fb_campaign_id'] = $camp->id;
            $insert['campaign_id'] = $camp->campaign_id;
            if (count($response['data']) < 1) continue;
            if (count($response['data']) > 0) {
                $response = $response['data'][0];
                if ($response['impressions'] < 1) continue;

                if (isset($response['actions'])) {
                    foreach ($response['actions'] as $item) {

                        if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                            $insert['unique_adds_to_cart'] = $item['value'];
                        }

                        if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                            $insert['purchase'] = $insert['results'] = $item['value'];
                        }
                    }
                }

                if (isset($response['unique_actions'])) {
                    foreach ($response['unique_actions'] as $item) {
                        if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                            $insert['unique_clicks'] = isset($item['value']) ? $item['value'] : 0;
                        }
                    }
                }

                $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
                $insert['spend'] = $response['spend'];

                if ($response['account_currency'] == 'VND') {
                    $insert['spend'] = $response['spend'] / 22600;
                }

                if ($response['account_currency'] == 'EUR') {
                    $insert['spend'] = $response['spend'] / 1.21;
                }


                $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
                $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
                $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;
                $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;

                $insert['time_range'] = $since;
                if (\App\FbCampaignInsight::where('campaign_id', $camp->campaign_id)->where('fb_campaign_id', $camp->id)->where('time_range', $since)->count()) {
                    \App\FbCampaignInsight::where('campaign_id', $camp->campaign_id)->where('fb_campaign_id', $camp->id)->where('time_range', $since)->update($insert);
                } else {
                    \App\FbCampaignInsight::forceCreate(
                        $insert
                    );
                }
            }



            try {
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookClientException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookServerException $e) {
                $response = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                $response = $e->getMessage();
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                $response = $e->getMessage();
            } catch(\Exception $e) {
                continue;
            }

            if ($response == "error")
            {
                continue;
            }
        }

        // old version
        $env = FbCampaignApp::where('id', 26)->first();

        date_default_timezone_set('America/Phoenix');
        $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
        $api->setDefaultGraphVersion('11.0');
        $api->setLogger(new CurlLogger());

        $fields = array(
            'spend', 'unique_link_clicks_ctr', 'clicks', 'impressions', 'unique_clicks', 'reach', 'account_currency'

        );

        $params = array(
            'time_increment' => 1,
            'time_range'     => json_encode([
                'since' => '2021-06-01',
                'until' => '2021-06-10'
            ]),
            //'date_preset'    => 'today'
        );
        $id = 23847475668910604;
        $response = (new Campaign($id))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent();
        dd($response);

        if (count($response['data']) > 0) {
            $response = $response['data'][0];

            if (isset($response['actions'])) {
                foreach ($response['actions'] as $item) {

                    if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                        $insert['unique_adds_to_cart'] = $item['value'];
                    }

                    if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                        $insert['purchase'] = $insert['results'] = $item['value'];
                    }
                }
            }

            if (isset($response['unique_actions'])) {
                foreach ($response['unique_actions'] as $item) {
                    if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                        $insert['unique_clicks'] = isset($item['value']) ? $item['value'] : 0;
                    }
                }
            }

            $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
            if ($response['account_currency'] == 'VND') {
                $response['spend'] = $response['spend'] / 22600;
            }
            $insert['spend'] = $response['spend'];
            $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
            $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
            $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;
            $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;

            dd($insert);

            \App\FbCampaignInsight::forceCreate(
                $insert
            );
        }
        try {

            $response = (new Campaign($id))->getInsights(
                $fields,
                $params
            )->getResponse()->getContent();
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            $response = $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
            $response = $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
            $response = $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookClientException $e) {
            $response = $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookOtherException $e) {
            $response = $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookServerException $e) {
            $response = $e->getMessage();
        } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
            $response = $e->getMessage();
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            $response = $e->getMessage();
        } catch(\Exception $e) {
            $response = "error";
        }
        if ($response == "error")
        {
            return;
        }

        $insert = [];
        $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
        $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
        $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
        $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
        $insert['time_range'] = date('Y-m-d');
        $insert['fb_campaign_id'] = $this->id;
        $insert['campaign_id'] = $this->campaign_id;

        if (count($response['data']) > 0) {
            $response = $response['data'][0];

            if ($response['impressions'] < 1) return;

            if (isset($response['actions'])) {
                foreach ($response['actions'] as $item) {

                    if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                        $insert['unique_adds_to_cart'] = $item['value'];
                    }

                    if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                        $insert['purchase'] = $insert['results'] = $item['value'];
                    }
                }
            }

            if (isset($response['unique_actions'])) {
                foreach ($response['unique_actions'] as $item) {
                    if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                        $insert['unique_clicks'] = isset($response['link_click']) ? $response['link_click'] : 0;
                    }
                }
            }

            $insert['cost_per_unique_click'] = $response['cost_per_unique_click'];
            $insert['spend'] = $response['spend'];
            $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
            $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
            $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;

            $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;
        }

        \App\FbCampaignInsight::updateOrCreate(
            ['time_range' => date('Y-m-d'), 'campaign_id' => $this->campaign_id],
            $insert
        );

        /*$params = array(
            'time_increment' => 1,
            'date_preset'    => 'yesterday'
        );
        $id = 23847358382670534;
        $response = (new Campaign($id))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent();

        $insert = [];
        $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
        $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
        $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
        $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
        $insert['time_range'] = date('Y-m-d');
        $insert['fb_campaign_id'] = 23847358382670534;
        $insert['campaign_id'] = 23847358382670534;
        if (count($response['data']) > 0) {
            $response = $response['data'][0];
            if (isset($response['actions'])) {
                foreach ($response['actions'] as $item) {

                    if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                        $insert['unique_adds_to_cart'] = $item['value'];
                    }

                    if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                        $insert['purchase'] = $insert['results'] = $item['value'];
                    }
                }
            }

            if (isset($response['unique_actions'])) {
                foreach ($response['unique_actions'] as $item) {
                    if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                        $insert['unique_clicks'] = isset($response['link_click']) ? $response['link_click'] : 0;
                    }
                }
            }

            $insert['cost_per_unique_click'] = $response['cost_per_unique_click'];
            $insert['spend'] = $response['spend'];
            $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
            $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
            $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;

            $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;
        }
        dd($insert);
        \App\FbCampaignInsight::updateOrCreate(
            ['time_range' => date('Y-m-d'), 'campaign_id' => $this->campaign_id],
            $insert
        );

        die;
        date_default_timezone_set('America/Phoenix');
        $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
        $api->setDefaultGraphVersion('11.0');
        $api->setLogger(new CurlLogger());

        $fields = array(
            'spend', 'unique_link_clicks_ctr', 'clicks', 'impressions', 'unique_clicks', 'reach', 'unique_actions',
            'cost_per_unique_click', 'cost_per_unique_action_type', 'actions', 'cpm', 'purchase_roas',
            'website_purchase_roas'
        );*/

        $begin = new \DateTime('2021-01-01');
        $end = new \DateTime('2021-05-17');
        $end = $end->modify('+1 day');

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval, $end);
        foreach ($daterange as $date) {
            $since = $date->format("Y-m-d");
            $params = array(
                'time_increment' => 1,
                'time_range'     => json_encode([
                    'since' => date('Y-m-d', strtotime($since)),
                    'until' => date('Y-m-d', strtotime($since))
                ])
            );
            $id = 23847428674590298;

            $response = (new Campaign($id))->getInsights(
                $fields,
                $params
            )->getResponse()->getContent();

            $insert = [];
            $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
            $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
            $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
            $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
            $insert['fb_campaign_id'] = 344;
            $insert['campaign_id'] = $id;
            if (count($response['data']) > 0) {
                $response = $response['data'][0];
                //if ($response['impressions'] < 1) continue;
                if (isset($response['actions'])) {
                    foreach ($response['actions'] as $item) {

                        if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                            $insert['unique_adds_to_cart'] = $item['value'];
                        }

                        if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                            $insert['purchase'] = $insert['results'] = $item['value'];
                        }
                    }
                }

                if (isset($response['unique_actions'])) {
                    foreach ($response['unique_actions'] as $item) {
                        if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                            $insert['unique_clicks'] = isset($item['value']) ? $item['value'] : 0;
                        }
                    }
                }

                $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
                $insert['spend'] = $response['spend'];
                $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
                $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
                $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;
                $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;

                $insert['time_range'] = $since;

                if (!empty($insert['spend'])) {
                    var_dump(
                        $insert
                    );
                }
            }
        }
        die;
    }
}
