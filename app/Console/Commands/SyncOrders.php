<?php

namespace App\Console\Commands;

use App\Excel\OrdersExport;
use App\Excel\OrdersImport;
use App\Since;
use Illuminate\Console\Command;
use App\Shop;
use App\Components\Shopify\ShopifyWrapper;
use Carbon\Carbon;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class SyncOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:sync {--shop=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize orders from ShopifyAPI';

    /**
     * @var int
     */
    public static $limit = 250;

    /**
     * Processor list
     *
     * @var array
     */
    protected $processorIds;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->processorIds = 1;

        $optionShop = $this->option('shop');

        $query = Shop::active();
        if ($optionShop) {
            $query->where('id', $optionShop);
        }

        if ($shops = $query->get()) {
            foreach ($shops as $shop) {
                $this->import($shop, $shop->getShopifyWrapper(), 1, static::$limit);
            }
        }
    }

    protected function import(Shop $shop, ShopifyWrapper $wrapper, $page, $limit)
    {
        $max = $limit + 1;
        $orderArray = [];
        $now = Carbon::now()->format('ymd');
        $filename = 'shopify_export_date_'.$now.'.xlsx';
        if (!Storage::exists($filename)) {
            $orderArrayHeading[] = array(
                'Order Name',
                'Created At',
                'Fulfillment Status',
                'Total',
                'Shipping Lines Total Price',
                'Notes',
                'Billing First Name',
                'Billing Last Name',
                'Billing Company',
                'Billing Address1',
                'Billing City',
                'Billing Province Code',
                'Billing Zip',
                'Billing Country Code',
                'Email',
                'Billing Phone',
                'Shipping First Name',
                'Shipping Last Name',
                'Shipping Address1',
                'Shipping City',
                'Shipping Province Code',
                'Shipping Zip',
                'Shipping Country Code',
                'Payment Gateway Names',
                'Discount Amount',
                'Subtotal',
                'Shipping Method',
                'Lineitem Refunded Subtotal',
                'Lineitem Tax Lines Total Price',
                'Lineitem SKU',
                'Total Lineitems Quantity',
                'Lineitem Name',
                'Lineitem Quantity',
                'Lineitem Price',
                'Lineitem Title',
                'Lineitem Product Image URL',
                'Production Link',
                'Customized Name',
                'Product ID',
                'Product Handle'
            );
            Excel::store(new OrdersExport($orderArrayHeading), $filename);
        }

        try {
            $max = $limit + 1;
            $since = Since::latest('id')->first();
            $params = [
                'limit' => 250,
                'status' => 'any',
            ];
            if ($since) {
                $params['since_id'] = $since->since_id;
            }

            $result = $wrapper->getOrders($params);

            if (!empty($result->orders) && $count = count($result->orders)) {
                $ordersReverse = $result->orders;
                $orderEnd = end($ordersReverse)->id;

                foreach ($ordersReverse as $order) {
                    foreach ($order->line_items as $item) {
                        $totalLineitemsQuantity = array_sum(array_column($order->line_items, 'quantity'));
                        $productHandle = $productImage = '';
                        if (isset($item->product_id)) {
                            $productImages = $wrapper->getImages($item->product_id);
                            $product = $wrapper->getProduct($item->product_id);
                            if (!empty($productImages->images) && count($productImages->images) > 0) {
                                $productImage = $productImages->images[0]->src;
                            }

                            if (!empty($product->product->handle)) {
                                $productHandle = $product->product->handle;
                            }
                        }

                        $productLink = '';
                        foreach ($item->properties as $property) {
                            if($property->name == '_customily-production-url') {
                                $productLink = $property->value;
                            }
                        }

                        $shippingMethod = count($order->shipping_lines) > 0 ? $order->shipping_lines[0]->title : '';

                        $orderArray[] = array(
                            'Order Name' => $order->name,
                            'Created At' => $order->created_at,
                            'Fulfillment Status' => $order->fulfillment_status,
                            'Total' => $order->current_total_price,
                            'Shipping Lines Total Price' => $item->price,
                            'Notes' => $order->note,
                            'Billing First Name' => $order->billing_address->first_name,
                            'Billing Last Name' => $order->billing_address->last_name,
                            'Billing Company' => $order->billing_address->company,
                            'Billing Address1' => $order->billing_address->address1,
                            'Billing City' => $order->billing_address->city,
                            'Billing Province Code' => $order->billing_address->province_code,
                            'Billing Zip' => $order->billing_address->zip,
                            'Billing Country Code' => $order->billing_address->country_code,
                            'Email' => $order->email,
                            'Billing Phone' => !empty($order->billing_address->phone) ? $order->billing_address->phone : '',
                            'Shipping First Name' => !empty($order->shipping_address->first_name) ? $order->shipping_address->first_name : '',
                            'Shipping Last Name' => !empty($order->shipping_address->last_name) ? $order->shipping_address->last_name : '',
                            'Shipping Address1' => !empty($order->shipping_address->address1) ? $order->shipping_address->address1 : '',
                            'Shipping City' => !empty($order->shipping_address->city) ? $order->shipping_address->city : '',
                            'Shipping Province Code' => !empty($order->shipping_address->province_code) ? $order->shipping_address->province_code : '',
                            'Shipping Zip' => !empty($order->shipping_address->zip) ? $order->shipping_address->zip : '',
                            'Shipping Country Code' => !empty($order->shipping_address->country_code) ? $order->shipping_address->country_code : '',
                            'Payment Gateway Names' => $order->payment_gateway_names,
                            'Discount Amount' => $order->total_discounts,
                            'Subtotal' => $order->subtotal_price,
                            'Shipping Method' => $shippingMethod,
                            'Lineitem Refunded Subtotal' => '',
                            'Lineitem Tax Lines Total Price' => $order->total_tax,
                            'Lineitem SKU' => $item->sku,
                            'Total Lineitems Quantity' => $totalLineitemsQuantity,
                            'Lineitem Name' => $item->name,
                            'Lineitem Quantity' => $item->quantity,
                            'Lineitem Price' => $item->price,
                            'Lineitem Title' => $item->title,
                            'Lineitem Product Image URL' => $productImage,
                            'Production Link' => $productLink,
                            'Customized Name' => '',
                            'Product ID' => $item->product_id,
                            'Product Handle' => $productHandle
                        );
                    }
                }
                Since::insert([
                    'since_id' => $orderEnd
                ]);
            }
            // TODO import to excel
            $this->excel($orderArray);
        } catch (\Exception $e) {
            $this->error('Cannot get orders from shop: ' . $shop->name);
            $this->error('Exception message: ' . $e->getMessage());
            $this->error('Happens at: ' . $e->getFile());
            $this->error('Line number: ' . $e->getLine());
            throw $e;
        }
    }

    function excel($data)
    {
        $now = Carbon::now()->format('ymd');
        $filename = 'shopify_export_date_'.$now.'.xlsx';
        Excel::store(new OrdersImport($data), $filename);
    }
}
