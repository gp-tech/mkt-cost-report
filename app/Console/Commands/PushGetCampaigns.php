<?php

namespace App\Console\Commands;

use App\Campaigns;
use App\FbCampaignAccount;
use App\FbCampaignApp;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\AdAccount;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Promise\all;

//use App\Jobs\GetAdsInsights;

class PushGetCampaigns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insight:get-campaigns {act_id : The ID of the ads account} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push get campaigns.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $j = DB::table('failed_jobs')->select('payload')->get();
//        $aw = json_decode($j[0]->payload)->data->command;
//        $cm = unserialize($aw);
//        dd($cm);

        // php /var/www/report/artisan queue:restart
        // php artisan insight:get-campaigns act_168132194711202
        $act_id = $this->argument('act_id');

        if ($act_id == 'today') {
            //$accounts = FbCampaignAccount::all();
            $accounts = FbCampaignAccount::where('status', 1)->get();
            //$accounts = FbCampaignAccount::where('fb_app_id', 76)->get();

            foreach ($accounts as $data) {
                if ($data->fb_app_id) {
                    $env = FbCampaignApp::where('id', $data->fb_app_id)->first();
                }

                if (empty($env) || !$env->status) continue;

                $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
                $api->setDefaultGraphVersion('11.0');
                //$api->setLogger(new CurlLogger());
                // start
                $fields = array(
                    'id',
                    'name',
                    'status',
                    'start_time'
                );
                $params = array (
//                    'date_preset' => 'this_month',
//                    'limit' => 1000
                );
                $act_id = 'act_'.$data->account_id;
                //$act_id = 'act_170173358270630';

                try {
                    $re = (new AdAccount($act_id))->getCampaigns(
                        $fields,
                        $params
                    )->getResponse()->getContent();
                    if (is_array($re['data'])) {
                        FbCampaignAccount::where('account_id', $data->account_id)->where('fb_app_id', $data->fb_app_id)->update(['status' => 1]);
                        foreach ($re['data'] as $campaign) {
                            if ($campaign['start_time'] < date('Y-m-d', strtotime('-6 months'))) {
                                continue;
                            }
                            //echo $campaign['name'] . PHP_EOL;
                            $campaign['campaign_id'] = $campaign['id'];
                            $campaign['account_id'] = $data->account_id;
                            $campaign['fb_app_id'] = $data->fb_app_id;
                            $campaign['start_time'] = date('Y-m-d', strtotime($campaign['start_time']));

                            if (\App\FbCampaign::where('campaign_id', $campaign['id'])->where('account_id', $data->account_id)->count()) {
                                \App\FbCampaign::where('campaign_id', $campaign['id'])->where('account_id', $data->account_id)->update(['status' => $campaign['status']]);
                                continue;
                            };
                            //dd($campaign);
                            unset($campaign['id']);
                            //dd($campaign);
                            \App\FbCampaign::forceCreate(
                                $campaign
                            );
                            echo 'Insert >  ' . $campaign['name'] . PHP_EOL;
                            //\Artisan::call('insight:get-campaigns-insight cam_' . $campaign['campaign_id'].'_'.$data->account_id);
                        }
                    }
                } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookClientException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookServerException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                    return $e->getMessage();
                } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                    return $e->getMessage();
                } catch(\Exception $e) {
                    echo 'GET ERROR ' . $data->account_id . ' Account ' . $data->fb_app_id. ' message ' . $e->getMessage() . PHP_EOL;
                    FbCampaignAccount::where('account_id', $data->account_id)->where('fb_app_id', $data->fb_app_id)->update(['status' => 0, 'message' => $e->getMessage()]);
                    continue;
                }
            }
        } else {
            $pos = stripos($act_id, '_');
            if ($pos === false) {
                $data = FbCampaignAccount::where('account_id', "{$act_id}")->first();
            } else {
                $act = explode('_', $act_id);
                $act_id = $act[0];
                $data = FbCampaignAccount::where('account_id', "{$act[0]}")->where('fb_app_id', $act[1])->first();
            }

            if ($data->fb_app_id) {
                $env = FbCampaignApp::where('id', $data->fb_app_id)->first();
            }
            if (empty($env) || !$env->status) return;
            $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
            $api->setDefaultGraphVersion('11.0');
            $api->setLogger(new CurlLogger());

            // start
            $fields = array(
                'id',
                'name',
                'status',
                'start_time'
            );
            $params = array();
            $act_id = 'act_' . $act_id;
            try {
                $re = (new AdAccount($act_id))->getCampaigns(
                    $fields,
                    $params
                )->getResponse()->getContent();

                if (is_array($re['data'])) {
                    foreach ($re['data'] as $campaign) {
                        $campaign['campaign_id'] = $campaign['id'];
                        //$campaign['name'] = strip_tags($campaign['name']);
                        $campaign['account_id'] = $data->account_id;
                        $campaign['fb_app_id'] = $data->fb_app_id;
                        $campaign['start_time'] = date('Y-m-d', strtotime($campaign['start_time']));
                        if ($campaign['start_time'] < date('Y-m-d', strtotime("-7 month"))) continue;
                        if (\App\FbCampaign::where('campaign_id', $campaign['id'])->where('account_id', $data->account_id)->where('fb_app_id', $data->fb_app_id)->count()) {
                            \App\FbCampaign::where('campaign_id', $campaign['id'])->where('account_id', $data->account_id)->where('fb_app_id', $data->fb_app_id)->update(['status' => $campaign['status']]);
                            continue;
                        }
                        unset($campaign['id']);
                        $save = \App\FbCampaign::forceCreate(
                            $campaign
                        );
                    }
                }
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookClientException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookServerException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                return $e->getMessage();
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                return $e->getMessage();
            } catch(\Exception $e) {
                return;
            }
        }

        //\Artisan::call('insight:get-campaigns-insight all_time');
    }
}
