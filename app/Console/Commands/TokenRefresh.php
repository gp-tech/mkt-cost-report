<?php

namespace App\Console\Commands;

use App\Campaigns;
use App\FbCampaignAccount;
use App\FbCampaignApp;
use Facebook\FacebookApp;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\AdAccount;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

//use App\Jobs\GetAdsInsights;

class TokenRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:token:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'token refresh';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function get_web_page($url) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);

        $content  = curl_exec($ch);

        curl_close($ch);

        return $content;
    }

    public function handle()
    {
        $apps = FbCampaignApp::get();
        foreach ($apps as $app) {
            $curl = 'https://graph.facebook.com/v11.0/oauth/access_token?grant_type=fb_exchange_token&client_id='.$app->app_id.'&client_secret='.$app->app_secret.'&fb_exchange_token=' . $app->access_token;
            $response = $this->get_web_page($curl);
            if ($response) {
                $resArr = json_decode($response);
                if (!empty($resArr->access_token)) {
                    FbCampaignApp::where('id', $app->id)->update(['access_token' => $resArr->access_token]);
                }
            }
        }
    }
}
