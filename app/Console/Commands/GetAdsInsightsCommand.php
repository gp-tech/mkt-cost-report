<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\GetAdsInsights;
//use App\Account;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\AdSet;
use FacebookAds\Object\AdsInsights;

class GetAdsInsightsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insight:get-ads-insights';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push get ads insights command to queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $accounts = AdsAccount::get();

        foreach ($accounts as $account) {
            GetAdsInsights::dispatch($account);
        }


        $app_id = "832375617311301";
        $app_secret = "836bc7034e25026e29165dae6ff5c0c6";
        $access_token = "EAAL1Coz9UkUBAETVADGgCZBYGFlDge4FkIVFYsXw2gghJLj4l5p1oMHVvStBVgzXt18GF8HHuWZCsLUOpiGla6BjOAhZCbF7FGQZBQBMVmNf370gLY1AXwnwCHBcyN38H8E709XFreUPBzKYlvAcaUXpTn5r8vQ1Vk0cF3ifiUwySMq6sY48";
        $account_id = "act_168132194711202";

        Api::init($app_id, $app_secret, $access_token);

        $id = '23846473529650584';

        $api = Api::init($app_id, $app_secret, $access_token);
        $api->setDefaultGraphVersion('10.0');
        $api->setLogger(new CurlLogger());

//curl -i -X GET 'https://graph.facebook.com/v10.0/oauth/access_token?grant_type=fb_exchange_token&client_id=832375617311301&client_secret=836bc7034e25026e29165dae6ff5c0c6&fb_exchange_token=EAAL1Coz9UkUBACqYq09V5vxdp8esNdwcmDR8hcq42Gsh3FBmTuiMZBS4WpOHOZBSfmkU04digxPdTCUo6iXbO1RJc4clvhqGbywA2wvuIUPrL631pLkXeiaf94dE5gMevrmKbSWS2yru9AVkdGr3xUrnriuaQ7G8wv6PBpPxSkZCFbYxRlZAPRG3f3KORgfsIXGb7pVGCZCYjvlE8RwOJ';



        $fields = array(
            'impressions',
        );
        $params = array(
            'breakdown' => 'publisher_platform',
        );
        echo json_encode((new AdSet($id))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent(), JSON_PRETTY_PRINT);
        echo '<pre>';
        die;

        $accounts = Account::get();

        foreach ($accounts as $account) {
            GetMediaInsights::dispatch($account);
        }


        die;

        $act_id = $this->argument('act_id');
        $api = Api::init(ENV('APP_ID'), ENV('APP_SECRET'), ENV('ACCESS_TOKEN'));
        $api->setDefaultGraphVersion('10.0');
        $api->setLogger(new CurlLogger());

        $fields = array(
            'results', 'spend', 'unique_link_clicks_ctr',
            'cost_per_unique_click', 'cost_per_unique_action_type', 'actions', 'cpm', 'purchase_roas', 'website_purchase_roas'
        );
        $params = array(
            'breakdown' => 'publisher_platform',
        );
        $id = '23846442750910584';
        echo json_encode((new AdSet($id))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent(), JSON_PRETTY_PRINT);
        echo '<pre>';
        die;

        // done
        $fields = array(
            'id',
            'name',
            'status'
        );
        $params = array(
            'effective_status' => array('ACTIVE', 'PAUSED'),
        );
        $re = (new AdAccount($act_id))->getCampaigns(
            $fields,
            $params
        )->getResponse()->getContent();

        if (is_array($re['data'])) {
            foreach ($re['data'] as $campaign) {
                $campaign['campaign_id'] = $campaign['id'];
                unset($campaign['id']);
                \App\FbCampaign::firstOrCreate(
                    $campaign
                );
            }
        }
        //
        die;

        foreach ($accounts as $account) {
            GetAdsInsights::dispatch($account);
        }


        $account_id = "act_168132194711202";


        $id = '23846473529650584';

        $api = Api::init($app_id, $app_secret, $access_token);
        $api->setDefaultGraphVersion('10.0');
        $api->setLogger(new CurlLogger());

//curl -i -X GET 'https://graph.facebook.com/v10.0/oauth/access_token?grant_type=fb_exchange_token&client_id=832375617311301&client_secret=836bc7034e25026e29165dae6ff5c0c6&fb_exchange_token=EAAL1Coz9UkUBACqYq09V5vxdp8esNdwcmDR8hcq42Gsh3FBmTuiMZBS4WpOHOZBSfmkU04digxPdTCUo6iXbO1RJc4clvhqGbywA2wvuIUPrL631pLkXeiaf94dE5gMevrmKbSWS2yru9AVkdGr3xUrnriuaQ7G8wv6PBpPxSkZCFbYxRlZAPRG3f3KORgfsIXGb7pVGCZCYjvlE8RwOJ';


        $fields = array(
            'impressions', 'results', 'reach', 'cost_per_result', 'spend'
        );
        $params = array(
            'breakdown' => 'publisher_platform',
        );
        echo json_encode((new AdSet($id))->getInsights(
            $fields,
            $params
        )->getResponse()->getContent(), JSON_PRETTY_PRINT);
        echo '<pre>';
        die;

        $accounts = FbCampaign::get();

        foreach ($accounts as $account) {
            GetMediaInsights::dispatch($account);
        }
    }
}
