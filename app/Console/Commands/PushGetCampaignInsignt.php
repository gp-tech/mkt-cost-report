<?php

namespace App\Console\Commands;

use App\Campaigns;
use App\FbCampaignAccount;
use App\FbCampaignApp;
use App\Jobs\GetFbCampaignInsights;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AbstractCrudObject;
use FacebookAds\Object\Campaign;
use Illuminate\Console\Command;

class PushGetCampaignInsignt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insight:get-campaigns-insight {time}'; // time = today || range_23846473529650584

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push get campaign insights.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    //cung 1 camp nam trong 2 app khac nhau thi du lieu co khac nhau ko.
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //date_default_timezone_set('America/Phoenix');
        //$this->getAdsInsights();
        $time = $this->argument('time');

        if ('today' == $time) {
            $fb_campaigns = \App\FbCampaign::where('status', '=', 'ACTIVE')->get();
        } elseif ($time == 'yesterday') {
            $fb_campaigns = \App\FbCampaign::where('status', '=', 'ACTIVE')->get();
        } else {
            $fb_campaigns = \App\FbCampaign::get();
        }

//        else {
//            //$fb_campaigns = \App\FbCampaign::get();
//            $fb_campaigns = \App\FbCampaign::where('account_id', $this->argument('time'))->get();
//            $time = 'all';
//        }

        //$fb_campaigns = \App\FbCampaign::where('account_id', '170173358270630')->get();
        //dd($fb_campaigns);
        //$fb_campaigns = \App\FbCampaign::whereIn('fb_app_id', [45])->get();
        //$time = $this->argument('time');
        //$fb_campaigns = \App\FbCampaign::where('account_id', 968528397304416)->get();
        //dd($fb_campaigns);
        //$fb_campaigns = \App\FbCampaign::where('id', 12578)->get();

        //dd($fb_campaigns);
        //$fb_campaigns = \App\FbCampaign::where('status' , '=' ,'ERROR')->get();

        //dd($fb_campaigns);
        //dd(count($fb_campaigns));
        //$fb_campaigns = \App\FbCampaign::get();
        //dd($fb_campaigns);
        //$fb_campaigns = \App\FbCampaign::where('status', 'ACTIVE')->get();
        $i = 0;
        foreach ($fb_campaigns as $fb_campaign) {
            //if ($fb_campaign->status != 'ACTIVE') continue;
            //\Artisan::call('insight:get-campaigns '.$account->account_id);

            if (!empty($fb_campaign->fb_app_id)) {
                $env = FbCampaignApp::where('id', $fb_campaign->fb_app_id)->first();

                if (empty($env)) {
                    //\App\FbCampaign::where('id', $fb_campaign->id)->delete();
                    //\App\FbCampaignInsight::where('fb_campaign_id', $fb_campaign->id)->delete();
                    continue;
                }

                if ($env->status != 1) continue;
                if ($fb_campaign->start_time < date('Y-m-d', strtotime('-3 months'))) {
                    $fb_campaign->start_time = date('Y-m-d', strtotime('-3 months'));
                }

                //\App\FbCampaign::where('id', $fb_campaign->id)->update(['status' => 'ACTIVE']);
                if ($i > 0 && $i % 1000 == 0) {
                    echo 'Sleeping ' . $i . PHP_EOL;
                    sleep(5);
                }

                //$this->getAdsInsights($fb_campaign, $time, $env);
                GetFbCampaignInsights::dispatch($fb_campaign, $time, $env);
            }
            $i++;
        }
    }

    public function getAdsInsights($fb_campaign, $time, $env)
    {
        date_default_timezone_set('America/Phoenix');
        $api = Api::init($env->app_id, $env->app_secret, $env->access_token);
        $api->setDefaultGraphVersion('11.0');
        //$api->setLogger(new CurlLogger());
        //echo date('Y-m-d h:i:s');die;
        //if ('2015-02-24' < date('Y-m-d', strtotime("-1 month"))) return;
        $fields = array(
            'spend', 'unique_link_clicks_ctr', 'clicks', 'impressions', 'unique_clicks', 'reach', 'unique_actions',
            'cost_per_unique_click', 'cost_per_unique_action_type', 'actions', 'cpm', 'purchase_roas',
            'website_purchase_roas', 'account_currency', 'action_values'
        );

        //\App\FbCampaign::where('id', $fb_campaign->id)->update(['status' => 'ACTIVE']);

        if ($time == 'all') {
            $start_time = $fb_campaign->start_time;
            $begin = new \DateTime($start_time);
            $end = new \DateTime(date('Y-m-d'));
            $end = $end->modify('+1 day');

            $interval = new \DateInterval('P1D');
            $daterange = new \DatePeriod($begin, $interval, $end);
            foreach ($daterange as $date) {
                $since = $date->format("Y-m-d");
                $params = array(
                    'time_increment' => 1,
                    'time_range'     => json_encode([
                        'since' => date('Y-m-d', strtotime($since)),
                        'until' => date('Y-m-d', strtotime($since))
                    ])
                );
                $id = $fb_campaign->campaign_id;
                try {
                    $response = (new Campaign($id))->getInsights(
                        $fields,
                        $params
                    )->getResponse()->getContent();
                    if (!isset($response['data'])) return;
                    if (count($response['data']) < 1) return;

                    $this->updateOrCreateData($response, $since, $fb_campaign->id, $fb_campaign->campaign_id);

                } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookClientException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookServerException $e) {
                    return $e->getMessage();
                } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                    return $e->getMessage();
                } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                    return $e->getMessage();
                } catch(\Exception $e) {
                    echo 'GET ERROR APP = ' . $fb_campaign->fb_app_id . ' message ' . $e->getMessage() . PHP_EOL;
                    //\App\FbCampaign::where('id', $fb_campaign->id)->update(['status' => 'ERROR']);
                    return;
                }
            }
        } else {
            $params = array(
                'time_increment' => 1,
                'time_range'     => json_encode([
                    'since' => date('Y-m-d', strtotime($time)),
                    'until' => date('Y-m-d', strtotime($time))
                ])
            );
            $id = $fb_campaign->campaign_id;

            //if ($fb_campaign->start_time < date('Y-m-d', strtotime("-2 month"))) return;



            try {
                $response = (new Campaign($id))->getInsights(
                    $fields,
                    $params
                )->getResponse()->getContent();



                if (!isset($response['data'])) return;
                if (count($response['data']) < 1) return;

                $this->updateOrCreateData($response, $time, $fb_campaign->id, $fb_campaign->campaign_id);
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthenticationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookAuthorizationException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookClientException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookOtherException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookServerException $e) {
                return $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookThrottleException $e) {
                return $e->getMessage();
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                return $e->getMessage();
            } catch(\Exception $e) {
                echo 'GET ERROR APP = ' . $fb_campaign->fb_app_id . ' message ' . $e->getMessage() . PHP_EOL;
                //\App\FbCampaign::where('id', $fb_campaign->id)->update(['status' => 'ERROR']);
                return;
            }
        }
    }

    public function updateOrCreateData($response, $time, $id, $campaign_id) {


        $insert = [];
        $insert['cost_per_unique_add_to_cart'] = $insert['cost_per_unique_click'] = 0;
        $insert['purchase'] = $insert['unique_adds_to_cart'] = $insert['results'] = 0;
        $insert['clicks'] = $insert['unique_clicks'] = $insert['impressions'] = $insert['cpm'] = 0;
        $insert['spend'] = $insert['unique_link_clicks_ctr'] = 0;
        $insert['time_range'] = date('Y-m-d', strtotime($time));
        $insert['fb_campaign_id'] = $id;
        $insert['campaign_id'] = $campaign_id;

        if (count($response['data']) < 1) return;

        if (count($response['data']) > 0) {
            $response = $response['data'][0];

            if ($response['impressions'] < 1) return;

            if (isset($response['actions'])) {
                foreach ($response['actions'] as $item) {

                    if (isset($item['action_type']) && $item['action_type'] == 'add_to_cart') {
                        $insert['unique_adds_to_cart'] = $item['value'];
                    }

                    if (isset($item['action_type']) && $item['action_type'] == 'purchase') {
                        $insert['purchase'] = $insert['results'] = $item['value'];
                    }
                }
            }

            if (isset($response['unique_actions'])) {
                foreach ($response['unique_actions'] as $item) {
                    if (isset($item['action_type']) && $item['action_type'] == 'link_click') {
                        $insert['unique_clicks'] = $item['value'];
                    }
                }
            }

            $insert['cost_per_unique_click'] = isset($response['cost_per_unique_click']) ? $response['cost_per_unique_click'] : 0;
            $insert['spend'] = $response['spend'];

            if ($response['account_currency'] == 'VND') {
                $insert['spend'] = round($response['spend'] / 22600, 2);
            }

            if ($response['account_currency'] == 'EUR') {
                $insert['spend'] = round($response['spend'] / 0.82, 2);
            }
            $insert['unique_link_clicks_ctr'] = isset($response['unique_link_clicks_ctr']) ? $response['unique_link_clicks_ctr'] : 0;
            $insert['impressions'] = isset($response['impressions']) ? $response['impressions'] : 0;
            $insert['clicks'] = isset($response['clicks']) ? $response['clicks'] : 0;

            $insert['cpm'] = isset($response['cpm']) ? $response['cpm'] : 0;
            echo 'Updating campaign: ' . $campaign_id . PHP_EOL;
            if (\App\FbCampaignInsight::where('campaign_id', $campaign_id)->where('time_range', $insert['time_range'])->count()) {
                \App\FbCampaignInsight::where('campaign_id', $campaign_id)->where('time_range', $insert['time_range'])->update($insert);
            } else {
                \App\FbCampaignInsight::forceCreate(
                    $insert
                );
            }

        }

    }
}
