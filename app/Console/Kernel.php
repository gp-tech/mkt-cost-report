<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('orders:sync')->hourlyAt(15);
        $schedule->command('app:refresh all')->dailyAt('08:00');
        $schedule->command('app:token:refresh')->weekly();

        $schedule->command('insight:get-campaigns-insight today')->everyTwoHours();

        $schedule->command('insight:get-campaigns-insight yesterday')->dailyAt('00:03');
        $schedule->command('insight:get-campaigns-insight yesterday')->dailyAt('17:15');

        $schedule->command('insight:get-campaigns today')->dailyAt('01:00');
        $schedule->command('insight:get-campaigns today')->dailyAt('13:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
