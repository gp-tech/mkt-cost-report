<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = [
        'uid',
        'name',
        'barcode'
    ];
    
    public function links() {
        return $this->hasMany('App\Link', 'barcode', 'barcode');
    }
}
