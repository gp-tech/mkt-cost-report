<?php

namespace App\Jobs;

use App\FbCampaign;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GetFbCampaignInsights implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $fb_campaign;
    protected $time;
    protected $env;
    protected $by_time;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(FbCampaign $fb_campaign, $time, $env)
    {
        $this->fb_campaign = $fb_campaign;
        $this->time = $time;
        $this->env = $env;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            $this->fb_campaign->getAdsInsights($this->time, $this->env);
        }
        catch (\Exception $e) {
            report($e);
        }
    }
}
