<?php
namespace App\Excel;

use Maatwebsite\Excel\Concerns\FromArray;

class OrdersExport implements FromArray
{
    private $array;

    public function __construct($array){
        $this->array = $array;
    }

    public function array(): array{
        return $this->array;
    }
}

