<?php

namespace App\Excel;

use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Excel;
use Maatwebsite\Excel\Files\LocalTemporaryFile;

class OrdersImport implements FromCollection, ShouldAutoSize, WithEvents
{
    use Exportable;

    private $array;
    protected $calledByEvent;


    public function __construct($array){
        $this->array = $array;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        if ($this->calledByEvent) { // flag
            return collect($this->array);
        }

        return collect([]);
    }

    public function registerEvents(): array
    {
        return [
            BeforeWriting::class => function(BeforeWriting $event) {

                $now = Carbon::now()->format('ymd');
                $filename = 'shopify_export_date_'.$now.'.xlsx';

                $templateFile = new LocalTemporaryFile(storage_path('app/').$filename);
                $event->writer->reopen($templateFile, Excel::XLSX);
                $event->writer->getSheetByIndex(0);


                $this->calledByEvent = true; // set the flag
                $event->writer->getSheetByIndex(0)->export($event->getConcernable()); // call the export on the first sheet

                return $event->getWriter()->getSheetByIndex(0);
            },
        ];
    }
}
