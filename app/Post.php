<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'name', 'image', 'niche', 'page_id', 'campaign_url', 'campaign_code', 'post_id', 'pushed_date'
    ];
}
