<?php
namespace App\Repositories;

use App\Shop;

class ShopRepository
{
    public static function paginateShopList($limit) {
        return Shop::select([
            'id',
            'name',
            'domain',
            'api_key',
            'api_secret',
            'api_password',
            'status',
        ])->orderBy('id', 'asc')->paginate($limit);
    }
}