<?php
namespace App\Repositories;

use App\Product;

class ProductRepository
{

    public static function paginateProductList($limit, $conditions = []) {
//         $query = Order::with(['shop' => function ($builder) {
//             $builder->select('id', 'name', 'domain');
//         }, 'user' => function ($builder) {
//             $builder->select('id', 'name');
//         }])->select([
//             'id',
//             'shop_id',
//             'user_id',
//             'name',
//             'created_at',
//             'email',
//             'financial_status',
//             'fulfillment_status',
//             'total_price',
//         ])->orderBy('created_at', 'desc');
        
//         if (!empty($conditions['user'])) {
//             $query->where('user_id', $conditions['user']);
//         }
//         return $query->paginate($limit);
    }
    
    /**
     *
     * @param array $data            
     */
    public static function importProduct($data)
    {
        // convert to array
        if (is_object($data)) {
            $data = (array) $data;
        }
        
        if (! empty($data['product_id'])) {
            $product = Product::firstOrNew(['uid' => $data['product_id']]);
            
            $product->fill([
                'name' => empty($data['name']) ? '' : $data['name'],
                'barcode' => empty($data['barcode']) ? '' : $data['barcode'],
            ]);
            
            $product->save();
            
            return $product;
        }
        
        return null;
    }
}