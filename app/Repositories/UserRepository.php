<?php
namespace App\Repositories;

use App\User;
use App\Order;

class UserRepository
{
    public static function paginateUserList($limit = 0, $options = []) {
        $query = User::visible()->select([
            'id',
            'name',
            'email',
            'role_id',
            'status',
        ])->orderBy('id', 'asc');

        if (!empty($options['role_id'])) {
            $query->where('role_id', $options['role_id']);
        }

        if ($limit) {
            $result = $query->paginate($limit);
        } else {
            $result = $query->get();
        }

        return $result;
    }

    public static function allProcessorIds()
    {
        $ids = User::get('id');
        dd($ids);
        $userOrders = [];
        foreach ($ids as $id) {
            $userOrders[$id] = Order::where('user_id', $id)->count();
        }

        asort($userOrders);
        return array_keys($userOrders);
    }

}
