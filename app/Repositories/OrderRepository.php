<?php
namespace App\Repositories;

use App\Order;
use DB;
use App\Components\Shopify\ShopifyHelper;
use App\OrderProduct;

class OrderRepository
{

    public static function paginateOrderList($limit, $conditions = []) {
        $query = Order::with(['shop' => function ($builder) {
            $builder->select('id', 'name', 'domain');
        }, 'user' => function ($builder) {
            $builder->select('id', 'name');
        }, 'pending_user' => function ($builder) {
            $builder->select('id', 'name');
        }]);

        $query->leftJoin('addresses', function($join) {
            $join->on('orders.id', '=', 'addresses.order_id');
            $join->where('addresses.type', '=', 1);
        });
        $query->select([
            'orders.*',
            'addresses.first_name',
            'addresses.last_name',
        ])
        ->orderByRaw('IF(cancelled_at IS NULL, 0, 1) ASC')
        ->orderBy('is_processed', 'asc')
        ->orderByRaw('FIELD(financial_status,"paid","partially_paid","refunded","pending")')
        ->orderByRaw('FIELD(fulfillment_status,"partial","fulfilled")')
        ->orderBy('created_at', 'desc');

        if (!empty($conditions['name'])) {
            $query->where(function ($query) use ($conditions) {
//                 $query->orWhere('addresses.first_name', 'like', '%' . $conditions['name'] . '%');
//                 $query->orWhere('addresses.last_name', 'like', '%' . $conditions['name'] . '%');
                $query->orWhere(\DB::raw('CONCAT(first_name, " ", last_name)'), 'like', '%' . $conditions['name'] . '%');

                $query->orWhere('orders.uid', 'like', '%' . $conditions['name'] . '%');
                $query->orWhere('orders.name', 'like', '%' . $conditions['name'] . '%');

                //$query->orWhereIn('orders.id');
            });

        }
        if (!empty($conditions['user'])) {
            $query->where('orders.user_id', $conditions['user']);
        }
        if (!empty($conditions['shop'])) {
            $query->where('shop_id', $conditions['shop']);
        }
        if (!empty($conditions['pending_user'])) {
            $query->where('pending_user_id', $conditions['pending_user']);
        }
        if (!empty($conditions['payment_status'])) {
            $query->where('financial_status', $conditions['payment_status']);
        }
        if (isset($conditions['processed'])) {
            if ($conditions['processed'] == '0' || $conditions['processed'] == '1') {
                $query->where('is_processed', $conditions['processed']);
            }
        }
        if (!empty($conditions['fulfillment_status'])) {
            if ($conditions['fulfillment_status'] == 'null') {
                $query->where('fulfillment_status', null);
            } else {
                $query->where('fulfillment_status', $conditions['fulfillment_status']);
            }
        }

//         dd($query->toSql());
        return $query->paginate($limit);
    }

    /**
     *
     * @param array $data
     */
    public static function importOrder($shop, &$processorIds, $data)
    {
        // convert to array
        if (is_object($data)) {
            $data = (array) $data;
        }
        unset($data['note']);

        $data = ShopifyHelper::parseDateTimeFields($data);

        // check exiting order
        $order = Order::where('uid', $data['id'])->where('shop_id', $shop->id)->first();
        if (! $order) {
            $order = new Order();
            if (empty($processorIds)) {
                $userId = 0;
            } else {
                $userId = array_pop($processorIds);
                array_unshift($processorIds, $userId);
            }
            $order->user_id = $userId;
            $order->shop_id = $shop->id;
        }
        $order->fill($data);
        $order->fill([
            'uid' => $data['id'],
        ]);

        $order->save();

        if (! empty($data['line_items'])) {
            foreach ($data['line_items'] as $item) {
                if (empty($item->variant_id)) {
                    $item->barcode = null;
                } else {
                    $item->barcode = $shop->getShopifyWrapper()->getBarcode($item->variant_id);
                }
                $product = ProductRepository::importProduct($item);
                if ($product) {
                    OrderProduct::create([
                        'order_id' => $order->id,
                        'product_id' => $product->id,
                        'quantity' => $item->quantity,
                        'price' => $item->price,
                        'total_discount' => $item->total_discount,
                    ]);
                }
            }
        }

        // insert billing_address
        if (! empty($data['billing_address'])) {
            //AddressRepository::importAddress(0, $order->id, $data['billing_address']);
        }

        // insert shipping_address
        if (! empty($data['shipping_address'])) {
            //AddressRepository::importAddress(1, $order->id, $data['shipping_address']);
        }

    }

    public static function countAssigningToUser($userId = null)
    {
        if ($userId === null) {
            $userId = \Auth::id();
        }

        return Order::where('pending_user_id', $userId)->count();
    }
}
