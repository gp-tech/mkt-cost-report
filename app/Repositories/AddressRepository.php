<?php
namespace App\Repositories;

use App\Address;

class AddressRepository
{

    public static function paginateAddressList($limit, $conditions = []) {
//         $query = Order::with(['shop' => function ($builder) {
//             $builder->select('id', 'name', 'domain');
//         }, 'user' => function ($builder) {
//             $builder->select('id', 'name');
//         }])->select([
//             'id',
//             'shop_id',
//             'user_id',
//             'name',
//             'created_at',
//             'email',
//             'financial_status',
//             'fulfillment_status',
//             'total_price',
//         ])->orderBy('created_at', 'desc');
        
//         if (!empty($conditions['user'])) {
//             $query->where('user_id', $conditions['user']);
//         }
//         return $query->paginate($limit);
    }
    
    /**
     *
     * @param array $data            
     */
    public static function importAddress($type, $orderId, $data)
    {
        // convert to array
        if (is_object($data)) {
            $data = (array) $data;
        }
        
        $address = Address::firstOrNew(['type' => $type, 'order_id' => $orderId]);
        
        $address->fill([
            'first_name' => empty($data['first_name']) ? '' : $data['first_name'],
            'last_name' => empty($data['last_name']) ? '' : $data['last_name'],
            'address1' => empty($data['address1']) ? '' : $data['address1'],
            'address2' => empty($data['address2']) ? '' : $data['address2'],
            'telephone' => empty($data['phone']) ? '' : $data['phone'],
            'city' => empty($data['city']) ? '' : $data['city'],
            'company' => empty($data['company']) ? '' : $data['company'],
            'state' => empty($data['province']) ? '' : $data['province'],
            'state_code' => empty($data['province_code']) ? '' : $data['province_code'],
            'zip' => empty($data['zip']) ? '' : $data['zip'],
            'country' => empty($data['country']) ? '' : $data['country'],
            'country_code' => empty($data['country_code']) ? '' : $data['country_code'],
            
        ]);
        
        $address->save();
    }
}