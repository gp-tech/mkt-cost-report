<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Wall extends Model
{
    //
    protected $fillable = [
        'billing_id',
        'host',
        'port',
        'country',
        'state',
        'city',
    ];

    public static function getNext($billingId)
    {
        $maxRoller = static::where('billing_id', '=', $billingId)->max('roller');
        
        $nextItem = static::where('billing_id', '=', $billingId)->where('roller', '<', $maxRoller)->first();
        if (empty($nextItem)) {
            $nextItem = static::where('billing_id', '=', $billingId)->where('roller', '=', $maxRoller)->first();
            $nextItem->roller = $maxRoller + 1;
        } else {
            $nextItem->roller = $maxRoller;
        }
        
        $nextItem->save();
        
        return $nextItem;
    }
}
