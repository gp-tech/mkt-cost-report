@extends('layouts.admin')

@section('title', 'Edit Caption')

@section('content')
    <h1>Edit Caption</h1>
    <form id="caption-form" method="post" action="/captions/edit/{{ $caption->id }}">
        <div class="form-group">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
                @if(is_array(session()->get('success')))
                <ul>
                    @foreach (session()->get('success') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
                @else
                    {{ session()->get('success') }}
                @endif
            </div>
        @endif
        </div>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ $caption->name }}">
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <input type="content" class="form-control" id="content" name="content" value="{{ $caption->content }}">
        </div>
        <div class="form-group">
            <label for="default">Default</label>
            <input type="checkbox" value="1" class="form-control" id="default" name="default" {{ $caption->default == 1 ? 'checked="checked"' : '' }}>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection