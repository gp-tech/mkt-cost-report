@extends('layouts.admin')

@section('title', 'Caption list')

@section('content')
    <h1>Caption list</h1>
    <div class="posts-buttons">
        <div class="float-right">
            <a class="btn btn-primary" href="/captions/add">Add</a>
        </div>
        <div class="clearfix"></div>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col" class="col-2">Name</th>
            <th scope="col" class="col-4">Content</th>
            <th scope="col">Default</th>
            <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
        @foreach ($captions as $caption)
            <tr>
                <th scope="row">{{ $no++ }}</th>
                <td>
                    {{ $caption->name }}
                </td>
                <td>
                    {{ $caption->content }}
                </td>
                <td>
                    {{ $caption->default == 1 ? 'Yes' : '' }}
                </td>
                <td>
                    <a href="/captions/edit/{{ $caption->id }}">Edit</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
