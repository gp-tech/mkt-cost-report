@extends('layouts.admin')

@section('title', 'Create Facebook App')

@section('content')
{{--    <h1 class="h3 mb-2 text-gray-800"></h1>--}}
    <div class="card mb-4">
        <div class="card-header">Create Facebook APP</div>
        <div class="card-body">
            <p>
                <a class="btn btn-danger" target="_blank" href="https://developers.facebook.com/apps/">Link create app.</a> |
                <a class="btn btn-info" target="_blank" href="https://developers.facebook.com/tools/explorer/?method=GET&path=%3CAD_SET_ID%3E%2Finsights%3Ffields%3Dcost_per_store_visit_action%252Cstore_visit_actions&version=v10.0">Link to get Token.</a>
                <a class="btn btn-info" target="_blank" href="/img/2021-03-22 23-56-34.png">Need to permission.</a>
            </p>

            <form id="user-form" method="post" action="/fb-campaigns/add-app">
                <div class="form-group">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session()->has('success'))
                    <div class="alert alert-success">
                        @if(is_array(session()->get('success')))
                        <ul>
                            @foreach (session()->get('success') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                        @else
                            {{ session()->get('success') }}
                        @endif
                    </div>
                @endif
                </div>
                <div class="form-group">
                    <label for="name">App Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="content">User ID</label>
                    <input type="content" class="form-control" id="user_id" name="user_id">
                </div>
                <div class="form-group">
                    <label for="content">App ID</label>
                    <input type="content" class="form-control" id="app_id" name="app_id">
                </div>
                <div class="form-group">
                    <label for="content">App Secret</label>
                    <input type="content" class="form-control" id="app_secret" name="app_secret">
                </div>
                <div class="form-group">
                    <label for="content">Access Token</label>
                    <input type="content" class="form-control" id="access_token" name="access_token">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>


        </div>
    </div>
@endsection
