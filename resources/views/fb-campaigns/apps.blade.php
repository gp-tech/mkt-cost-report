@extends('layouts.admin')

@section('title', 'Acc')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Facebook APP</h1>

    @if (isset($success))
    <div class="alert  alert-success alert-dismissible fade show" role="alert">
        {{$success}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @if (session('success'))
        <div class="alert  alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    @if (session('error'))
        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
            {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    @if (isset($error))
        <div class="alert  alert-danger alert-dismissible fade show" role="alert">
            {{$error}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <a class="btn btn-sm btn-primary" href="/fb-campaigns/add-app">Create APP</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th></th>
                        <th>NAME</th>
                        <th>USER ID</th>
                        <th>APP STATUS</th>
                        <th>ACCOUNT</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach ($FbCampaignApps as $FbCampaignApp)
                    <?php if (2 == $FbCampaignApp->status) continue;  ?>
                        <tr>
                            <td scope="row">{{ $FbCampaignApp->id }}</td>
                            <td scope="row">{{ $FbCampaignApp->name }}</td>
                            <td scope="row" class="text-right">{{ $FbCampaignApp->user_id }}</td>
                            <td scope="row" class="text-right"><?= $FbCampaignApp->status ? '<span class="btn-info" style="padding: 5px 10px">RUNNING</span>' : '<span class="btn-danger" style="padding: 5px 10px">ERROR</span>' ?><br/>
                                <?= $FbCampaignApp->status == 0 ? $FbCampaignApp->message : '' ?>
                            </td>
                            <td scope="row" class="text-right"> <a href="/fb-campaigns/accounts?app_id={{$FbCampaignApp->id}}" class="btn btn-info"> Accounts </a>
                                | <a href="/fb-campaigns/get-account/{{$FbCampaignApp->id}}" class="btn btn-info"> Get Accounts </a>
                            </td>
                            <td scope="row" class="text-right"><a href="/fb-campaigns/app-refresh/{{$FbCampaignApp->id}}" class="btn btn-primary"> Refresh </a> |
                                <a href="/fb-campaigns/app-edit/{{$FbCampaignApp->id}}" class="btn btn-info"> Edit </a> |
                                <a href="/fb-campaigns/app-delete/{{$FbCampaignApp->id}}" class="btn btn-danger"
                                   onclick="return confirm('Are you want to delete the app?')"> Delete </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Custom styles for this page -->
    @push('styles')<!-- Bootstrap core JavaScript-->
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush
    @push('scripts')<!-- Bootstrap core JavaScript-->
        <script src="/vendor/jquery/jquery.min.js"></script>
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->

        <script>
            $(function() {
                $('#dataTable').DataTable({
                    "iDisplayLength": 25
                });

            });
        </script>
    @endpush
@endsection
