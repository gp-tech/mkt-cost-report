@extends('layouts.admin')

@section('title', 'Account')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"></h1>
    <!-- DataTales Example -->
    <form id="campaigns-form" action="/fb-campaigns/accounts">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" id="fb_app_id" name="app_id">
                        <option value="">Select App</option>
                        @foreach ($fb_apps as $fb_app)
                            <option value="{{ $fb_app->id }}" {{ isset($_GET['app_id']) && $fb_app->id == $_GET['app_id'] ? 'selected="selected"' : '' }}>{{ $fb_app->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select class="form-control" name="status">
                        <option value="1" {{ isset($status) && 1 == $status ? 'selected="selected"' : '' }}>Running</option>
                        <option value="0" {{ isset($status) && 0 == $status ? 'selected="selected"' : '' }}>Error</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <input id="btnSearch" type="submit" class="btn btn-primary" value="Search"/>
            </div>
        </div>
    </form>

    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Facebook Account
            <a class="btn btn-sm btn-primary" href="/fb-campaigns/add-account">Create Facebook Account</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th></th>
                        <th width="30%">NAME</th>
                        <th>ACCOUNT ID</th>
                        <th>STATUS</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach ($FbCampaignAccounts as $FbCampaignAccount)
                        <tr>
                            <td scope="row">{{ $i++ }}</td>
                            <td scope="row">{{ $FbCampaignAccount->name }}</td>
                            <td scope="row">{{ $FbCampaignAccount->account_id }}</td>
                            <td scope="row"><?= $FbCampaignAccount->status ? '<span class="btn-info" style="padding: 5px 10px">RUNNING</span>' : '<span class="btn-danger" style="padding: 5px 10px">ERROR</span>' ?><br/>
                                <?= $FbCampaignAccount->status != 1 ? $FbCampaignAccount->message : '' ?></td>
                            <td scope="row" width="5%"><a href="/fb-campaigns/index?act={{ $FbCampaignAccount->account_id }}" class="btn btn-xs btn-info"> Insight</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Custom styles for this page -->
    @push('styles')<!-- Bootstrap core JavaScript-->
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush
    @push('scripts')<!-- Bootstrap core JavaScript-->
        <script src="/vendor/jquery/jquery.min.js"></script>
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->

        <script>
            $(function() {
                $('#dataTable').DataTable({
                    "iDisplayLength": 25
                });

            });
        </script>
    @endpush
@endsection
