@extends('layouts.admin')

@section('title', 'Create Campaign Account')

@section('content')
    <div class="card mb-4">
        <div class="card-header">Create Campaign Account</div>
        <div class="card-body">
            <form id="user-form" method="post" action="/fb-campaigns/add-account">
                <div class="form-group">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session()->has('success'))
                        <div class="alert alert-success">
                            @if(is_array(session()->get('success')))
                                <ul>
                                    @foreach (session()->get('success') as $message)
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            @else
                                {{ session()->get('success') }}
                            @endif
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="group">FB Application</label>
                    <select class="form-control" id="fb_app_id" name="fb_app_id">
                        <option value=""></option>
                        @foreach ($fb_apps as $fb_app)
                            <option value="{{ $fb_app->id }}" {{ $fb_app->id == $fb_app->fb_app_id ? 'selected="selected"' : '' }}>{{ $fb_app->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="content">Account ID</label>
                    <input type="content" class="form-control" id="account_id" name="account_id">
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection
