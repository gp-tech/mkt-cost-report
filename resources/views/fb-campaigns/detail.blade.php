@extends('layouts.admin')

@section('title', 'Campaigns')

@section('content')
    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Data Detail
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="campaignDataTable" width="100%" cellspacing="0">
                    <thead>

                        <tr>
                        <th scope="col"></th>
                        <th scope="col">Results</th>
                        <th scope="col">Spend</th>
                        <th scope="col">Unique link CTR</th>
                        <th scope="col">CPUC</th>
                        <th scope="col">CPUATC</th>
                        <th scope="col">UATC</th>
                        <th scope="col">CPM</th>
                        <th scope="col">Purchase</th>
                        <th scope="col">Time</th>

                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 0;
                    $label = [
                        0 => 'Today',
                        1 => 'Yesterday',
                        2 => 'Last 2 Day',
                        3 => 'Last 3 Day',
                    ];
                    ?>
                    @foreach ($result as $key => $campaign)
                        <?php if (isset($result[$i+1])) { ?>
                        <?php if ($i<3): ?>
                        <tr>
                            <td scope="row">{{ $label[$key] }}</td>
                            <td scope="row">{{ ($campaign->results) ? $campaign->results . ' | ' . $result[$i+1]->results : 0 }}</td>
                            <td scope="row">{{ ($campaign->spend) ? $campaign->spend : 0 }}</td>
                            <td scope="row">{{ ($campaign->unique_link_clicks_ctr) ? round($campaign->unique_link_clicks_ctr, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->cost_per_unique_click) ? round($campaign->cost_per_unique_click, 2): 0 }}</td>
                            <td scope="row">{{ ($campaign->cost_per_unique_add_to_cart) ? round($campaign->cost_per_unique_add_to_cart, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->unique_adds_to_cart) ? round($campaign->unique_adds_to_cart, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->cpm) ? round($campaign->cpm, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->purchase) ? $campaign->purchase : 0 }}</td>
                            <td scope="row">{{ ($campaign->time_range) ? date('Y-m-d', strtotime($campaign->time_range)) : 0 }}</td>
                        </tr>
                        <?php else:?>
                        <tr>
                            <td scope="row">{{ $label[$key] }}</td>
                            <td scope="row">{{ ($campaign->results) ? $campaign->results : 0 }}</td>
                            <td scope="row">{{ ($campaign->spend) ? $campaign->spend : 0 }}</td>
                            <td scope="row">{{ ($campaign->unique_link_clicks_ctr) ? round($campaign->unique_link_clicks_ctr, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->cost_per_unique_click) ? round($campaign->cost_per_unique_click, 2): 0 }}</td>
                            <td scope="row">{{ ($campaign->cost_per_unique_add_to_cart) ? round($campaign->cost_per_unique_add_to_cart, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->unique_adds_to_cart) ? round($campaign->unique_adds_to_cart, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->cpm) ? round($campaign->cpm, 2) : 0 }}</td>
                            <td scope="row">{{ ($campaign->purchase) ? $campaign->purchase : 0 }}</td>
                            <td scope="row">{{ ($campaign->time_range) ? date('Y-m-d', strtotime($campaign->time_range)) : 0 }}</td>
                        </tr>
                        <?php endif;?>
                        <?php $i++; } ?>
                    @endforeach
                    <tr>
                        <td scope="row">Total Last 3 Day</td>
                        <td scope="row">{{ ($last_3day->results) ? $last_3day->results : 0 }}</td>
                        <td scope="row">{{ ($last_3day->spend) ? $last_3day->spend : 0 }}</td>
                        <td scope="row">{{ ($last_3day->unique_link_clicks_ctr) ? round($last_3day->unique_link_clicks_ctr, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_3day->cost_per_unique_click) ? round($last_3day->cost_per_unique_click, 2): 0 }}</td>
                        <td scope="row">{{ ($last_3day->cost_per_unique_add_to_cart) ? round($last_3day->cost_per_unique_add_to_cart, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_3day->unique_adds_to_cart) ? round($last_3day->unique_adds_to_cart, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_3day->cpm) ? round($last_3day->cpm, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_3day->purchase) ? $last_3day->purchase : 0 }}</td>
                        <td scope="row">Last 3 day</td>
                    </tr>
                    <tr>
                        <td scope="row">Total Last 7 Day</td>
                        <td scope="row">{{ ($last_7day->results) ? $last_7day->results - $result[0]->results : 0 }}</td>
                        <td scope="row">{{ ($last_7day->spend) ? $last_7day->spend : 0 }}</td>
                        <td scope="row">{{ ($last_7day->unique_link_clicks_ctr) ? round($last_7day->unique_link_clicks_ctr - $result[0]->unique_link_clicks_ctr, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_7day->cost_per_unique_click) ? round($last_7day->cost_per_unique_click - $result[0]->cost_per_unique_click, 2): 0 }}</td>
                        <td scope="row">{{ ($last_7day->cost_per_unique_add_to_cart) ? round($last_7day->cost_per_unique_add_to_cart - $result[0]->cost_per_unique_add_to_cart, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_7day->unique_adds_to_cart) ? round($last_7day->unique_adds_to_cart - $result[0]->unique_adds_to_cart, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_7day->cpm) ? round($last_7day->cpm - $result[0]->cpm, 2) : 0 }}</td>
                        <td scope="row">{{ ($last_7day->purchase) ? $last_7day->purchase - $result[0]->purchase : 0 }}</td>
                        <td scope="row">Last 7 day</td>
                    </tr>
                    <tr>
                        <td scope="row">All Time</td>
                        <td scope="row">{{ ($campaign_total->results) ? $campaign_total->results : 0 }}</td>
                        <td scope="row">{{ ($campaign_total->spend) ? $campaign_total->spend : 0 }}</td>
                        <td scope="row">{{ ($campaign_total->unique_link_clicks_ctr) ? round($campaign_total->unique_link_clicks_ctr, 2) : 0 }}</td>
                        <td scope="row">{{ ($campaign_total->cost_per_unique_click) ? round($campaign_total->cost_per_unique_click, 2): 0 }}</td>
                        <td scope="row">{{ ($campaign_total->cost_per_unique_add_to_cart) ? round($campaign_total->cost_per_unique_add_to_cart, 2) : 0 }}</td>
                        <td scope="row">{{ ($campaign_total->unique_adds_to_cart) ? round($campaign_total->unique_adds_to_cart, 2) : 0 }}</td>
                        <td scope="row">{{ ($campaign_total->cpm) ? round($campaign_total->cpm, 2) : 0 }}</td>
                        <td scope="row">{{ ($campaign_total->purchase) ? $campaign_total->purchase : 0 }}</td>
                        <td scope="row">Total</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
