@extends('layouts.admin')

@section('title', 'Jobs Running')

@section('content')
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800"></h1>

    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Jobs Running
            <a class="btn btn-sm btn-primary" href="/fb-campaigns/clear-job" onclick="return confirm('Are you want to clear all jobs?')">Clear Jobs</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>UIID</th>
                        <th width="30%">NAME</th>
                        <th>ATTEMPTS</th>
                        <th>CREATED_AT</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach ($job_data as $job)
                        <tr>
                            <td scope="row">{{ $job['uuid'] }}</td>
                            <td scope="row">{{ $job['displayName'] }}</td>
                            <td scope="row">{{ $job['attempts'] }}</td>
                            <td scope="row">{{ $job['created_at'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Custom styles for this page -->
    @push('styles')<!-- Bootstrap core JavaScript-->
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @endpush
    @push('scripts')<!-- Bootstrap core JavaScript-->
        <script src="/vendor/jquery/jquery.min.js"></script>
        <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="/js/sb-admin-2.min.js"></script>

        <!-- Page level plugins -->
        <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->

        <script>
            $(function() {
                $('#dataTable').DataTable({
                    "iDisplayLength": 25
                });

            });
        </script>
    @endpush
@endsection
