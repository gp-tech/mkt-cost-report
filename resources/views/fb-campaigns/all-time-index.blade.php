@extends('layouts.admin')

@section('title', 'Facebook Campaigns')

@section('content')
    <h1 class="h3 mb-2 text-gray-800"></h1>
    <form id="campaigns-form" action="/fb-campaigns/index">
        <div class="row">
            <div class="col-md-2">
                <input type="text" name="keyword" placeholder="Keyword" class="form-control" value="{{ $keyword }}">
            </div>
            {{--<div class="col-md-2">
                <input type="text" name="keyword2" placeholder="Keyword 2" class="form-control" value="{{ $keyword2 }}">
            </div>--}}
            <div class="col-md-3">
                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                    <input type="hidden" name="start_date" class="start_date" />
                    <input type="hidden" name="end_date" class="end_date" />
                </div>
            </div>
            <div class="col-md-2">
                <input id="btnSearch" type="submit" class="btn btn-primary" value="Search"/>
            </div>
        </div>
    </form>
    </br>
    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Facebook Insight
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th scope="col">Campaign ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Results</th>
                        <th scope="col">Impressions</th>
                        <th scope="col">Amount spent</th>
                        <th scope="col">Unique link clicks</th>
                        <th scope="col">Unique CTR</th>
                        <th scope="col">CPM</th>
                        <th scope="col">Cost per result</th>
                        <th scope="col">Cost per add to cart</th>
                        <th scope="col">Cost per unique link click</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($campaigns as $campaign)
                        <tr>
                            <td scope="row">{{ $campaign->campaign_id }}</td>
                            <td scope="row" width="15%">{{ $campaign->name }}</td>
                            <th scope="col">{{ $campaign->results }}</th>
                            <th scope="col">{{ $campaign->impressions }}</th>
                            <th scope="col">{{ $campaign->spend }}</th>

                            <th scope="col">{{ round($campaign->unique_clicks, 2) }}</th>

                            <th scope="col">{{
                            $campaign->impressions > 0 ?
                            round($campaign->unique_clicks / ($campaign->impressions / 1000), 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->impressions > 0 ?
                            round(($campaign->spend / ($campaign->impressions / 1000)) , 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->results > 0 ?
                            round($campaign->spend / $campaign->results, 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->unique_adds_to_cart > 0 ?
                            round($campaign->spend / $campaign->unique_adds_to_cart, 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->unique_clicks > 0 ?
                            round($campaign->spend / $campaign->unique_clicks, 2) : 0}}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Custom styles for this page -->
    @push('styles')<!-- Bootstrap core JavaScript-->
    <link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    @endpush
    @push('scripts')<!-- Bootstrap core JavaScript-->
    <script src="/vendor/jquery/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <!-- Page level custom scripts -->
        <script>
            $(function() {
                var start = moment.utc("<?= $start; ?>");
                var end = moment.utc("<?= $end; ?>");

                function cb(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $('#reportrange input.start_date').val(start.format('YYYY-MM-DD'));
                    $('#reportrange input.end_date').val(end.format('YYYY-MM-DD'));
                }

                $('#reportrange').daterangepicker({
                    startDate: start,
                    endDate: end,
                    ranges: {
                        'All time': [moment('2017-01-01'), moment()],
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, cb);

                cb(start, end);
                $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
                    $('#campaigns-form').submit();
                });
            });

            $(function() {
                var table = $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endsection
