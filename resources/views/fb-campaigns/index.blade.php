@extends('layouts.admin')

@section('title', 'Facebook Campaigns')

@section('content')
    <h1 class="h3 mb-2 text-gray-800"></h1>
    <form id="campaigns-form" action="/fb-campaigns/index">
        <div class="row">
            <div class="col-md-2">
                <input type="text" name="keyword" placeholder="Contains" class="form-control" value="{{ isset($keyword) ? $keyword : '' }}">

            </div>
            <div class="col-md-2">
                <input type="text" name="keyword1" placeholder="Doesn't Contains" class="form-control" value="{{ isset($keyword1) ? $keyword1 : '' }}">
            </div>
            <div class="col-md-5">
                <div id="reportrange"
                     style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                    <input type="hidden" name="start_date" class="start_date"/>
                    <input type="hidden" name="end_date" class="end_date"/>
                    <input type="hidden" name="act" value="<?= isset($_GET['act']) ? $_GET['act'] : ''; ?>" />
                </div>
            </div>
            <div class="col-md-2">
                <input id="btnSearch" type="submit" class="btn btn-primary" value="Search"/>
            </div>
        </div>
    </form>
    </br>
    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Facebook Campaigns
        </div>
        <?php if (count($campaigns)) { ?>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Results</th>

                        <th scope="col">Impressions</th>
                        <th scope="col">Amount spent</th>
                        <th scope="col">Unique link clicks</th>

                        <th scope="col">CPM</th>
                        <th scope="col">Cost per result</th>
                        <th scope="col">Cost per add to cart</th>
                        <th scope="col">Cost per unique link click</th>
                        <th scope="col">Purchase roas</th>
                        <th scope="col">Purchase conversion value</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($campaigns as $campaign)
                        <tr>
                            <td scope="row" width="10%">{{ $campaign->campaign_id }} <br/> {{ $campaign->name }}</td>
                            <th scope="col">{{ $campaign->campaignInsight->sum('results') }}</th>
                            <th scope="col">{{ $campaign->campaignInsight->sum('impressions') }}</th>
                            <th scope="col">{{ round($campaign->campaignInsight->sum('spend'), 2) }}</th>

                            <th scope="col">{{ round($campaign->campaignInsight->sum('unique_clicks'), 2) }}</th>

                            <th scope="col">{{
                            $campaign->campaignInsight->sum('impressions') > 0 ?
                            round(($campaign->campaignInsight->sum('spend') / ($campaign->campaignInsight->sum('impressions') / 1000)) , 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->campaignInsight->sum('results') > 0 ?
                            round($campaign->campaignInsight->sum('spend') / $campaign->campaignInsight->sum('results'), 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->campaignInsight->sum('unique_adds_to_cart') > 0 ?
                            round($campaign->campaignInsight->sum('spend') / $campaign->campaignInsight->sum('unique_adds_to_cart'), 2) : 0 }}</th>

                            <th scope="col">{{
                            $campaign->campaignInsight->sum('unique_clicks') > 0 ?
                            round($campaign->campaignInsight->sum('spend') / $campaign->campaignInsight->sum('unique_clicks'), 2) : 0}}</th>

                            <th scope="col">{{ round($campaign->campaignInsight->sum('purchase_roas'), 2) }}</th>
                            <th scope="col">{{ round($campaign->campaignInsight->sum('purchase_conversion_value'), 2) }}</th>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <?php if ($total['impressions'] > 1) : ?>
                    <tr>
                        <th scope="col">Total</th>
                        <th scope="col">{{ $total['results'] }}</th>
                        <th scope="col">{{ $total['impressions'] }}</th>
                        <th scope="col">{{ round($total['spend'], 2) }}</th>

                        <th scope="col">{{ $total['unique_clicks'] }}</th>


                        <th scope="col">{{ round(($total['spend'] / ($total['impressions'] / 1000)) , 2) }}</th>
                        <th scope="col">{{ $total['results'] ? round($total['spend'] / $total['results'], 2) : 0 }}</th>
                        <th scope="col">{{ $total['unique_adds_to_cart'] ? round($total['spend'] / $total['unique_adds_to_cart'], 2) : 0 }}</th>
                        <th scope="col">{{ $total['unique_clicks'] ? round($total['spend'] / $total['unique_clicks'], 2) : 0 }}</th>
                        <th scope="col">{{ $total['purchase_roas'] }}</th>
                        <th scope="col">{{ $total['purchase_conversion_value'] }}</th>
                    </tr>
                    <?php endif; ?>
                    </tfoot>
                </table>
            </div>
        </div>
        <?php } else { ?>
            Not found Data
        <?php } ?>
    </div>
@endsection
<!-- Custom styles for this page -->
@push('styles')<!-- Bootstrap core JavaScript-->
<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css"/>
@endpush
@push('scripts')<!-- Bootstrap core JavaScript-->

<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.24/api/sum().js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(function () {
        var today = moment.utc("<?=  date('Y-m-d'); ?>");
        var yesterday = moment.utc("<?=  date('Y-m-d', strtotime('-1 day')); ?>");
        var start = moment.utc("<?= $start; ?>");
        var end = moment.utc("<?= $end; ?>");

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#reportrange input.start_date').val(start.format('YYYY-MM-DD'));
            $('#reportrange input.end_date').val(end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'All time': [moment('2017-01-01'), moment()],
                'Today': [today, today],
                'Yesterday': [yesterday, yesterday],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

        var table = $('#dataTable').DataTable({
            searching: false,
            order: [[ 1, "desc" ]],
            "iDisplayLength": 20,
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
            ]
        });
    });
</script>
@endpush
