@extends('layouts.admin')

@section('title', 'Facebook Campaigns')

@section('content')
    <h1 class="h3 mb-2 text-gray-800"></h1>
    <form id="campaigns-form" action="/shopify/orders">
        <div class="row">
            <div class="col-md-2">
                <input type="text" name="keyword" placeholder="Keyword" class="form-control" value="{{ $keyword }}">
            </div>
            {{--<div class="col-md-2">
                <input type="text" name="keyword2" placeholder="Keyword 2" class="form-control" value="{{ $keyword2 }}">
            </div>--}}
            <div class="col-md-3">
                <div id="reportrange"
                     style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                    <input type="hidden" name="start_date" class="start_date"/>
                    <input type="hidden" name="end_date" class="end_date"/>
                    <input type="hidden" name="act" value="<?= isset($_GET['act']) ? $_GET['act'] : ''; ?>" />
                </div>
            </div>
            <div class="col-md-2">
                <input id="btnSearch" type="submit" class="btn btn-primary" value="Search"/>
            </div>
        </div>
    </form>
    </br>
    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Tee Campaigns
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th scope="col">Order ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>

                        <th scope="col">Total Price</th>
                        <th scope="col">Currency</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($orders as $campaign)
                        <tr>
                            <td scope="row">{{ $campaign->order_number }}</td>
                            <th scope="col">{{ $campaign->name }}</th>
                            <th scope="col">{{ $campaign->email }}</th>
                            <th scope="col">${{ $campaign->total_price }}</th>
                            <th scope="col">{{ $campaign->currency }}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
<!-- Custom styles for this page -->
@push('styles')<!-- Bootstrap core JavaScript-->
<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endpush
@push('scripts')<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.24/api/sum().js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(function () {
        var today = moment.utc("<?=  date('Y-m-d'); ?>");
        var yesterday = moment.utc("<?=  date('Y-m-d', strtotime('-1 day')); ?>");
        var start = moment.utc("<?= $start; ?>");
        var end = moment.utc("<?= $end; ?>");

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#reportrange input.start_date').val(start.format('YYYY-MM-DD'));
            $('#reportrange input.end_date').val(end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'All time': [moment('2017-01-01'), moment()],
                'Today': [today, today],
                'Yesterday': [yesterday, yesterday],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

        var table = $('#dataTable').DataTable({
            searching: false,
            order: [[ 1, "desc" ]],
            "iDisplayLength": 50,
        });
    });
</script>
@endpush
