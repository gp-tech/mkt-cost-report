@extends('layouts.admin')

@section('title', 'Create FB Page')

@section('content')
    <h1>Create FB Page</h1>
    <form id="user-form" method="post" action="/createPage">
        <div class="form-group">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
                @if(is_array(session()->get('success')))
                <ul>
                    @foreach (session()->get('success') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
                @else
                    {{ session()->get('success') }}
                @endif
            </div>
        @endif
        </div>
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ Request::old('name') }}">
        </div>
        <div class="form-group">
            <label for="email">FB ID</label>
            <input type="text" class="form-control" id="fb_id" name="fb_id" value="{{ Request::old('fb_id') }}">
        </div>
        <input type="hidden" name="token" value="{{ csrf_token() }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
