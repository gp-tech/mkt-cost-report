@extends('layouts.admin')

@section('title', 'Post list')
@section('content')
    <h1 class="mt-4">Pages list</h1>
    <div class="posts-buttons row">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr style="display:none;">
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="border: 0px !important;">
                        <a href="/pages/add" id="btnToken" type="button"  class="btn btn-primary">Create FB Page</a>
                    </td>
                </tr>
                </tbody>
            </table>
            @if (session()->has('success'))
                <div class="alert alert-success">
                    @if(is_array(session()->get('success')))
                        <ul>
                            @foreach (session()->get('success') as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    @else
                        {{ session()->get('success') }}
                    @endif
                </div>
            @endif
        </div>
    </div>
    <div class="card mb-8">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Fb Id</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($pages as $page)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <th scope="row">{{ $page->name }}</th>
                            <th scope="row">{{ $page->fb_id }}</th>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="center">
                </div>
            </div>
        </div>
    </div>
@endsection
