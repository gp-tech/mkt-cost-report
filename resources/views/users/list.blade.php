@extends('layouts.admin')

@section('title', 'User list')
@section('content')
    <h1 class="mt-4">User list</h1>
    <div class="posts-buttons row">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr style="display:none;">
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="border: 0px !important;">
                        <a href="/users/add" id="btnToken" type="button"  class="btn btn-primary">Add user</a>
                    </td>
                    <td style="border: 0px !important;">
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Group</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td></td>
                    <td>
                        {{ $user->name }}
                    </td>
                    <td>
                        {{ $user->email }}
                    </td>
                    <td>
                        {{ ($user->group == 1 ? 'Administrator' : ($user->group == 2 ? 'Content' : ($user->group == 3 ? 'Sale' : ''))) }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="center">
            {{ $users->withQueryString()->links() }}
        </div>
    </div>

@endsection
