@extends('layouts.admin')

@section('title', 'Sales')

@section('content')
    <h1>Sales</h1>
    <div  id="sale-form" class="row" style="margin-bottom: 20px">
        <div class="col-xl-12">
            <div class="card mb-4 information">
                <div class="card-header">
                    <i class="fas fa-chart-area mr-1"></i>
                    Information
                </div>
                <div class="card-body">
                    <div class="col-xl-12">
                        <label>Cookie</label>
                        <textarea id="txaCookie" name="cookie">{{ $settings['cookie'] }}</textarea>
                    </div>
                    <div class="row col-xl-12">
                        <div class="col-xl-6">
                            <label>Authorization:</label>
                            <input type="text" id="txtAuth" name="auth" value="{{ $settings['auth'] }}"/>
                        </div>
                        <div class="col-xl-6">
                            <label>Default List ID:</label>
                            <input type="text" id="txtDefaultList" name="defaultList" value="{{ $settings['list'] }}"/>
                        </div>
                    </div>
                    <div class="row col-xl-12">
                        <div class="col-xl-6">
                            <label>XSRF-TOKEN:</label>
                            <input type="text" id="txtToken" name="token" value="{{ $settings['token'] }}"/>
                        </div>
                    </div>
                    <div class="row col-xl-12">
                        <div class="col-xl-6">
                            <label>Notify to Email:</label>
                            <input type="text" id="txtNotifyEmail" name="notifyEmail"  value="{{ $settings['notifyEmail'] }}"/>
                        </div>
                        <div class="col-xl-6">
                            <label>Report to Email:</label>
                            <input type="text" id="txtReportEmail" name="reportEmail"  value="{{ $settings['reportEmail'] }}"/>
                        </div>
                    </div>
                    <div class="col-xl-12" style="margin-top: 10px;">
                        <button id="btnSave" type="submit" class="btn btn-info">Save</button>
                    </div>
                </div>
            </div>
            <!-- <div class="card mb-4 information">
                <div class="card-header">
                    <i class="fas fa-chart-area mr-1"></i>
                    Campaign Detail
                </div>
                <div class="card-body row">
                    <div class="col-xl-8">
                        <div class="row">
                            <div class="col-xl-3">
                                <label>From(YYYY/MM/DD):</label>
                                <input type="text" id="txtFrom" class="datepicker" name="from" />
                            </div>
                            <div class="col-xl-3">
                                <label>To(YYYY/MM/DD):</label>
                                <input type="text" id="txtTo" class="datepicker" name="to" />
                            </div>
                            <div class="col-xl-6">
                                <label style="width: 100%; margin-bottom: 0">&nbsp;</label>
                                <button id="btnRange" type="submit" class="btn btn-primary">Get Range</button>
                                <button id="btnYesterday" type="submit" class="btn btn-primary">Get Yesterday</button>
                            </div>
                        </div>
                    </div>
                    <div class="row col-xl-4">
                        <div class="row">
                            <div class="col-xl-6">
                                <label>Month(YYYY.MM)</label>
                                <input type="text" class="monthpicker" id="txtMonth" name="month" />
                            </div>
                            <div class="col-xl-6">
                                <label style="width: 100%; margin-bottom: 0">&nbsp;</label>
                                <button id="btnExport" type="submit" class="btn btn-success">Export Month</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card mb-4 information">
                <div class="card-header">
                    <i class="fas fa-chart-area mr-1"></i>
                    Sendy Filter
                </div>
                <div class="card-body">
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary" id="btnPushSendy">Push Sendy</button>
                    </div>
                    <div class="table-responsive">
                        <form id="sendyFilterForm">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <table class="table table-bordered" id="tblSendy" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th scope="col"></th>
                                <th scope="col">Filter (Start with)</th>
                                <th scope="col">List ID</th>
                                <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($settings["filters"] as $key => $filter)
                                <tr data-id="{{ $key }}">
                                    <td><input type="checkbox" name="filters[{{ $key }}][check]" {{ $filter['check'] == '1' ? 'checked="checked"' : ''}} value="1"/></td>
                                    <td><input type="text" name="filters[{{ $key }}][filter]" value="{{$filter['filter']}}" /></td>
                                    <td><input type="text" name="filters[{{ $key }}][listID]" value="{{$filter['listID']}}" /></td>
                                    <td><button type="button" class="btn btn-danger btnRemoveFilter" data-id="{{ $key }}" data-toggle="modal" data-target="#removeModal">Delete</button></td>
                                </tr>
                                @endforeach
                            </tbody>
                           
                        </table>
                        </form>
                    </div>
                </div>
            </div>
            <button id="btnToday" type="submit" class="btn btn-primary">Get Today</button>
            <button id="btnAll" type="submit" class="btn btn-primary">Get All</button>
            <span id="message"></span> -->
        </div>
    <style>
    </style>
    <script>
        $(function(){
            $( ".datepicker" ).datepicker({ 
                dateFormat: 'yy/mm/dd'
            });

            $( ".monthpicker" ).datepicker({ 
                dateFormat: 'yy.mm'
            });
      
            var isRunning = false;
            var sleepTime = 500;

            $('#btnSave').click(function(){
                var params = {
                    _token: $('input[name=_token]').val(),
                    cookie: $('#txaCookie').val(),
                    auth: $('#txtAuth').val(),
                    token: $('#txtToken').val(),
                    notifyEmail: $('#txtNotifyEmail').val(),
                    reportEmail: $('#txtReportEmail').val()
                };
                /*
                $.get('/sales/check', function(response){
                    console.log(response);
                });
                */

                $.post('/sales/save', params, function(response){
                    console.log(response);
                });
            });

            $('#btnAll').click(function(){
                var button = $(this);
                var page = 1;
                var type = 'read';
                var i = 0;
                var subscribed = 0;

                button.attr('disabled', 'disabled');
                $('#message').text('All time - Reading page');

                var allProcess = setInterval(() => {
                    if(isRunning){
                       return; 
                    }

                    isRunning = true;

                    switch(type){
                        case 'read':
                           
                            $.get('/sales/getAll', {type: type, page: page}, function(response){
                                if(response.hasNext){
                                    $('#message').text('All time - Reading page: ' + page);
                                    page = response.page;
                                    isRunning = false;
                                }
                                else{
                                    type = 'download';
                                    page = 1;
                                    isRunning = false;
                                    $('#message').text('All time - Downloading');
                                    //clearInterval(allProcess);
                                }
                            });
                        break;
                        case 'download':
                            $.get('/sales/getAll', {type: type, page: page}, function(response){
                                if(response.hasNext){
                                    $('#message').text('All time - Downloading : ' + page);
                                    page = response.page;
                                    isRunning = false;
                                }
                                else{
                                    if(response.message != ''){
                                        clearInterval(todayProcess);
                                        alert(response.message);
                                        isRunning = false;
                                    }
                                    else{
                                        $('#message').text('All Time - Reading Files');
                                        type = 'push';
                                        page = 1;
                                        isRunning = false;
                                    }
                                }
                            });
                        break;
                        case 'push':
                            $.get('/sales/getAll', {type: type, page: page}, function(response){
                                if(response.hasNext){
                                    subscribed += response.subscribed;
                                    $('#message').text('All Time - Reading Files: ' + page + '/' + response.total +' | Subscribe Email: '+ subscribed);
                                    page = response.page;
                                    isRunning = false;
                                }
                                else{
                                    $('#message').text('All Time - Reading Files: ' + response.total + '/' + response.total +' | Subscribe Email: '+ subscribed);
                                    clearInterval(allProcess);
                                    isRunning = false;
                                    button.removeAttr('disabled');
                                    alert('Done!');
                                }
                            });
                        break;
                    }
                    
                }, sleepTime);
            });

            $('#btnToday').click(function(){
                var button = $(this);
                var page = 1;
                var type = 'read';
                var i = 0;
                var subscribed = 0;

                button.attr('disabled', 'disabled');
                $('#message').text('Today - Reading page');

                var todayProcess = setInterval(() => {
                    if(isRunning){
                       return; 
                    }

                    isRunning = true;

                    switch(type){
                        case 'read':
                            $.get('/sales/getToday', {type: type, page: page}, function(response){
                                if(response.hasNext){
                                    $('#message').text('Today - Reading page: ' + page);
                                    page = response.page;
                                    isRunning = false;
                                }
                                else{
                                    type = 'download';
                                    $('#message').text('Today - Downloading');
                                    page = 1;
                                    isRunning = false;
                                    //clearInterval(todayProcess);
                                }
                            });
                        break;
                        case 'download':
                            $.get('/sales/getToday', {type: type, page: page}, function(response){
                                if(response.hasNext){
                                    $('#message').text('Today - Downloading : ' + page);
                                    page = response.page;
                                    isRunning = false;
                                }
                                else{
                                    if(response.message != ''){
                                        alert(response.message);
                                        isRunning = false;
                                    }
                                    else{
                                        $('#message').text('Today - Reading Files');
                                        type = 'push';
                                        page = 1;
                                        isRunning = false;
                                    }
                                }
                            });
                        break;
                        case 'push':
                            $.get('/sales/getToday', {type: type, page: page}, function(response){
                                if(response.hasNext){
                                    subscribed += response.subscribed;
                                    $('#message').text('Today - Reading Files: ' + page + '/' + response.total +' | Subscribe Email: '+ subscribed);
                                    page = response.page;
                                    isRunning = false;
                                }
                                else{
                                    $('#message').text('Today - Reading Files: ' + response.total + '/' + response.total +' | Subscribe Email: '+ subscribed);
                                    clearInterval(todayProcess);
                                    isRunning = false;
                                    button.removeAttr('disabled');
                                    alert('Done!');
                                }
                            });
                        break;
                    }
                    
                }, sleepTime);
            });

            $('#btnPushSendy').click(function(){
                var page = 1;
                var subscribed = 0;

                $('#message').text('All Time - Reading Files');

                var sendyProcess = setInterval(() => {
                    if(isRunning){
                       return; 
                    }

                    isRunning = true;

                    $.get('/sales/sendy', {page: page}, function(response){
                        if(response.hasNext){
                            subscribed += response.subscribed;
                            page = response.page;
                            isRunning = false;
                            $('#message').text('All Time - Reading Files: ' + page + '/' + response.total +' | Subscribe Email: '+ subscribed);
                        }
                        else{
                            clearInterval(sendyProcess);
                        }
                    });
                }, sleepTime);
            });

            $('#btnExport').click(function(){
                var month = $('#txtMonth').val();

                location.href = '/sales/exportMonth?month=' + month;
            });

            $('#btnRange').click(function(){
                var button = $(this);
                var params = {
                    _token: $('input[name=_token]').val(),
                    from: $('#txtFrom').val(),
                    to: $('#txtTo').val()
                };
                console.log(params);

                button.attr('disabled', 'disabled');

                $.post('/sales/getRange', params,  function(response){
                    if(response.status == 'error'){
                        alert(response.message);
                    }
                    else{
                        alert('Done!');
                    }

                    setTimeout(function(){
                        button.removeAttr('disabled');
                    }, 1500);
                });
            });

            $('#btnYesterday').click(function(){
                var button = $('#btnYesterday');
                button.attr('disabled', 'disabled');

                $.get('/sales/getYesterday', function(response){
                    if(response.status == 'error'){
                        alert(response.message);
                    }
                    else{
                        alert('Done!');
                    }

                    setTimeout(function(){
                        button.removeAttr('disabled');
                    }, 1500);
                });
            });

            $(document).delegate('.btnRemoveFilter', 'click', function(){
                if(confirm('Are you sure want to delete this filter?')){
                    $(this).parents('tr').remove();
                }
            });

            $('.btnAddFilter').click(function(){
                var lastChild = $('#tblSendy tbody tr:last-child');
                var html = '';
                var count = 0;

                if(lastChild.length > 0){
                    count = parseInt(lastChild.attr('data-id')) + 1;
                }

                html += '<tr data-id="'+ count +'">';
                html += '   <td><input type="checkbox" name="filters['+ count +'][check]" /></td>';
                html += '   <td><input type="text" name="filters['+ count +'][filter]" value="" /></td>';
                html += '   <td><input type="text" name="filters['+ count +'][listID]" value="" /></td>';
                html += '   <td><button type="button" class="btn btn-danger btnRemoveFilter">Delete</button></td>';
                html += '</tr>';

                $('#tblSendy tbody').append(html);
            });

            $('#sendyFilterForm').submit(function(){
                var button = $('#btnSaveFilter');
                params = $(this).serializeArray();
                button.attr('disabled', 'disabled');

                $.post('/sales/saveSendy', params,  function(response){
                    if(response.status == 'error'){
                        alert(response.message);
                    }

                    setTimeout(function(){
                        button.removeAttr('disabled');
                    }, 1500);
                });

                console.log('aaa');
                return false;
            })
        });
    </script>
@endsection