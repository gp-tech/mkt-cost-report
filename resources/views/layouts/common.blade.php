<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="/bootstrap.min.css">
        <link href="style.css" rel="stylesheet">

        <script src="/jquery-3.5.1.min.js"></script>
        <script src="/popper.min.js"></script>
        <script src="/bootstrap.min.js"></script>
        <script src="script.js" type="text/javascript"></script>
    </head>
    <body>
    <div class="container">
        @yield('content')
    </div>
    </body>
</html>