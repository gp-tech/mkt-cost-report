{{--<nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">--}}
{{--    <div class="sb-sidenav-menu">--}}
{{--        <div class="nav">--}}
{{--            <div class="sb-sidenav-menu-heading">Core</div>--}}
{{--            <a href="/dashboard" class="nav-link collapsed {{ Request::is('dashboard') ? 'active' : '' }}">Dashboard</a>--}}

{{--            <a href="/sales/index" class="nav-link collapsed {{ Request::is('sales/index')  ? 'active' : '' }}">Sales</a>--}}
{{--            <a href="/campaigns/index" class="nav-link collapsed {{ Request::is('campaigns/index')  ? 'active' : '' }}">TeeChip Campaigns</a>--}}
{{--            <a href="/fb-campaigns/index" class="nav-link collapsed {{ Request::is('fb-campaigns/index')  ? 'active' : '' }}">Facebook Campaigns</a>--}}

{{--            <a class="nav-link collapsed" href="/logout">Logout</a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</nav>--}}

<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Golden</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Request::is('dashboard') ? 'active' : '' }}">
        <a class="nav-link" href="/dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        E-commerce
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ Request::is('fb-campaigns/index', 'fb-campaigns/apps', 'fb-campaigns/add-app') ? 'active' : '' }}">
        <a class="nav-link collapsed" href=""
           data-toggle="collapse" data-target="#collapseTwo"
           aria-expanded="true" aria-controls="collapseTwo">

            <span>Facebook</span>
        </a>
        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/fb-campaigns/apps">Facebook Apps</a>
                <a class="collapse-item" href="/fb-campaigns/accounts">Campaign Account</a>
                <a class="collapse-item" href="/fb-campaigns/index">Facebook Insight</a>
                <a class="collapse-item" href="/fb-campaigns/jobs">Jobs</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item {{ Request::is('campaigns/index')  ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
           aria-expanded="true" aria-controls="collapseUtilities">

            <span>TeeChip</span>
        </a>
        <div id="collapseUtilities" class="collapse show" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/campaigns/index">TeeChip Insight</a>
            </div>
        </div>
    </li>

    <li class="nav-item {{ Request::is('shopify/orders')  ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
           aria-expanded="true" aria-controls="collapseUtilities">
            <span>Shopify</span>
        </a>
        <div id="collapseShopify" class="collapse show" aria-labelledby="headingUtilities"
             data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="/shopify/orders">Orders</a>
            </div>
        </div>
    </li>

    <li class="nav-item {{ Request::is('report-campaigns')  ? 'active' : '' }}">
        <a class="nav-link" href="/report-campaigns">
            <i class="fas fa-fw fa-table"></i>
            <span>Report</span></a>
    </li>
{{--    <!-- Divider -->--}}
{{--    <hr class="sidebar-divider">--}}

{{--    <!-- Heading -->--}}
{{--    <div class="sidebar-heading">--}}
{{--        Addons--}}
{{--    </div>--}}

{{--    <!-- Nav Item - Pages Collapse Menu -->--}}
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"--}}
{{--           aria-expanded="true" aria-controls="collapsePages">--}}
{{--            <i class="fas fa-fw fa-folder"></i>--}}
{{--            <span>Pages</span>--}}
{{--        </a>--}}
{{--        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">--}}
{{--            <div class="bg-white py-2 collapse-inner rounded">--}}
{{--                <h6 class="collapse-header">Login Screens:</h6>--}}
{{--                <a class="collapse-item" href="login.html">Login</a>--}}
{{--                <a class="collapse-item" href="register.html">Register</a>--}}
{{--                <a class="collapse-item" href="forgot-password.html">Forgot Password</a>--}}
{{--                <div class="collapse-divider"></div>--}}
{{--                <h6 class="collapse-header">Other Pages:</h6>--}}
{{--                <a class="collapse-item" href="404.html">404 Page</a>--}}
{{--                <a class="collapse-item" href="blank.html">Blank Page</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </li>--}}

{{--    <!-- Nav Item - Charts -->--}}
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link" href="charts.html">--}}
{{--            <i class="fas fa-fw fa-chart-area"></i>--}}
{{--            <span>Charts</span></a>--}}
{{--    </li>--}}

{{--    <!-- Nav Item - Tables -->--}}
{{--    <li class="nav-item">--}}
{{--        <a class="nav-link" href="tables.html">--}}
{{--            <i class="fas fa-fw fa-table"></i>--}}
{{--            <span>Tables</span></a>--}}
{{--    </li>--}}

{{--    <!-- Divider -->--}}
{{--    <hr class="sidebar-divider d-none d-md-block">--}}

{{--    <!-- Sidebar Toggler (Sidebar) -->--}}
{{--    <div class="text-center d-none d-md-inline">--}}
{{--        <button class="rounded-circle border-0" id="sidebarToggle"></button>--}}
{{--    </div>--}}

{{--    <!-- Sidebar Message -->--}}
{{--    <div class="sidebar-card">--}}
{{--        <img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="">--}}
{{--        <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>--}}
{{--        <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>--}}
{{--    </div>--}}

</ul>
<!-- End of Sidebar -->
