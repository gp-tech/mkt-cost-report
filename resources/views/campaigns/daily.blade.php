@extends('layouts.admin')

@section('title', 'Tee Campaigns')

@section('content')
    <h1 class="h3 mb-2 text-gray-800"></h1>
    <form id="campaigns-form" action="/campaigns/index">
        <div class="row">
            <div class="col-md-2">
                <input type="text" name="keyword1" placeholder="Keyword 1" class="form-control" value="{{ $keyword1 }}">
            </div>
            <div class="col-md-2">
                <input type="text" name="keyword2" placeholder="Keyword 2" class="form-control" value="{{ $keyword2 }}">
            </div>
            <div class="col-md-3">
                <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                    <input type="hidden" name="start_date" class="start_date" />
                    <input type="hidden" name="end_date" class="end_date" />
                </div>
            </div>
            <div class="col-md-2">
                <input id="btnSearch" type="submit" class="btn btn-primary" value="Search"/>
            </div>
        </div>
    </form>
    </br>
    <div class="card card-header-actions shadow mb-4">
        <div class="card-header">
            Tee Campaigns
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
{{--                        <th scope="col">Name</th>--}}
                        <th scope="col">URL</th>
                        <th scope="col">Views</th>
                        <th scope="col">Orders</th>
                        <th scope="col">
                            Conversion
                        </th>
                        <th scope="col">
                            Unit Sales
                        </th>
                        <th scope="col">
                            Total Profit
                        </th>
                        <th scope="col">Pixel</th>
                        <th scope="col">Status</th>
                        <th scope="col">Updated at</th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($campaigns as $campaign)
                        <tr>
                            <td scope="row"></td>
{{--                            <td scope="row"><a target="_blank" href="{{ $campaign->url }}">{{ $campaign->name }}</a></td>--}}
                            <td scope="row"><a target="_blank" href="{{ $campaign->url }}">{{ $campaign->url }}</a></td>
                            <td scope="row" class="text-right">{{ $campaign->view }}</td>
                            <td scope="row" class="text-right">{{ $campaign->orders }}</td>
                            <td scope="row" class="text-right">{{ round($campaign->conversion, 2) }}</td>
                            <td scope="row" class="text-right">{{ $campaign->unit_sales }}</td>
                            <td scope="row" class="text-right">{{ $campaign->total_profit }}</td>
                            <td scope="row">{{ $campaign->pixel }}</td>
                            <td scope="row">{{ $campaign->status == 1 ? 'On' : 'OFF' }}</td>
                            <td scope="row">{{ $campaign->time_range }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="2" style="text-align:right">Total:</th>
                        <th style="text-align:right">{{$campaigns_total->total_view}}</th>
                        <th style="text-align:right">{{$campaigns_total->total_orders}}</th>
                        <th style="text-align:right">{{round($campaigns_total->total_conversion, 2)}}</th>
                        <th style="text-align:right">{{$campaigns_total->total_unit_sales}}</th>
                        <th style="text-align:right">{{$campaigns_total->total_profits}}</th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>


@endsection
<!-- Custom styles for this page -->
@push('styles')<!-- Bootstrap core JavaScript-->
<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('scripts')<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.24/api/sum().js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(function() {
        var start = moment.utc("<?= $start; ?>");
        var end = moment.utc("<?= $end; ?>");

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#reportrange input.start_date').val(start.format('YYYY-MM-DD'));
            $('#reportrange input.end_date').val(end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'All time': [moment('2017-01-01'), moment()],
                'Today': [moment().subtract(1, 'days'), moment()],
                'Yesterday': [moment().subtract(2, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

        var table = $('#dataTable').DataTable({
            searching: false,
            "iDisplayLength": 50
        });
    });
</script>
@endpush
