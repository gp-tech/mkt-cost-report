@extends('layouts.admin')

@section('title', 'Report')

@section('content')
    <form id="campaigns-form" action="/report-campaigns">
        <div class="row">
            <div class="col-md-2">
                <input type="text" name="keyword1" placeholder="Keyword 1" class="form-control" value="{{ $keyword1 }}">
            </div>
            <div class="col-md-2">
                <input type="text" name="keyword2" placeholder="Keyword 2" class="form-control" value="{{ $keyword2 }}">
            </div>
            <div class="col-md-3">
                <div id="reportrange"
                     style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                    <i class="fa fa-calendar"></i>&nbsp;
                    <span></span> <i class="fa fa-caret-down"></i>
                    <input type="hidden" name="start_date" class="start_date"/>
                    <input type="hidden" name="end_date" class="end_date"/>
                </div>
            </div>
            <div class="col-md-2">
                <input id="btnSearch" type="submit" class="btn btn-primary" value="Search"/>
            </div>
        </div>
    </form>
    </br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-header-actions shadow mb-4">
                <div class="card-header">
                    ROI: {{ $total['spend'] > 0 ? (round( (($campaigns_total->total_profits -  $total['spend'] ) / $total['spend']), 2)) .'%'  : 'Not Available' }}
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-header-actions shadow mb-4">
                <div class="card-header">
                    TeeChip
                </div>
                <div class="card-body">
                    <?php //if ($tee_campaigns) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="tee-dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th scope="col">URL</th>
                                <th scope="col">Views</th>
                                <th scope="col">Orders</th>
                                <th scope="col">
                                    Conversion
                                </th>
                                <th scope="col">
                                    Unit Sales
                                </th>
                                <th scope="col">
                                    Total Profit
                                </th>
                            </tr>
                            </thead>
                            <?php if ($start == '2017-01-01') { ?>
                                <tbody>
                                @foreach ($tee_data as $campaign)
                                    <tr>
                                        <td scope="row"><a target="_blank"
                                                           href="{{ $campaign['url'] }}">{{ $campaign['url'] }}</a></td>
                                        <td scope="row" class="text-right">{{ $campaign['view'] }}</td>
                                        <td scope="row" class="text-right">{{ $campaign['orders'] }}</td>
                                        <td scope="row" class="text-right">{{ $campaign['view'] ? round(($campaign['orders'] / $campaign['view']) * 100, 2) : 0 }}}}%</td>
                                        <td scope="row" class="text-right">{{ $campaign['unit_sales'] }}</td>
                                        <td scope="row" class="text-right">{{ $campaign['total_profit'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th style="text-align:right">Total:</th>
                                    <th style="text-align:right">{{$campaigns_total->total_view}}</th>
                                    <th style="text-align:right">{{$campaigns_total->total_orders}}</th>
                                    <th style="text-align:right">{{ $campaigns_total->total_view ? round(($campaigns_total->total_orders / $campaigns_total->total_view) * 100, 2) : 0}}%</th>
                                    <th style="text-align:right">{{$campaigns_total->total_unit_sales}}</th>
                                    <th style="text-align:right">{{$campaigns_total->total_profits}}</th>
                                </tr>
                                </tfoot>
                            <?php } else { ?>
                                <tbody>
                                    @foreach ($tee_data as $campaign)
                                        <?php if ($campaign['view'] < 1) continue; ?>
                                        <tr>
                                            <td scope="row" style="width: 30%;">
                                                <a target="_blank" href="{{ $campaign['url'] }}">{{ $campaign['url'] }}</a>
                                            </td>
                                            <td scope="row" class="text-right">{{ $campaign['view'] }}</td>
                                            <td scope="row" class="text-right">{{ $campaign['orders'] }}</td>
                                            <td scope="row" class="text-right">{{ $campaign['view'] ? round(($campaign['orders'] / $campaign['view']) * 100, 2) : 0}}%</td>
                                            <td scope="row" class="text-right">{{ $campaign['unit_sales'] }}</td>
                                            <td scope="row" class="text-right">{{ $campaign['total_profit'] }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th style="text-align:right">Total:</th>
                                        <th style="text-align:right">{{$campaigns_total->total_view}}</th>
                                        <th style="text-align:right">{{$campaigns_total->total_orders}}</th>
                                        <th style="text-align:right">{{$campaigns_total->total_view ? round(($campaigns_total->total_orders / $campaigns_total->total_view) * 100, 2) : 0}}%</th>
                                        <th style="text-align:right">{{$campaigns_total->total_unit_sales}}</th>
                                        <th style="text-align:right">{{$campaigns_total->total_profits}}</th>
                                    </tr>
                                </tfoot>
                            <?php } ?>


                        </table>
                    </div>
                    <?php //} ?>
                </div>
            </div>
        </div>


        <div class="col-lg-12">
            <div class="card card-header-actions shadow mb-4">
                <div class="card-header">
                    Facebook
                </div>
                <div class="card-body">
                    <?php if ($fb_campaigns) { ?>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="fb-dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Results</th>
                                <th scope="col">Impressions</th>
                                <th scope="col">Amount spent</th>
                                <th scope="col">Unique link clicks</th>
                                <th scope="col">Unique CTR</th>
                                <th scope="col">CPM</th>
                                <th scope="col">Cost per result</th>
                                <th scope="col">Cost per add to cart</th>
                                <th scope="col">Cost per unique link click</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php if ($start == '2017-01-01') { ?>
                            @foreach ($fb_campaigns as $campaign)
                                <tr>
                                    <td scope="row"
                                        width="10%">{{ $campaign->account_id }} <br/> {{ $campaign->name }}</td>
                                    <th scope="col">{{ $campaign->results }}</th>
                                    <th scope="col">{{ $campaign->impressions }}</th>
                                    <th scope="col">{{ $campaign->spend }}</th>
                                    <th scope="col">{{ round($campaign->unique_clicks, 2) }}</th>
                                    <th scope="col">{{
                                    $campaign->impressions > 0 ?
                                    round($campaign->unique_clicks / ($campaign->impressions / 1000), 2) : 0 }}</th>
                                    <th scope="col">{{
                                    $campaign->impressions > 0 ?
                                    round(($campaign->spend / ($campaign->impressions / 1000)) , 2) : 0 }}</th>
                                    <th scope="col">{{
                                    $campaign->results > 0 ?
                                    round($campaign->spend / $campaign->results, 2) : 0 }}</th>
                                    <th scope="col">{{
                                    $campaign->unique_adds_to_cart > 0 ?
                                    round($campaign->spend / $campaign->unique_adds_to_cart, 2) : 0 }}</th>
                                    <th scope="col">{{
                                    $campaign->unique_clicks > 0 ?
                                    round($campaign->spend / $campaign->unique_clicks, 2) : 0 }}</th>
                                </tr>
                            @endforeach
                            <?php } else { ?>
                            @foreach ($fb_campaigns as $campaign)
                                <tr>
                                    <td scope="row"
                                        width="8%">{{ $campaign->account_id }} <br/> {{ $campaign->name }}</td>
                                    <th scope="col">{{ $campaign->campaignInsight->sum('results') }}</th>
                                    <th scope="col">{{ $campaign->campaignInsight->sum('impressions') }}</th>
                                    <th scope="col">{{ round($campaign->campaignInsight->sum('spend'), 2) }}</th>
                                    <th scope="col">{{ round($campaign->campaignInsight->sum('unique_clicks'), 2) }}</th>
                                    <th scope="col">{{
                                        $campaign->campaignInsight->sum('impressions') > 0 ?
                                        round($campaign->campaignInsight->sum('unique_clicks') / ($campaign->campaignInsight->sum('impressions') / 1000), 2) : 0 }}</th>

                                    <th scope="col">{{
                                        $campaign->campaignInsight->sum('impressions') > 0 ?
                                        round(($campaign->campaignInsight->sum('spend') / ($campaign->campaignInsight->sum('impressions') / 1000)) , 2) : 0 }}</th>
                                    <th scope="col">{{
                                        $campaign->campaignInsight->sum('results') > 0 ?
                                        round($campaign->campaignInsight->sum('spend') / $campaign->campaignInsight->sum('results'), 2) : 0 }}</th>
                                    <th scope="col">{{
                                        $campaign->campaignInsight->sum('unique_adds_to_cart') > 0 ?
                                        round($campaign->campaignInsight->sum('spend') / $campaign->campaignInsight->sum('unique_adds_to_cart'), 2) : 0 }}</th>
                                    <th scope="col">{{
                                        $campaign->campaignInsight->sum('unique_clicks') > 0 ?
                                        round($campaign->campaignInsight->sum('spend') / $campaign->campaignInsight->sum('unique_clicks'), 2) : 0}}</th>
                                </tr>
                            @endforeach
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <?php if ($total['impressions'] > 0) : ?>
                            <tr>
                                <th scope="col">Total</th>
                                <th scope="col">{{ $total['results'] }}</th>
                                <th scope="col">{{ $total['impressions'] }}</th>
                                <th scope="col">{{ round($total['spend'], 2) }}</th>
                                <th scope="col">{{ $total['unique_clicks'] }}</th>
                                <th scope="col">{{ $total['impressions'] > 0 ? round(($total['unique_clicks'] / ($total['impressions'] / 1000)) , 2) : 0 }}</th>
                                <th scope="col">{{ $total['impressions'] > 0 ? round(($total['spend'] / ($total['impressions'] / 1000)) , 2) : 0 }}</th>

                                <th scope="col">{{ $total['results'] > 0 ? round($total['spend'] / $total['results'], 2) : 0 }}</th>
                                <th scope="col">{{ $total['unique_adds_to_cart'] > 0 ? round($total['spend'] / $total['unique_adds_to_cart'], 2) : 0 }}</th>

                                <th scope="col">{{ $total['unique_clicks'] ? round($total['spend'] / $total['unique_clicks'], 2) : 0}}</th>
                            </tr>
                            <?php endif; ?>
                            </tfoot>
                        </table>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

@endsection
<!-- Custom styles for this page -->
@push('styles')<!-- Bootstrap core JavaScript-->
<link href="/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@endpush
@push('scripts')<!-- Bootstrap core JavaScript-->
<script src="/vendor/jquery/jquery.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $(function () {
        var today = moment.utc("<?=  date('Y-m-d'); ?>");
        var yesterday = moment.utc("<?=  date('Y-m-d', strtotime('-1 day')); ?>");
        var start = moment.utc("<?= $start; ?>");
        var end = moment.utc("<?= $end; ?>");

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            $('#reportrange input.start_date').val(start.format('YYYY-MM-DD'));
            $('#reportrange input.end_date').val(end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'All time': [moment('2017-01-01'), moment()],
                'Today': [today, today],
                'Yesterday': [yesterday, yesterday],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

        $('#tee-dataTable').DataTable({
            searching: false,
            "iDisplayLength": 10,
            order: [[ 1, "desc" ]],
        });

        $('#fb-dataTable').DataTable({
            searching: false,
            "iDisplayLength": 10,
            order: [[ 1, "desc" ]],
        });
    });
</script>
@endpush
