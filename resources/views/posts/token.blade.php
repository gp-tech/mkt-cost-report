@extends('layouts.admin')

@section('title', 'Post list')
@section('content')
    <h1 class="mt-4">Post list</h1>
    <div class="posts-buttons row">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr style="display:none;">
                    <th scope="col">#</th>
                    <th scope="col"></th>
                    <th scope="col">Detail</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="border: 0px !important;"><input type="text" class="form-control" id="access-token" value="" placeholder="User Token"/></td>
                    <td style="border: 0px !important;">
                        <button id="btnToken" type="button"  class="btn btn-primary">Generate Page Token</button>
                    </td>

                </tr>
                </tbody>
            </table>

        </div>
    </div>


    <script>
    $(function(){
        $('#uploadButton').click(function(){
            $('#uploadFrame').attr('src', '/posts/uploadImages');
        })

        $('.btnEdit').click(function(){
            var button = $(this);
            var id = button.attr('data-id');
            $('#editFrame').attr('src', '/posts/edit/' + id);
        })

        $('.get-post-id').click(function(){
            var button = $(this);
            var id = button.attr('data-id');

            button.attr('disabled', 'disabled');
            button.append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');

            jQuery.post('/posts/getFBPostID', {id: id, _token: '{{ csrf_token() }}'}, function(response){

                if(response.status == 'success'){
                    $('#post-' + id).html('<a href="https://www.facebook.com/'+ response.post_id +'">' +response.post_id+ '</a>');
                    button.text('Get Post ID');
                }
                else{
                    if(response.status == 'error'){
                        alert(response.message);
                    }

                    button.removeAttr('disabled').text('Get Post ID');
                }
            });
        })

        $('#date').change(function(){
            location.href = '/posts/list?date=' + $(this).val();
        })

        $('#btnExport').click(function(){
            location.href = '/posts/export?date=' + $('#date').val();
        })

        $('#btnGetAll').click(function(){
            var button = $(this);
            var date = $('#date').val();

            button.attr('disabled', 'disabled');

            $.get('/posts/getAllIDs', {date: date}, function(response){
                var ids = response.ids;
                var total = ids.length;
                var i = 0;
                var isRun = false;

                if(ids.length > 0){
                    button.text('Get all post ID ('+ i +'/ '+ total +')');

                    var scheduleJobs = setInterval(() => {
                        if(isRun){
                            return;
                        }

                        isRun = true;

                        jQuery.post('/posts/getFBPostID', {id: ids[i].id, _token: '{{ csrf_token() }}'}, function(response){

                            if(response.status == 'success'){
                                $('#post-' + ids[i].id).html('<a target="blank" href="https://www.facebook.com/'+ response.post_id +'">' +response.post_id+ '</a>');
                                button.text('Get all post ID ('+ (i + 1) +'/ '+ total +')');
                            }
                            else{
                                if(response.status == 'error'){
                                    alert(response.message);
                                    clearInterval(scheduleJobs);

                                    setTimeout(function(){
                                        button.removeAttr('disabled').text('Get all post ID');
                                    }, 1000);
                                }
                            }

                            i++;

                            if(i >= total){
                                clearInterval(scheduleJobs);

                                setTimeout(function(){
                                    button.removeAttr('disabled').text('Get all post ID');
                                }, 1500);
                            }
                            else{
                                isRun = false;
                            }
                        });
                    }, 1000);
                }
                else{
                    alert('All posts have already submitted to Facebook.');
                    button.removeAttr('disabled').text('Get all post ID');
                }
            })
        })

        $('#btnToken').click(function(){
            var token = $('#access-token').val();
            var button = $(this);

            button.attr('disabled', 'disabled');

            $.post('/posts/saveToken', {'access-token' : token, _token: '{{ csrf_token() }}'}, function(response){
                if(response.status == 'error'){
                    alert(response.message);
                }

                setTimeout(function(){
                    button.removeAttr('disabled');
                }, 1000);
            })
        })

    })
    </script>
@endsection
