@extends('layouts.admin')

@section('title', 'Post list')
@section('content')
    <h1 class="mt-4">Post list</h1>
    <div class="posts-buttons row">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                <tr style="display:none;">
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="border: 0px !important;">
                        <a href="/posts/token" id="btnToken" type="button"  class="btn btn-primary">Generate Token</a>
                        <button id="uploadButton" type="button" class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">Upload Images</button>
                    </td>
                    <td style="border: 0px !important;">

                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header">
            <select class="col-md-4 form-control" id="date" name="group">
                @foreach ($dates as $date)
                    <option value="{{ $date['value'] }}" {{ ($currentDate == $date['value'] ? 'selected="selected"' : '') }}>{{ $date['label'] }}</option>
                @endforeach
            </select>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Image</th>
                        <th scope="col">Detail</th>
                        <th scope="col">Post ID</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($posts as $post)
                        <tr>
                            <th scope="row">{{ $no++ }}</th>
                            <td><img style="max-width: 200px; max-height: 100px" class="post-image" src="/images/{{ $post->image }}"></td>
                            <td>
                                <div>
                                    <label>Campaign code: </label> <span>{{ $post->campaign_code }}</span>

                                </div>
                                <div>
                                    <label>Niche code: </label> <span>{{ $post->niche }}</span>
                                </div>
                        <div>
                                    <label>FB Page ID: </label> <span>{{ $post->page_id }}</span>
                                </div>
                                <div>
                                    <label>Domain: </label> <a href="{{ $domain . $post->campaign_code }}">{{ $domain . $post->campaign_code }}</a>
                                </div>
                                <div>
                                    <label>Description: </label>
                                    @if (isset($captionTemplates[$post->caption_id]))
                                    <div>{{ str_replace('<link>', $domain . $post->campaign_code, $captionTemplates[$post->caption_id]) }}</div>
                                    @endif
                                </div>
                            </td>
                            <td id="post-{{ $post->id }}">
                                @if (!empty($post->post_id))
                                <a target="blank" href="https://www.facebook.com/{{ $post->post_id }}">{{ $post->post_id }}</a>
                                @endif
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary get-post-id" class="get-post-id" data-id="{{ $post->id }}" {{ !empty($post->post_id) ? 'disabled="disabled"' : '' }}>Get Post ID</button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-info btnEdit" data-id="{{ $post->id }}" data-toggle="modal" data-target="#editModal">Edit</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="center">
                    {{ $posts->withQueryString()->links() }}
                </div>
            </div>
        </div>
    </div>
    <div class="text-right">
        <button id="btnGetAll" type="button" class="btn btn-primary">Get all post ID</button>
        <button id="btnExport" type="button" class="btn btn-primary">Export</button>
    </div>
    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload Images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe width="100%" style="min-height: 400px" id="uploadFrame" src="/posts/uploadImages"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe id="editFrame" src=""></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
    <script>
    $(function(){
        $('#uploadButton').click(function(){
            $('#uploadFrame').attr('src', '/posts/uploadImages');
        })

        $('.btnEdit').click(function(){
            var button = $(this);
            var id = button.attr('data-id');
            $('#editFrame').attr('src', '/posts/edit/' + id);
        })

        $('.get-post-id').click(function(){
            var button = $(this);
            var id = button.attr('data-id');

            button.attr('disabled', 'disabled');
            button.append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');

            jQuery.post('/posts/getFBPostID', {id: id, _token: '{{ csrf_token() }}'}, function(response){

                if(response.status == 'success'){
                    $('#post-' + id).html('<a href="https://www.facebook.com/'+ response.post_id +'">' +response.post_id+ '</a>');
                    button.text('Get Post ID');
                }
                else{
                    if(response.status == 'error'){
                        alert(response.message);
                    }

                    button.removeAttr('disabled').text('Get Post ID');
                }
            });
        })

        $('#date').change(function(){
            location.href = '/posts/list?date=' + $(this).val();
        })

        $('#btnExport').click(function(){
            location.href = '/posts/export?date=' + $('#date').val();
        })

        $('#btnGetAll').click(function(){
            var button = $(this);
            var date = $('#date').val();

            button.attr('disabled', 'disabled');

            $.get('/posts/getAllIDs', {date: date}, function(response){
                var ids = response.ids;
                var total = ids.length;
                var i = 0;
                var isRun = false;

                if(ids.length > 0){
                    button.text('Get all post ID ('+ i +'/ '+ total +')');

                    var scheduleJobs = setInterval(() => {
                        if(isRun){
                            return;
                        }

                        isRun = true;

                        jQuery.post('/posts/getFBPostID', {id: ids[i].id, _token: '{{ csrf_token() }}'}, function(response){

                            if(response.status == 'success'){
                                $('#post-' + ids[i].id).html('<a target="blank" href="https://www.facebook.com/'+ response.post_id +'">' +response.post_id+ '</a>');
                                button.text('Get all post ID ('+ (i + 1) +'/ '+ total +')');
                            }
                            else{
                                if(response.status == 'error'){
                                    alert(response.message);
                                    clearInterval(scheduleJobs);

                                    setTimeout(function(){
                                        button.removeAttr('disabled').text('Get all post ID');
                                    }, 1000);
                                }
                            }

                            i++;

                            if(i >= total){
                                clearInterval(scheduleJobs);

                                setTimeout(function(){
                                    button.removeAttr('disabled').text('Get all post ID');
                                }, 1500);
                            }
                            else{
                                isRun = false;
                            }
                        });
                    }, 1000);
                }
                else{
                    alert('All posts have already submitted to Facebook.');
                    button.removeAttr('disabled').text('Get all post ID');
                }
            })
        })

        $('#btnToken').click(function(){
            var token = $('#access-token').val();
            var button = $(this);

            button.attr('disabled', 'disabled');

            $.post('/posts/saveToken', {'access-token' : token, _token: '{{ csrf_token() }}'}, function(response){
                if(response.status == 'error'){
                    alert(response.message);
                }

                setTimeout(function(){
                    button.removeAttr('disabled');
                }, 1000);
            })
        })

    })
    </script>
@endsection
