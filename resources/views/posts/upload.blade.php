@extends('layouts.empty')

@section('title', 'Post list')

@section('content')
    <form method='post' action='/posts/upload' enctype="multipart/form-data">
        <input type="file" id='images' name="images[]" multiple><br><br>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input  id="submit" type="button" class="btn btn-primary" value="Upload">
        <div id="counter"></div>
        <div id="detail"></div>
    </form>
    <script>
    $(document).ready(function(){
        $('#submit').click(function(){
            var button = $('#submit');
            var isRun = false;
            var i = 0;
            var total = document.getElementById('images').files.length;

            button.attr('disabled', 'disabled');
            $('#counter').html('Uploading: 0/' + total);
            $('#detail').html('');
            $('#process-bar').attr('aria-valuemax', total);
            $('#process-bar').attr('aria-valuenow', 0);
            $('#process-bar').show();

            var process = setInterval(() => {
                if(isRun){
                    return;
                }

                isRun = true;
                var form_data = new FormData();
                var file = document.getElementById('images').files[i];
                form_data.append("images[]", file);
                form_data.append('_token', '{{csrf_token()}}');

                $.ajax({
                    url: '/posts/upload', 
                    type: 'post',
                    data: form_data,
                    //dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        i++;

                        setTimeout(() => {
                            if(i >= total){
                                isRun = true;
                                clearInterval(process);
                                button.removeAttr('disabled');
                                $('#counter').html('Uploaded: '+ total + '/' + total);
                            }
                            else{
                                $('#counter').html('Uploading: '+ i + '/' + total);
                                isRun = false;
                            }
                            $('#detail').append(file.name + '<br>');
                        }, 1000);
                    }
                });
            }, 1000);
        });
    });
    </script>
@endsection