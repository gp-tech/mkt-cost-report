@extends('layouts.empty')

@section('content')
    <form id="post-form" method="post" action="/posts/edit/{{ $post->id }}">
        <div class="form-group">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session()->has('success'))
            <div class="alert alert-success">
                @if(is_array(session()->get('success')))
                <ul>
                    @foreach (session()->get('success') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
                @else
                    {{ session()->get('success') }}
                @endif
            </div>
        @endif
        </div>
        <div class="form-group">
            <label for="niche">Niche</label>
            <input type="text" class="form-control" id="niche" name="niche" value="{{ $post->niche }}">
        </div>
        <div class="form-group">
            <label for="group">FB Page</label>
            <select class="form-control" id="page" name="page">
		        <option value=""></option>
                @foreach ($pages as $page)
                <option value="{{ $page->fb_id }}" {{ $post->page_id == $page->fb_id ? 'selected="selected"' : '' }}>{{ $page->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="group">Caption</label>
            <select class="form-control" id="caption" name="caption">
                <option value=""></option>
                @foreach ($captions as $caption)
                <option value="{{ $caption->id }}" {{ $post->caption_id == $caption->id ? 'selected="selected"' : '' }}>{{ $caption->name }}</option>
                @endforeach
            </select>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $post->id }}">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
