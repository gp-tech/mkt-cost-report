<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('city')->nullable();
            $table->string('company')->nullable();
            $table->string('country_code', 2)->nullable();
            $table->string('zip')->nullable()->change();
            $table->string('state_code', 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('city');
            $table->dropColumn('company');
            $table->dropColumn('country_code');
            $table->integer('zip')->nullable()->change();
            $table->dropColumn('state_code');
        });
    }
}
