<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->string('campaign_id');
            $table->string('url');
            $table->integer('view');
            $table->integer('orders');
            $table->double('conversion');
            $table->integer('unit_sales');
            $table->decimal('total_profit');
            $table->string('pixel');
            $table->tinyInteger('status');
            $table->tinyInteger('source');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
