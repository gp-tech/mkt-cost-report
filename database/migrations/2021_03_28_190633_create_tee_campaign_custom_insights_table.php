<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeeCampaignCustomInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::create('tee_campaign_custom_insights', function (Blueprint $table) {
//            $table->id();
//
//            $table->string('tee_campaign_id', 250);
//            $table->string('url');
//            $table->integer('view');
//            $table->integer('orders');
//            $table->double('conversion');
//            $table->integer('unit_sales');
//            $table->decimal('total_profit');
//            $table->string('pixel');
//            $table->tinyInteger('status');
//            $table->tinyInteger('source');
//            $table->string('time_range');
//            $table->timestamps();
//
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tee_campaign_custom_insights');
    }
}
