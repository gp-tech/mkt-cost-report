<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site');
            $table->string('card');
            $table->string('verify');
            $table->tinyInteger('expire_month');
            $table->integer('expire_year');
            $table->string('email');
            $table->string('password');
            $table->string('username');
            $table->string('is_registered');
            $table->unique(['site', 'card', 'email'], 'u');
            $table->integer('roller')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billings');
    }
}
