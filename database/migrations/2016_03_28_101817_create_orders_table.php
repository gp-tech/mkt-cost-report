<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('pending_user_id')->default(0);
            $table->unsignedBigInteger('uid');
            $table->string('name');
            $table->unsignedInteger('billing_address_id');
            $table->unsignedInteger('ship_address_id');
            $table->unsignedInteger('order_number');
            $table->string('cancel_reason')->nullable();
            $table->dateTime('cancelled_at')->nullable();
            $table->dateTime('processed_at')->nullable();
            $table->dateTime('closed_at')->nullable();
            $table->string('currency', 3);
            $table->string('email');
            $table->string('customer')->nullable();
            $table->string('financial_status');
            $table->string('fulfillment_status')->nullable();
            $table->decimal('total_price', 13, 2);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
