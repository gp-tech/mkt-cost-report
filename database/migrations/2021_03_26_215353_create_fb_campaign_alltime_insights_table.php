<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFbCampaignAlltimeInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_campaign_alltime_insights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('fb_campaign_id');
            $table->bigInteger('campaign_id');

            $table->string('name');
            $table->string('results');
            $table->string('spend');
            $table->string('unique_link_clicks_ctr');
            $table->string('cost_per_unique_click');

            $table->string('cost_per_unique_add_to_cart');
            $table->string('unique_adds_to_cart');

            $table->string('cpm');
            $table->string('purchase');
            $table->string('clicks');
            $table->string('impressions');
            $table->string('unique_clicks');
            $table->string('time_range');
            $table->timestamps();
            $table->foreign('fb_campaign_id')->references('id')->on('fb_campaigns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fb_campaign_alltime_insights');
    }
}
