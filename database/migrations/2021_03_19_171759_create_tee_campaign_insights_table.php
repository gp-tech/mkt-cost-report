<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeeCampaignInsightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tee_campaign_insights', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('campaign_id');
            $table->string('tee_campaign_id', 250);

            $table->string('url');
            $table->integer('view');
            $table->integer('orders');
            $table->double('conversion');

            $table->integer('unit_sales');
            $table->decimal('total_profit');

            $table->string('pixel');
            $table->tinyInteger('status');
            $table->tinyInteger('source');
            $table->timestamp('time_range', $precision = 0);

            $table->timestamps();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tee_campaign_insights');
    }
}
