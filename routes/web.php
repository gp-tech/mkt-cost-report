<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('login',
    [
        'as'   => 'login',
        'uses' => 'LoginController@index'
    ]
);


Route::post('loginPost', ['uses' => 'LoginController@authenticate']);

Route::get('logout', function () { Auth::logout();
    return redirect()->intended('/');
});

Route::get('dashboard', [
    'middleware' => 'auth',
    'uses'       => 'DashboardController@index'
]);

Route::get('users/list', [
    'middleware' => 'auth',
    'uses'       => 'UserController@list'
]);

Route::get('users/add', [
    'middleware' => 'auth',
    'uses'       => 'UserController@add'
]);

Route::post('/createUser', [
    'middleware' => 'auth',
    'uses'       => 'UserController@add'
]);

Route::get('pages/add', [
    'middleware' => 'auth',
    'uses'       => 'PageController@add'
]);

Route::get('pages/list', [
    'middleware' => 'auth',
    'uses'       => 'PageController@list'
]);

Route::post('/createPage', [
    'middleware' => 'auth',
    'uses'       => 'PageController@add'
]);

Route::get('posts/list', [
    'middleware' => 'auth',
    'uses'       => 'PostController@list'
]);

Route::get('posts/token', [
    'middleware' => 'auth',
    'uses'       => 'PostController@token'
]);

Route::get('posts/export', [
    'middleware' => 'auth',
    'uses'       => 'PostController@export'
]);

Route::post('posts/getFBPostID', [
    'middleware' => 'auth',
    'uses'       => 'PostController@getFBPostID'
]);

Route::get('posts/getAllIDs', [
    'middleware' => 'auth',
    'uses'       => 'PostController@getAllIDs'
]);

Route::get('posts/uploadForm', [
    'middleware' => 'auth',
    'uses'       => 'PostController@uploadForm'
]);

Route::match(['get', 'post'], 'posts/edit/{id}', [
    'middleware' => 'auth',
    'uses'       => 'PostController@edit'
]);

Route::post('posts/saveToken', [
    'middleware' => 'auth',
    'uses'       => 'PostController@saveToken'
]);

Route::get('posts/uploadImages', function () {
    return view('posts.upload');
});

Route::post('posts/upload', [
    'middleware' => 'auth',
    'uses'       => 'PostController@upload'
]);

Route::get('captions/list', [
    'middleware' => 'auth',
    'uses'       => 'CaptionController@list'
]);

Route::match(['get', 'post'], 'captions/add', [
    'middleware' => 'auth',
    'uses'       => 'CaptionController@add'
]);

Route::match(['get', 'post'], 'captions/edit/{id}', [
    'middleware' => 'auth',
    'uses'       => 'CaptionController@edit'
]);

Route::get('sales/index', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@index'
]);

Route::get('sales/check', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@check'
]);

Route::post('sales/save', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@save'
]);


Route::get('sales/getAll', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@getAll'
]);

Route::get('sales/getToday', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@getToday'
]);

Route::get('sales/sendy', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@sendy'
]);

Route::post('sales/saveSendy', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@saveSendy'
]);

Route::post('sales/getRange', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@getRange'
]);

Route::get('sales/getYesterday', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@getYesterday'
]);

Route::get('sales/exportMonth', [
    'middleware' => 'auth',
    'uses'       => 'SaleController@exportMonth'
]);

Route::get('sales/cron', [
    'uses' => 'SaleController@cron'
]);

Route::get('campaigns/index', [
    'middleware' => 'auth',
    'uses'       => 'CampaignController@index'
]);

Route::get('campaigns/getAll', [
    'middleware' => 'auth',
    'uses'       => 'CampaignController@getAll'
]);

Route::get('campaigns/cron', [
    'uses' => 'CampaignController@cron'
]);
Route::get('fb-campaigns/index', [
    'middleware' => 'auth',
    'uses'       => 'FbCampaignController@index'
]);
Route::get('report-campaigns', 'CampaignController@report')->name('report-campaigns.index');

Route::group([
    'middleware' => 'auth',
], function () {

    Route::get('fb-campaigns/anyData', 'FbCampaignController@anyData')->name('fb-campaigns.data');
    Route::get('fb-campaigns/detail/{id}', 'FbCampaignController@detail')->name('fb-campaigns.detail');

    Route::get('fb-campaigns/app-refresh/{id}', 'FbCampaignController@appRefresh')->name('fb-campaigns.app-refresh');
    Route::get('fb-campaigns/app-edit/{id}', 'FbCampaignController@appEdit')->name('fb-campaigns.app-edit');
    Route::post('fb-campaigns/app-edit/{id}', 'FbCampaignController@appEdit');
    Route::get('fb-campaigns/app-delete/{id}', 'FbCampaignController@appDelete');
    Route::get('fb-campaigns/apps', 'FbCampaignController@apps')->name('fb-campaigns.apps');

    Route::get('fb-campaigns/add-app', 'FbCampaignController@addApp')->name('fb-campaigns.add-app');
    Route::post('fb-campaigns/add-app', 'FbCampaignController@addApp')->name('fb-campaigns.add-app');

    Route::get('fb-campaigns/accounts', 'FbCampaignController@accounts')->name('fb-campaigns.accounts');
    Route::get('fb-campaigns/add-account', 'FbCampaignController@addAccount')->name('fb-campaigns.add-account');
    Route::post('fb-campaigns/add-account', 'FbCampaignController@addAccount')->name('fb-campaigns.add-account');

    Route::get('fb-campaigns/jobs', 'FbCampaignController@jobs')->name('fb-campaigns.jobs');
    Route::get('fb-campaigns/clear-job', 'FbCampaignController@clearJob')->name('fb-campaigns.clear-jobs');
    Route::get('fb-campaigns/get-account/{id}', 'FbCampaignController@getAccounts')->name('fb-campaigns.get-accounts');
});

Route::get('shopify/orders', 'ShopifyController@orders')->name('shopify.orders');
